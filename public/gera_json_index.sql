SET SERVEROUTPUT ON
DECLARE

    v_arquivo UTL_FILE.FILE_TYPE;

BEGIN

  v_arquivo := utl_file.fopen('SGV_SIT', 'index.js', 'W');

  utl_file.put_line(v_arquivo, 'var last_update = "'|| to_char(sysdate,'dd/mm/yyyy hh24:mi:ss') ||'";');
  utl_file.put_line(v_arquivo, 'var tables = [');
  
    FOR y IN (SELECT owner
                    ,table_name
                    ,comments
                    ,created
                    ,last_ddl_time
                    ,rownum AS linha
                FROM (SELECT t.owner
                            ,t.table_name
                            ,translate(c.comments,'"'||chr(10)||chr(13),'   ') comments
                            ,o.created
                            ,o.last_ddl_time
                        FROM dba_tables t
                        JOIN dba_tab_comments c ON c.owner = t.owner AND c.table_name = t.table_name
                        JOIN dba_objects o ON  o.owner = t.owner AND o.object_name = t.table_name AND o.object_type = 'TABLE'
                        WHERE T.OWNER IN ('ADM_SGV_COB'
                                    ,'ADM_SGV_SFA'
                                    ,'DW_SGV'
                                    ,'ADM_SGV_WS'
                                    ,'ADM_SGV_PIN'
                                    ,'ADM_SGV_SEX'
                                    ,'ADM_SIT'
                                    ,'ADM_SGV_EQP'
                                    ,'ADM_SGV_ACE'
                                    ,'ADM_SGV_PRC'
                                    ,'ADM_SGV_FTR'
                                    ,'ADM_SGV_CAD'
                                    ,'ADM_SGV_AUD'
                                    ,'ADM_SGV_ALO'
                                    ,'ADM_SGV_TMP'
                                    ,'ADM_SGR_AUD'
                                    ,'ADM_SGV_CEP'
                                    ,'ADM_SGR_CRED'
                                    ,'ADM_SIG_PRC'
                                    ,'ADM_SGV_TRN'
                                    ,'ADM_SGR_ACE'
                                    ,'ADM_SGV_JDE'
                                    ,'ADM_SGR_CAD'
                                    ,'ADM_SGV_AUX'
                                    ,'ADM_SGV_INT'
                                    ,'DATAMART'
                                    ,'ADM_SGV_DNE'
                                    ,'SIG_VIVO'
                                    ,'ADM_ACE_AUD'
                                    ,'ADM_ACE_CAD'
                                    --,'ADM_BPM'
                                    ,'ADM_SGL_CAD'
                                    ,'ADM_SGL_FTR'
                                    ,'ADM_SGR'
                                    ,'ADM_SGR_PROS'
                                    ,'ADM_SGV'
                                    ,'ADM_SGV_BI'
                                    ,'ADM_SGV_GRD'
                                    ,'ADM_SGV_RMV')
                       ORDER BY 1,2))
    LOOP
    
      IF y.linha != 1
      THEN
        utl_file.put_line(v_arquivo, ',');
      END IF;
    
      utl_file.put_line(v_arquivo, '{"schema":"'|| y.owner ||'","name":"' || y.table_name || '","comments":"'|| y.comments ||'","lastddltime":"'|| y.last_ddl_time ||'","createdtime":"'|| y.created ||'"}');
    
    END LOOP;

  utl_file.put_line(v_arquivo, '];'); -- Fechando TABLES

  utl_file.fclose(v_arquivo);
 
  --adm_sgv.sgv_sit_pk001.envia_ftp('172.16.20.21', '21', 'testerel1', 'testerel1', 'SGV_SIT', 'index.js', 'index.js');
 -- utl_file.fremove('SGV_SIT', 'index.js');

END;
/