var last_update = "11/12/2019 16:51:12";
var tables = [
{"schema":"ADM_SGR_ACE","name":"ACESSO_NEGADO","comments":"","lastddltime":"13/05/2019 10:54:54","createdtime":"11/05/2019 21:25:54"}
,
{"schema":"ADM_SGR_ACE","name":"DEPARTAMENTO","comments":"Cadastro de departamentos","lastddltime":"13/05/2019 10:54:52","createdtime":"11/05/2019 21:25:54"}
,
{"schema":"ADM_SGR_ACE","name":"DEPARTAMENTO_USUARIO","comments":"V�nculo entre usu�rio e departamento","lastddltime":"13/05/2019 10:54:52","createdtime":"11/05/2019 21:25:54"}
,
{"schema":"ADM_SGR_ACE","name":"EXPEDIENTE","comments":"","lastddltime":"13/05/2019 10:54:55","createdtime":"11/05/2019 21:25:53"}
,
{"schema":"ADM_SGR_ACE","name":"FUNCIONALIDADE","comments":"Cadastro de funcionalidades","lastddltime":"13/05/2019 10:54:53","createdtime":"11/05/2019 21:25:54"}
,
{"schema":"ADM_SGR_ACE","name":"FUNCIONALIDADE_PERMISSAO","comments":"V�nculo das funcionalidades com as permiss�es","lastddltime":"13/05/2019 10:54:53","createdtime":"11/05/2019 21:25:54"}
,
{"schema":"ADM_SGR_ACE","name":"GRUPO","comments":"Cadastro de Grupos","lastddltime":"11/05/2019 23:32:33","createdtime":"11/05/2019 21:25:54"}
,
{"schema":"ADM_SGR_ACE","name":"GRUPO_PERMISSAO","comments":"V�nculo de Grupos e suas permiss�es","lastddltime":"13/05/2019 10:54:54","createdtime":"11/05/2019 21:25:54"}
,
{"schema":"ADM_SGR_ACE","name":"PERMISSAO","comments":"Cadastro de permiss�es","lastddltime":"11/05/2019 23:32:33","createdtime":"11/05/2019 21:25:54"}
,
{"schema":"ADM_SGR_ACE","name":"REDEFINIR_SENHA","comments":"","lastddltime":"13/05/2019 10:54:54","createdtime":"11/05/2019 21:25:54"}
,
{"schema":"ADM_SGR_ACE","name":"USUARIO_EMPRESA","comments":"V�nculo de usu�rio com empresa","lastddltime":"13/05/2019 10:54:55","createdtime":"11/05/2019 21:25:54"}
,
{"schema":"ADM_SGR_ACE","name":"USUARIO_GRUPO","comments":"V�nculo de usu�rio com grupo","lastddltime":"13/05/2019 10:54:54","createdtime":"11/05/2019 21:25:54"}
,
{"schema":"ADM_SGR_ACE","name":"USUARIO_SUPER_PERMISSAO","comments":"","lastddltime":"13/05/2019 10:54:55","createdtime":"11/05/2019 21:25:54"}
,
{"schema":"ADM_SGR_ACE","name":"USUARIO_WS","comments":"","lastddltime":"11/05/2019 23:32:33","createdtime":"11/05/2019 21:25:54"}
,
{"schema":"ADM_SGR_AUD","name":"AUDITORIA","comments":"","lastddltime":"12/05/2019 12:54:35","createdtime":"12/05/2019 11:01:20"}
,
{"schema":"ADM_SGR_AUD","name":"AUDITORIA_ITEM","comments":"","lastddltime":"13/05/2019 11:41:55","createdtime":"12/05/2019 11:01:44"}
,
{"schema":"ADM_SGR_AUD","name":"CAMPO","comments":"","lastddltime":"12/05/2019 12:54:34","createdtime":"12/05/2019 11:03:09"}
,
{"schema":"ADM_SGR_AUD","name":"TABELA","comments":"","lastddltime":"12/05/2019 12:54:35","createdtime":"12/05/2019 11:03:09"}
,
{"schema":"ADM_SGR_CAD","name":"ACAO_REALIZADA","comments":"","lastddltime":"11/05/2019 23:32:34","createdtime":"11/05/2019 21:25:54"}
,
{"schema":"ADM_SGR_CAD","name":"ACAO_TEMPLATE","comments":"","lastddltime":"13/05/2019 10:54:59","createdtime":"11/05/2019 21:25:55"}
,
{"schema":"ADM_SGR_CAD","name":"ACOES_GERAIS","comments":"","lastddltime":"13/05/2019 10:55:04","createdtime":"11/05/2019 21:25:55"}
,
{"schema":"ADM_SGR_CAD","name":"ACOES_GERAIS_EMAIL","comments":"","lastddltime":"13/05/2019 10:55:04","createdtime":"11/05/2019 21:25:54"}
,
{"schema":"ADM_SGR_CAD","name":"ATUALIZACAO_ENDERECO","comments":"","lastddltime":"13/05/2019 10:55:03","createdtime":"11/05/2019 21:25:55"}
,
{"schema":"ADM_SGR_CAD","name":"CANAL_VENDA_EMPRESA","comments":"Descri��o da tabela.","lastddltime":"13/05/2019 10:55:05","createdtime":"11/05/2019 21:25:54"}
,
{"schema":"ADM_SGR_CAD","name":"CATALOGO","comments":"","lastddltime":"11/05/2019 23:32:41","createdtime":"11/05/2019 21:25:55"}
,
{"schema":"ADM_SGR_CAD","name":"CATALOGO_EMPRESA","comments":"","lastddltime":"13/05/2019 10:54:59","createdtime":"11/05/2019 21:25:55"}
,
{"schema":"ADM_SGR_CAD","name":"CATALOGO_SERVICO","comments":"","lastddltime":"13/05/2019 10:55:00","createdtime":"11/05/2019 21:25:55"}
,
{"schema":"ADM_SGR_CAD","name":"CHAMADO","comments":"","lastddltime":"11/05/2019 23:32:41","createdtime":"11/05/2019 21:25:55"}
,
{"schema":"ADM_SGR_CAD","name":"CHAMADO_SERVICO","comments":"","lastddltime":"13/05/2019 10:55:00","createdtime":"11/05/2019 21:25:55"}
,
{"schema":"ADM_SGR_CAD","name":"CONFIGURACOES","comments":"","lastddltime":"11/05/2019 23:32:41","createdtime":"11/05/2019 21:25:55"}
,
{"schema":"ADM_SGR_CAD","name":"CONFIRMACAO_POSITIVA","comments":"","lastddltime":"13/05/2019 10:55:02","createdtime":"11/05/2019 21:25:55"}
,
{"schema":"ADM_SGR_CAD","name":"CONSULTA_CREDNET","comments":"","lastddltime":"11/05/2019 23:32:41","createdtime":"11/05/2019 21:25:59"}
,
{"schema":"ADM_SGR_CAD","name":"CONSULTA_SERASA","comments":"","lastddltime":"05/07/2019 13:10:56","createdtime":"11/05/2019 21:25:55"}
,
{"schema":"ADM_SGR_CAD","name":"CONSULTA_SERASA_ENDERECO","comments":"","lastddltime":"13/05/2019 10:55:01","createdtime":"11/05/2019 21:25:55"}
,
{"schema":"ADM_SGR_CAD","name":"CONSULTA_SERASA_LOGRADOURO","comments":"","lastddltime":"11/05/2019 23:32:40","createdtime":"11/05/2019 21:25:55"}
,
{"schema":"ADM_SGR_CAD","name":"CONSULTA_SERASA_SINTEGRA","comments":"","lastddltime":"11/05/2019 23:32:42","createdtime":"11/05/2019 21:25:55"}
,
{"schema":"ADM_SGR_CAD","name":"CONSULTA_SERASA_SITUACAO_PF","comments":"","lastddltime":"11/05/2019 23:32:42","createdtime":"11/05/2019 21:25:55"}
,
{"schema":"ADM_SGR_CAD","name":"CONSULTA_SERASA_SITUACAO_PJ","comments":"","lastddltime":"11/05/2019 23:32:42","createdtime":"11/05/2019 21:25:55"}
,
{"schema":"ADM_SGR_CAD","name":"CONSULTA_SERASA_TELEFONE","comments":"","lastddltime":"13/05/2019 10:55:01","createdtime":"11/05/2019 21:25:55"}
,
{"schema":"ADM_SGR_CAD","name":"EMPRESA_DOCUMENTO","comments":"Rela��o de DOCUMENTOS por EMPRESA.","lastddltime":"13/05/2019 10:55:04","createdtime":"11/05/2019 21:25:54"}
,
{"schema":"ADM_SGR_CAD","name":"EMPRESA_SOLUCAO","comments":"Sulu��es cadastradas para Empresa.","lastddltime":"13/05/2019 10:55:04","createdtime":"11/05/2019 21:25:54"}
,
{"schema":"ADM_SGR_CAD","name":"EXTERNAL_SERV_EXEC_CRON","comments":"Armazerna informa��o de servi�os externos executados no CRON","lastddltime":"11/05/2019 23:32:36","createdtime":"11/05/2019 21:25:55"}
,
{"schema":"ADM_SGR_CAD","name":"FASE","comments":"","lastddltime":"13/05/2019 10:54:57","createdtime":"11/05/2019 21:25:55"}
,
{"schema":"ADM_SGR_CAD","name":"FASE_HISTORICO","comments":"Hist�rico das fases","lastddltime":"13/05/2019 10:54:58","createdtime":"11/05/2019 21:25:55"}
,
{"schema":"ADM_SGR_CAD","name":"FASE_TEMPLATE","comments":"","lastddltime":"13/05/2019 10:54:57","createdtime":"11/05/2019 21:25:55"}
,
{"schema":"ADM_SGR_CAD","name":"FILE_METADADO","comments":"","lastddltime":"11/05/2019 23:32:44","createdtime":"11/05/2019 21:25:55"}
,
{"schema":"ADM_SGR_CAD","name":"HISTORICO_ENVIO_MP3","comments":"Armazena os dados de requisi��o e o resultado do retorno ao consumir o WS de envio de audio MP3 de aceite do cliente a ades�o de planos da operadora.","lastddltime":"11/05/2019 23:32:33","createdtime":"11/05/2019 21:25:54"}
,
{"schema":"ADM_SGR_CAD","name":"IMAGEM_APLICATIVO","comments":"","lastddltime":"13/05/2019 10:55:02","createdtime":"11/05/2019 21:25:55"}
,
{"schema":"ADM_SGR_CAD","name":"MANUTENCAO","comments":"Solicita��es de manuten��o","lastddltime":"22/08/2019 11:21:04","createdtime":"11/05/2019 21:25:55"}
,
{"schema":"ADM_SGR_CAD","name":"MANUTENCAO_TERMINAL","comments":"","lastddltime":"13/05/2019 10:55:04","createdtime":"11/05/2019 21:25:54"}
,
{"schema":"ADM_SGR_CAD","name":"MOTIVO_REPROVACAO_ADQUIRENTE","comments":"Respons�vel por armazenar os motivos de reprova��o de credenciamento das adquirentes.","lastddltime":"11/05/2019 23:32:34","createdtime":"11/05/2019 21:25:54"}
,
{"schema":"ADM_SGR_CAD","name":"NOTIFICACAO_MOBILE","comments":"","lastddltime":"17/09/2019 15:34:33","createdtime":"11/05/2019 21:25:55"}
,
{"schema":"ADM_SGR_CAD","name":"NOTIFICACAO_MOBILE_SGR","comments":"","lastddltime":"13/05/2019 10:55:03","createdtime":"11/05/2019 21:25:55"}
,
{"schema":"ADM_SGR_CAD","name":"PERGUNTA_RESPOSTA","comments":"","lastddltime":"11/05/2019 23:32:36","createdtime":"11/05/2019 21:25:55"}
,
{"schema":"ADM_SGR_CAD","name":"PROBLEMA","comments":"","lastddltime":"13/05/2019 10:55:04","createdtime":"11/05/2019 21:25:54"}
,
{"schema":"ADM_SGR_CAD","name":"PROCESSO","comments":"Cadastro de processos","lastddltime":"13/05/2019 10:54:58","createdtime":"11/05/2019 21:25:55"}
,
{"schema":"ADM_SGR_CAD","name":"PROCESSO_HISTORICO","comments":"","lastddltime":"13/05/2019 10:55:02","createdtime":"11/05/2019 21:25:55"}
,
{"schema":"ADM_SGR_CAD","name":"PROCESSO_TEMPLATE","comments":"","lastddltime":"13/05/2019 10:54:55","createdtime":"11/05/2019 21:25:56"}
,
{"schema":"ADM_SGR_CAD","name":"PROTOCOLO","comments":"","lastddltime":"13/05/2019 10:55:02","createdtime":"11/05/2019 21:25:55"}
,
{"schema":"ADM_SGR_CAD","name":"RESCISAO_TERMINAL","comments":"","lastddltime":"13/05/2019 10:55:04","createdtime":"11/05/2019 21:25:54"}
,
{"schema":"ADM_SGR_CAD","name":"SERVICO","comments":"","lastddltime":"13/05/2019 10:54:58","createdtime":"11/05/2019 21:25:55"}
,
{"schema":"ADM_SGR_CAD","name":"SERVICO_BOBINA","comments":"","lastddltime":"13/05/2019 10:55:03","createdtime":"11/05/2019 21:25:55"}
,
{"schema":"ADM_SGR_CAD","name":"SERVICO_GENERICO","comments":"Respons�vel por gerenciar solicita��es de servi�os gen�ricos.","lastddltime":"13/05/2019 10:55:04","createdtime":"11/05/2019 21:25:54"}
,
{"schema":"ADM_SGR_CAD","name":"SLA","comments":"","lastddltime":"22/05/2019 21:02:54","createdtime":"11/05/2019 21:25:56"}
,
{"schema":"ADM_SGR_CAD","name":"SLA_ITEM","comments":"","lastddltime":"13/05/2019 10:54:56","createdtime":"11/05/2019 21:25:56"}
,
{"schema":"ADM_SGR_CAD","name":"SLA_PROCESSAMENTO","comments":"","lastddltime":"13/05/2019 10:54:58","createdtime":"11/05/2019 21:25:55"}
,
{"schema":"ADM_SGR_CAD","name":"SOLICITACAO","comments":"Solicita��o de Credenciamento / Manuten��o / Rescis�o","lastddltime":"22/08/2019 11:18:22","createdtime":"11/05/2019 21:25:55"}
,
{"schema":"ADM_SGR_CAD","name":"SOLUCAO","comments":"","lastddltime":"13/05/2019 10:55:04","createdtime":"11/05/2019 21:25:54"}
,
{"schema":"ADM_SGR_CRED","name":"CONCILIA_DADOS","comments":"","lastddltime":"13/05/2019 10:55:05","createdtime":"11/05/2019 21:25:53"}
,
{"schema":"ADM_SGR_CRED","name":"CONCILIA_DADOS_AGRUPAMENTO","comments":"","lastddltime":"13/05/2019 10:55:09","createdtime":"11/05/2019 21:25:53"}
,
{"schema":"ADM_SGR_CRED","name":"CONTROLE_IMPORTACAO","comments":"Tabela respons�vel por gerenciar a importa��o de arquivos.","lastddltime":"11/05/2019 23:32:29","createdtime":"11/05/2019 21:25:52"}
,
{"schema":"ADM_SGR_CRED","name":"CONTROLE_IMPORTACAO_BIN","comments":"Tabela respons�vel por gerenciar a importa��o de arquivos.","lastddltime":"13/05/2019 10:55:13","createdtime":"11/05/2019 21:25:52"}
,
{"schema":"ADM_SGR_CRED","name":"CRED_AVALIACAO_ADQUIRENTE","comments":"","lastddltime":"13/05/2019 10:55:12","createdtime":"11/05/2019 21:25:53"}
,
{"schema":"ADM_SGR_CRED","name":"CRED_BAN_DOM_BANCARIO","comments":"Relaciona as bandeiras e domicilios banc�rios para pagamento do repasse da adquirente.","lastddltime":"13/05/2019 10:55:13","createdtime":"11/05/2019 21:25:52"}
,
{"schema":"ADM_SGR_CRED","name":"CRED_CONDICAO_COMERCIAL","comments":"","lastddltime":"13/05/2019 10:55:11","createdtime":"11/05/2019 21:25:53"}
,
{"schema":"ADM_SGR_CRED","name":"CRED_DETALHE_CREDITO","comments":"","lastddltime":"13/05/2019 10:55:11","createdtime":"11/05/2019 21:25:53"}
,
{"schema":"ADM_SGR_CRED","name":"CRED_DOCUMENTO","comments":"Tabela para registrar Documentos utilizados no credenciamento.","lastddltime":"11/05/2019 23:32:29","createdtime":"11/05/2019 21:25:52"}
,
{"schema":"ADM_SGR_CRED","name":"CRED_DOM_BANCARIO","comments":"","lastddltime":"21/11/2019 10:18:07","createdtime":"11/05/2019 21:25:53"}
,
{"schema":"ADM_SGR_CRED","name":"CREDENCIAMENTO","comments":"","lastddltime":"13/05/2019 10:55:05","createdtime":"11/05/2019 21:25:53"}
,
{"schema":"ADM_SGR_CRED","name":"CREDENCIAMENTO_CANAL_VENDA","comments":"Tabela com registros de canal venda do protocolo","lastddltime":"13/05/2019 10:55:14","createdtime":"11/05/2019 21:25:51"}
,
{"schema":"ADM_SGR_CRED","name":"CREDENCIAMENTO_CONSULTOR","comments":"","lastddltime":"13/05/2019 10:55:06","createdtime":"11/05/2019 21:25:53"}
,
{"schema":"ADM_SGR_CRED","name":"CREDENCIAMENTO_CONTATO","comments":"","lastddltime":"13/05/2019 10:55:06","createdtime":"11/05/2019 21:25:53"}
,
{"schema":"ADM_SGR_CRED","name":"CREDENCIAMENTO_CONTATO_ITEM","comments":"","lastddltime":"13/05/2019 10:55:06","createdtime":"11/05/2019 21:25:53"}
,
{"schema":"ADM_SGR_CRED","name":"CREDENCIAMENTO_ENDERECO","comments":"","lastddltime":"13/05/2019 10:55:06","createdtime":"11/05/2019 21:25:53"}
,
{"schema":"ADM_SGR_CRED","name":"CREDENCIAMENTO_FUNCIONAMENTO","comments":"Armazena o hor�rio de funcionamento do estabelecimento para uso no processo de credencimento.","lastddltime":"13/05/2019 10:55:12","createdtime":"11/05/2019 21:25:52"}
,
{"schema":"ADM_SGR_CRED","name":"CREDENCIAMENTO_MOBILE","comments":"","lastddltime":"13/05/2019 10:55:10","createdtime":"11/05/2019 21:25:53"}
,
{"schema":"ADM_SGR_CRED","name":"CREDENCIAMENTO_OPERADOR","comments":"","lastddltime":"13/05/2019 10:55:06","createdtime":"11/05/2019 21:25:53"}
,
{"schema":"ADM_SGR_CRED","name":"CREDENCIAMENTO_OPERADOR_PAPEL","comments":"","lastddltime":"13/05/2019 10:55:06","createdtime":"11/05/2019 21:25:53"}
,
{"schema":"ADM_SGR_CRED","name":"CREDENCIAMENTO_REDE","comments":"","lastddltime":"13/05/2019 10:55:06","createdtime":"11/05/2019 21:25:53"}
,
{"schema":"ADM_SGR_CRED","name":"CREDENCIAMENTO_SOCIO","comments":"","lastddltime":"13/05/2019 10:55:06","createdtime":"11/05/2019 21:25:53"}
,
{"schema":"ADM_SGR_CRED","name":"CREDENCIAMENTO_TEF","comments":"","lastddltime":"13/05/2019 10:55:06","createdtime":"11/05/2019 21:25:53"}
,
{"schema":"ADM_SGR_CRED","name":"CRED_EQUIPAMENTO","comments":"","lastddltime":"13/05/2019 10:55:11","createdtime":"11/05/2019 21:25:53"}
,
{"schema":"ADM_SGR_CRED","name":"CRED_EQUIPAMENTO_ADQUIRENTE","comments":"Credenciamento de equipamentos","lastddltime":"13/05/2019 10:55:13","createdtime":"11/05/2019 21:25:52"}
,
{"schema":"ADM_SGR_CRED","name":"CRED_EQUIPAMENTO_CANCELADO","comments":"Descri��o da tabela.","lastddltime":"13/05/2019 10:55:14","createdtime":"11/05/2019 21:25:52"}
,
{"schema":"ADM_SGR_CRED","name":"CRED_NFE_NEGOCIO","comments":"NFE por Neg�cio","lastddltime":"13/05/2019 10:55:14","createdtime":"11/05/2019 21:25:52"}
,
{"schema":"ADM_SGR_CRED","name":"CRED_TAXA_EXTRA","comments":"Credenciamento da taxa extra","lastddltime":"13/05/2019 10:55:14","createdtime":"11/05/2019 21:25:52"}
,
{"schema":"ADM_SGR_CRED","name":"ITEM_RESPOSTA_BIN_VAPP","comments":"Armazenar itens de resposta da BIN.","lastddltime":"13/05/2019 10:55:13","createdtime":"11/05/2019 21:25:52"}
,
{"schema":"ADM_SGR_CRED","name":"LOG_ACESSO_EXTERNO","comments":"","lastddltime":"13/05/2019 10:55:09","createdtime":"11/05/2019 21:25:53"}
,
{"schema":"ADM_SGR_CRED","name":"MOTIVO_RECUSA_BIN","comments":"Tabela respons�vel por gerenciar as corre��es do Arquivo de Exporta��o SGR - VAPP.","lastddltime":"11/05/2019 23:32:29","createdtime":"11/05/2019 21:25:52"}
,
{"schema":"ADM_SGR_CRED","name":"MOTIVO_RECUSA_PROTOCOLO","comments":"Tabela respons�vel por gerenciar a importa��o de arquivos.","lastddltime":"11/05/2019 23:32:29","createdtime":"11/05/2019 21:25:52"}
,
{"schema":"ADM_SGR_CRED","name":"OBSERVACAO","comments":"","lastddltime":"13/05/2019 10:55:10","createdtime":"11/05/2019 21:25:53"}
,
{"schema":"ADM_SGR_CRED","name":"ORDEM_SERVICO","comments":"","lastddltime":"13/05/2019 10:55:06","createdtime":"11/05/2019 21:25:53"}
,
{"schema":"ADM_SGR_CRED","name":"PROTOCOLO_EXPORTADO","comments":"Armazenar os protocolos exportardos.","lastddltime":"11/05/2019 23:32:29","createdtime":"11/05/2019 21:25:52"}
,
{"schema":"ADM_SGR_CRED","name":"PROTOCOLO_EXPORTADO_ITEM","comments":"","lastddltime":"13/05/2019 10:55:13","createdtime":"11/05/2019 21:25:52"}
,
{"schema":"ADM_SGR_CRED","name":"RESCISAO","comments":"","lastddltime":"13/05/2019 10:55:09","createdtime":"11/05/2019 21:25:53"}
,
{"schema":"ADM_SGR_CRED","name":"TEF_CONTATO","comments":"","lastddltime":"13/05/2019 10:55:08","createdtime":"11/05/2019 21:25:53"}
,
{"schema":"ADM_SGR_CRED","name":"TEF_FORNECEDOR","comments":"","lastddltime":"13/05/2019 10:55:08","createdtime":"11/05/2019 21:25:53"}
,
{"schema":"ADM_SGR_CRED","name":"TEF_REFERENCIA_BANCARIA","comments":"","lastddltime":"13/05/2019 10:55:08","createdtime":"11/05/2019 21:25:53"}
,
{"schema":"ADM_SGV_ACE","name":"ACAO","comments":"Entidade que identifica a a��o que uma Role (Permiss�o) executa no sistema","lastddltime":"11/05/2019 23:29:08","createdtime":"11/05/2019 21:25:43"}
,
{"schema":"ADM_SGV_ACE","name":"ACESSO","comments":"Empresas as quais o usu�rio est� vinculado para acesso no sistema","lastddltime":"14/10/2019 21:02:55","createdtime":"11/05/2019 21:25:44"}
,
{"schema":"ADM_SGV_ACE","name":"BLOQUEIO_USUARIO","comments":"Entidade que armazena o hist�rico de bloqueio do usu�rio, registrando os bloqueios e desbloqueios do mesmo.","lastddltime":"13/05/2019 10:56:21","createdtime":"11/05/2019 21:25:43"}
,
{"schema":"ADM_SGV_ACE","name":"CODIGO_ATIVACAO_SGV_PDV","comments":"","lastddltime":"13/05/2019 10:56:23","createdtime":"11/05/2019 21:25:43"}
,
{"schema":"ADM_SGV_ACE","name":"CONSULTOR_ACESSO_MV","comments":"Entidade que armazena o acesso do consultor","lastddltime":"11/05/2019 23:29:08","createdtime":"11/05/2019 21:25:43"}
,
{"schema":"ADM_SGV_ACE","name":"DEPARTAMENTO","comments":"","lastddltime":"11/05/2019 23:28:42","createdtime":"11/05/2019 21:25:43"}
,
{"schema":"ADM_SGV_ACE","name":"EMPRESA_DEPARTAMENTO","comments":"","lastddltime":"13/05/2019 10:56:24","createdtime":"11/05/2019 21:25:42"}
,
{"schema":"ADM_SGV_ACE","name":"ENVIO_EMAIL_USUARIO","comments":"Entidade respons�vel por armazenar informa��es de envio de e-mail do usu�rio","lastddltime":"30/07/2019 11:15:56","createdtime":"11/05/2019 21:25:43"}
,
{"schema":"ADM_SGV_ACE","name":"ESTABELECIMENTO_EMPRESA_TOKEN","comments":"Descri��o da tabela.","lastddltime":"29/05/2019 09:58:37","createdtime":"29/05/2019 09:58:36"}
,
{"schema":"ADM_SGV_ACE","name":"ESTRUTURA_ACESSO","comments":"Armazena a estrutura de acesso","lastddltime":"09/08/2019 16:34:03","createdtime":"11/05/2019 21:25:43"}
,
{"schema":"ADM_SGV_ACE","name":"EXPEDIENTE","comments":"Entidade que vai armazenar os registros do expediente do usu�rio do sistema / Consultor","lastddltime":"13/05/2019 10:56:20","createdtime":"11/05/2019 21:25:44"}
,
{"schema":"ADM_SGV_ACE","name":"FAVORITO","comments":"Entidade respons�vel por armazenar os favoritos do usu�rio","lastddltime":"13/05/2019 10:56:21","createdtime":"11/05/2019 21:25:43"}
,
{"schema":"ADM_SGV_ACE","name":"FUNCIONALIDADE","comments":"Entidade respons�vel por armazenar as funcionalidade do sistema","lastddltime":"11/05/2019 23:29:08","createdtime":"11/05/2019 21:25:43"}
,
{"schema":"ADM_SGV_ACE","name":"FUNCIONALIDADE_PERM","comments":"Entidade respons�vel por armazenar as funcionalidade_permissao do sistema","lastddltime":"13/05/2019 10:56:22","createdtime":"11/05/2019 21:25:43"}
,
{"schema":"ADM_SGV_ACE","name":"LOG_ACESSO","comments":"Armazena o log de acesso","lastddltime":"13/05/2019 10:56:21","createdtime":"11/05/2019 21:25:43"}
,
{"schema":"ADM_SGV_ACE","name":"LOG_USUARIO","comments":"Armazena um log do usu�rio","lastddltime":"13/05/2019 10:56:23","createdtime":"11/05/2019 21:25:43"}
,
{"schema":"ADM_SGV_ACE","name":"MC_PAPEL_PERMISSAO","comments":"Entidade respons�vel por complementar o PAPEL_PERMNISSAO","lastddltime":"11/05/2019 23:29:08","createdtime":"11/05/2019 21:25:43"}
,
{"schema":"ADM_SGV_ACE","name":"MENU","comments":"Menu de op��es do sistema","lastddltime":"13/05/2019 10:56:21","createdtime":"11/05/2019 21:25:44"}
,
{"schema":"ADM_SGV_ACE","name":"PAPEL","comments":"Cadastro dos pap�is dispon�veis para o sistema","lastddltime":"08/10/2019 21:03:28","createdtime":"11/05/2019 21:25:45"}
,
{"schema":"ADM_SGV_ACE","name":"PAPEL_PERMISSAO","comments":"Classe relacional entre papel e permiss�o, que identifica o acesso","lastddltime":"09/08/2019 16:34:03","createdtime":"11/05/2019 21:25:44"}
,
{"schema":"ADM_SGV_ACE","name":"PARAMETRO_SENHA","comments":"Classe respons�vel por guardar informa��es que asseguram a autentica��o e controle das senhas","lastddltime":"11/05/2019 23:29:08","createdtime":"11/05/2019 21:25:59"}
,
{"schema":"ADM_SGV_ACE","name":"PERGUNTA_SEGURANCA","comments":"","lastddltime":"11/05/2019 23:28:42","createdtime":"11/05/2019 21:25:43"}
,
{"schema":"ADM_SGV_ACE","name":"PERMISSAO","comments":"Funcionalidades do sistema de acordo com o perfil","lastddltime":"23/08/2019 21:01:26","createdtime":"11/05/2019 21:25:45"}
,
{"schema":"ADM_SGV_ACE","name":"PREFERENCIA","comments":"Entidade respons�vel por armazenar as prefer�ncias que o usu�rio ter� no sistema","lastddltime":"13/05/2019 10:56:20","createdtime":"11/05/2019 21:25:44"}
,
{"schema":"ADM_SGV_ACE","name":"RESPOSTA_SEGURANCA","comments":"","lastddltime":"13/05/2019 10:56:23","createdtime":"11/05/2019 21:25:43"}
,
{"schema":"ADM_SGV_ACE","name":"SENHA_USUARIO","comments":"Entidade que ir� armazenar as senhas utilizadas por um determinado usu�rio dentro do sistema","lastddltime":"13/05/2019 10:56:20","createdtime":"11/05/2019 21:25:44"}
,
{"schema":"ADM_SGV_ACE","name":"SISTEMA","comments":"Armazena informa��es do sistema","lastddltime":"11/05/2019 23:28:43","createdtime":"11/05/2019 21:25:43"}
,
{"schema":"ADM_SGV_ACE","name":"USUARIO","comments":"Cadastro de usu�rios do sistema","lastddltime":"14/10/2019 21:02:33","createdtime":"11/05/2019 21:25:44"}
,
{"schema":"ADM_SGV_ACE","name":"USUARIO_DEPARTAMENTO","comments":"","lastddltime":"13/05/2019 10:56:24","createdtime":"11/05/2019 21:25:43"}
,
{"schema":"ADM_SGV_ACE","name":"USUARIO_SISTEMA","comments":"Entidade respons�vel pelo relacionamento do usu�rio com o sistema","lastddltime":"13/05/2019 10:56:23","createdtime":"11/05/2019 21:25:43"}
,
{"schema":"ADM_SGV_ALO","name":"ALOCACAO_CHIP_EMPRESA","comments":"Cadastro de aloca��o do Chip � uma empresa","lastddltime":"13/05/2019 11:21:39","createdtime":"12/05/2019 10:26:28"}
,
{"schema":"ADM_SGV_ALO","name":"ALOCACAO_TERMINAL_EMPRESA","comments":"Cadastro de aloca��o do terminal � uma empresa","lastddltime":"13/05/2019 11:21:37","createdtime":"12/05/2019 10:26:30"}
,
{"schema":"ADM_SGV_ALO","name":"ALOCACAO_TERMINAL_ESTAB","comments":"Cadastro de aloca��o de terminal a um estabelecimento.","lastddltime":"13/05/2019 11:21:37","createdtime":"12/05/2019 10:26:29"}
,
{"schema":"ADM_SGV_ALO","name":"ALOCACAO_TERMINAL_ESTAB_BKP","comments":"","lastddltime":"12/05/2019 10:26:28","createdtime":"12/05/2019 10:26:28"}
,
{"schema":"ADM_SGV_ALO","name":"AUTORIZACAO_COMPARTILHA","comments":"Empresas com as quais a empresa poder� efetuar o compartilhamento.","lastddltime":"13/05/2019 11:21:39","createdtime":"12/05/2019 10:26:28"}
,
{"schema":"ADM_SGV_ALO","name":"CONSULTOR_ESTAB_EMPRESA","comments":"Entidade respons�vel pelo relacionamento do consulto e ESTABELECIMENTO_EMPRESA","lastddltime":"13/05/2019 11:21:40","createdtime":"12/05/2019 10:26:28"}
,
{"schema":"ADM_SGV_ALO","name":"ITEM_SOLICITACAO","comments":"Terminais que far�o parte dessa solicita��o de compartilhamento, podendo aprovar um ou mais itens","lastddltime":"13/05/2019 11:21:38","createdtime":"12/05/2019 10:26:29"}
,
{"schema":"ADM_SGV_ALO","name":"MC_ALOC_TERM","comments":"Entidade � modelo complementar que armazena a aloca��o de terminais","lastddltime":"12/05/2019 10:26:45","createdtime":"12/05/2019 10:26:28"}
,
{"schema":"ADM_SGV_ALO","name":"NEGOCIACAO_CHIP","comments":"Cadastro de per�odo de negocia��o para Chip","lastddltime":"13/05/2019 11:21:39","createdtime":"12/05/2019 10:26:28"}
,
{"schema":"ADM_SGV_ALO","name":"NEGOCIACAO_COMPARTILHA","comments":"Entidade respons�vel por armazenar as negocia��es compartilhadas","lastddltime":"13/05/2019 11:21:41","createdtime":"12/05/2019 10:26:27"}
,
{"schema":"ADM_SGV_ALO","name":"NEGOCIACAO_TERMINAL","comments":"Cadastro de per�odo de negocia��o desse terminal","lastddltime":"13/05/2019 11:21:36","createdtime":"12/05/2019 10:26:30"}
,
{"schema":"ADM_SGV_ALO","name":"RELACIONAMENTO_CONSULTOR","comments":"Armazena o relacionamento do consultor","lastddltime":"13/05/2019 11:21:41","createdtime":"12/05/2019 10:26:28"}
,
{"schema":"ADM_SGV_ALO","name":"SOLICITACAO_COMPARTILHA","comments":"Cadastro da solicita��o do compartilhamento de terminais pela empresa que deseja compartilhar seus terminais, sendo que essa solicita��o dever� ser aprovada pelo Integrador","lastddltime":"13/05/2019 11:21:37","createdtime":"12/05/2019 10:26:29"}
,
{"schema":"ADM_SGV_ALO","name":"SOLICITACAO_COMP_ESTOQUE","comments":"Entidade que armazena a solicita��o de compartilhamento de estoque, entre as empresas fornecedora e as compradoras","lastddltime":"13/05/2019 11:21:38","createdtime":"12/05/2019 10:26:29"}
,
{"schema":"ADM_SGV_ALO","name":"SOLICIT_COMP_ESTOQUE_ITEM","comments":"Identifica��o dos itens que fazem parte de uma solicita��o de compartilhamento de estoque. Estado da empresa Fornecedora Estado da empresa Compradora","lastddltime":"13/05/2019 11:21:38","createdtime":"12/05/2019 10:26:29"}
,
{"schema":"ADM_SGV_ALO","name":"VISITA_CONSULTOR","comments":"Armazena as visitas do consultor","lastddltime":"10/10/2019 09:30:23","createdtime":"12/05/2019 10:26:28"}
,
{"schema":"ADM_SGV_AUD","name":"ACAO_AUD","comments":"","lastddltime":"13/05/2019 11:43:27","createdtime":"12/05/2019 11:02:46"}
,
{"schema":"ADM_SGV_AUD","name":"ACAO_GERENCIAL_AUD","comments":"","lastddltime":"13/05/2019 11:43:00","createdtime":"12/05/2019 11:02:50"}
,
{"schema":"ADM_SGV_AUD","name":"ACAO_GERENCIAL_EMPRESA_AUD","comments":"","lastddltime":"13/05/2019 11:43:29","createdtime":"12/05/2019 11:02:45"}
,
{"schema":"ADM_SGV_AUD","name":"ACAO_GERENCIAL_MODELO_AUD","comments":"","lastddltime":"13/05/2019 11:43:29","createdtime":"12/05/2019 11:02:45"}
,
{"schema":"ADM_SGV_AUD","name":"ACAO_PRODUTO_AUD","comments":"","lastddltime":"13/05/2019 11:43:00","createdtime":"12/05/2019 11:02:50"}
,
{"schema":"ADM_SGV_AUD","name":"ACAO_PRODUTO_EMPRESA_AUD","comments":"","lastddltime":"13/05/2019 11:43:25","createdtime":"12/05/2019 11:02:46"}
,
{"schema":"ADM_SGV_AUD","name":"ACAO_PRODUTO_EMP_VALOR_AUD","comments":"","lastddltime":"13/05/2019 11:43:29","createdtime":"12/05/2019 11:02:45"}
,
{"schema":"ADM_SGV_AUD","name":"ACAO_PRODUTO_VENDA_AUD","comments":"","lastddltime":"13/05/2019 11:43:25","createdtime":"12/05/2019 11:02:46"}
,
{"schema":"ADM_SGV_AUD","name":"ACESSO_AUD","comments":"","lastddltime":"13/05/2019 11:42:55","createdtime":"12/05/2019 11:02:50"}
,
{"schema":"ADM_SGV_AUD","name":"AGENDA_LANCAMENTO_AUD","comments":"","lastddltime":"13/05/2019 11:43:16","createdtime":"12/05/2019 11:02:47"}
,
{"schema":"ADM_SGV_AUD","name":"AGENDA_TELECARGA_AUD","comments":"","lastddltime":"13/05/2019 11:43:13","createdtime":"12/05/2019 11:02:48"}
,
{"schema":"ADM_SGV_AUD","name":"AGRUPAMENTO_AUD","comments":"","lastddltime":"13/05/2019 11:43:00","createdtime":"12/05/2019 11:02:50"}
,
{"schema":"ADM_SGV_AUD","name":"ALOCACAO_CHIP_EMPRESA_AUD","comments":"","lastddltime":"13/05/2019 11:42:57","createdtime":"12/05/2019 11:02:50"}
,
{"schema":"ADM_SGV_AUD","name":"ALOCACAO_TERMINAL_EMPRESA_AUD","comments":"","lastddltime":"13/05/2019 11:42:57","createdtime":"12/05/2019 11:02:50"}
,
{"schema":"ADM_SGV_AUD","name":"ALOCACAO_TERMINAL_ESTAB_AUD","comments":"","lastddltime":"13/05/2019 11:42:58","createdtime":"12/05/2019 11:02:50"}
,
{"schema":"ADM_SGV_AUD","name":"AMBIENTE_AUD","comments":"","lastddltime":"13/05/2019 11:43:22","createdtime":"12/05/2019 11:02:47"}
,
{"schema":"ADM_SGV_AUD","name":"APLICATIVO_ATUALIZACAO_AUD","comments":"","lastddltime":"13/05/2019 11:43:23","createdtime":"12/05/2019 11:02:47"}
,
{"schema":"ADM_SGV_AUD","name":"APLICATIVO_AUD","comments":"","lastddltime":"13/05/2019 11:43:13","createdtime":"12/05/2019 11:02:48"}
,
{"schema":"ADM_SGV_AUD","name":"APLICATIVO_SO_AUD","comments":"","lastddltime":"13/05/2019 11:43:13","createdtime":"12/05/2019 11:02:48"}
,
{"schema":"ADM_SGV_AUD","name":"ATIVIDADE_AUD","comments":"","lastddltime":"13/05/2019 11:43:00","createdtime":"12/05/2019 11:02:50"}
,
{"schema":"ADM_SGV_AUD","name":"ATIVIDADE_FORNECEDOR_AUD","comments":"","lastddltime":"13/05/2019 11:43:00","createdtime":"12/05/2019 11:02:50"}
,
{"schema":"ADM_SGV_AUD","name":"ATIVIDADE_FORNECEDOR_CNAE_AUD","comments":"","lastddltime":"13/05/2019 11:43:01","createdtime":"12/05/2019 11:02:49"}
,
{"schema":"ADM_SGV_AUD","name":"AUDITORIA","comments":"","lastddltime":"02/12/2019 10:30:28","createdtime":"12/05/2019 11:02:29"}
,
{"schema":"ADM_SGV_AUD","name":"AUDITORIA_ITEM","comments":"","lastddltime":"02/12/2019 10:30:28","createdtime":"12/05/2019 11:02:32"}
,
{"schema":"ADM_SGV_AUD","name":"AUTORIZACAO_COMPARTILHA_AUD","comments":"","lastddltime":"13/05/2019 11:42:58","createdtime":"12/05/2019 11:02:50"}
,
{"schema":"ADM_SGV_AUD","name":"BAIRRO_AUD","comments":"","lastddltime":"13/05/2019 11:43:07","createdtime":"12/05/2019 11:02:49"}
,
{"schema":"ADM_SGV_AUD","name":"BAIXA_AUD","comments":"","lastddltime":"13/05/2019 11:43:09","createdtime":"12/05/2019 11:02:48"}
,
{"schema":"ADM_SGV_AUD","name":"BANCO_AUD","comments":"","lastddltime":"13/05/2019 11:43:09","createdtime":"12/05/2019 11:02:48"}
,
{"schema":"ADM_SGV_AUD","name":"BANCO_DET_COBRANCA_AUD","comments":"","lastddltime":"13/05/2019 11:43:09","createdtime":"12/05/2019 11:02:48"}
,
{"schema":"ADM_SGV_AUD","name":"BINARIO_AUD","comments":"","lastddltime":"13/05/2019 11:43:24","createdtime":"12/05/2019 11:02:46"}
,
{"schema":"ADM_SGV_AUD","name":"BLOQUEIO_AUD","comments":"","lastddltime":"13/05/2019 11:43:16","createdtime":"12/05/2019 11:02:47"}
,
{"schema":"ADM_SGV_AUD","name":"BLOQUEIO_EMPRESA_AUD","comments":"","lastddltime":"13/05/2019 11:43:16","createdtime":"12/05/2019 11:02:47"}
,
{"schema":"ADM_SGV_AUD","name":"BLOQUEIO_PIN_AUD","comments":"","lastddltime":"13/05/2019 11:43:16","createdtime":"12/05/2019 11:02:47"}
,
{"schema":"ADM_SGV_AUD","name":"BLOQUEIO_USUARIO_AUD","comments":"","lastddltime":"13/05/2019 11:42:55","createdtime":"12/05/2019 11:02:50"}
,
{"schema":"ADM_SGV_AUD","name":"BONUS_AUD","comments":"","lastddltime":"13/05/2019 11:43:01","createdtime":"12/05/2019 11:02:49"}
,
{"schema":"ADM_SGV_AUD","name":"CAMPO","comments":"","lastddltime":"12/05/2019 12:54:09","createdtime":"12/05/2019 11:02:45"}
,
{"schema":"ADM_SGV_AUD","name":"CANAL_VENDA_ESTAB_AUD","comments":"","lastddltime":"13/05/2019 11:43:22","createdtime":"12/05/2019 11:02:47"}
,
{"schema":"ADM_SGV_AUD","name":"CANAL_VENDA_FORN_NEG_AUD","comments":"","lastddltime":"13/05/2019 11:43:01","createdtime":"12/05/2019 11:02:49"}
,
{"schema":"ADM_SGV_AUD","name":"CATEGORIA_VALOR_BONUS_AUD","comments":"","lastddltime":"13/05/2019 11:43:26","createdtime":"12/05/2019 11:02:46"}
,
{"schema":"ADM_SGV_AUD","name":"CEP_AUD","comments":"","lastddltime":"13/05/2019 11:43:07","createdtime":"12/05/2019 11:02:49"}
,
{"schema":"ADM_SGV_AUD","name":"CHIP_AUD","comments":"","lastddltime":"13/05/2019 11:43:13","createdtime":"12/05/2019 11:02:48"}
,
{"schema":"ADM_SGV_AUD","name":"CICLO_RECEBIMENTO_AUD","comments":"","lastddltime":"13/05/2019 11:43:09","createdtime":"12/05/2019 11:02:48"}
,
{"schema":"ADM_SGV_AUD","name":"CIDADE_AUD","comments":"","lastddltime":"13/05/2019 11:43:07","createdtime":"12/05/2019 11:02:49"}
,
{"schema":"ADM_SGV_AUD","name":"COBRANCA_AUD","comments":"","lastddltime":"13/05/2019 11:43:10","createdtime":"12/05/2019 11:02:48"}
,
{"schema":"ADM_SGV_AUD","name":"CODIGO_RESPOSTA_AUD","comments":"","lastddltime":"13/05/2019 11:43:20","createdtime":"12/05/2019 11:02:47"}
,
{"schema":"ADM_SGV_AUD","name":"COMPOSICAO_ARQUIVO_PIN_AUD","comments":"","lastddltime":"13/05/2019 11:43:17","createdtime":"12/05/2019 11:02:47"}
,
{"schema":"ADM_SGV_AUD","name":"CONCESSIONARIA_AUD","comments":"","lastddltime":"13/05/2019 11:43:21","createdtime":"12/05/2019 11:02:47"}
,
{"schema":"ADM_SGV_AUD","name":"CONFIGURACAO_EMPRESA_AUD","comments":"","lastddltime":"13/05/2019 11:43:24","createdtime":"12/05/2019 11:02:46"}
,
{"schema":"ADM_SGV_AUD","name":"CONFIGURACAO_EMPRESA_AUD_2207","comments":"","lastddltime":"12/05/2019 11:02:46","createdtime":"12/05/2019 11:02:46"}
,
{"schema":"ADM_SGV_AUD","name":"CONSULTOR_ESTAB_EMPRESA_AUD","comments":"","lastddltime":"13/05/2019 11:42:58","createdtime":"12/05/2019 11:02:50"}
,
{"schema":"ADM_SGV_AUD","name":"CONTATO_AUD","comments":"","lastddltime":"13/05/2019 11:43:01","createdtime":"12/05/2019 11:02:49"}
,
{"schema":"ADM_SGV_AUD","name":"CONTROLE_SALDO_AUD","comments":"","lastddltime":"13/05/2019 11:43:28","createdtime":"12/05/2019 11:02:46"}
,
{"schema":"ADM_SGV_AUD","name":"CONVENIO_BANCARIO_AUD","comments":"","lastddltime":"13/05/2019 11:43:10","createdtime":"12/05/2019 11:02:48"}
,
{"schema":"ADM_SGV_AUD","name":"DADOS_GERAIS_AUD","comments":"","lastddltime":"13/05/2019 11:43:21","createdtime":"12/05/2019 11:02:47"}
,
{"schema":"ADM_SGV_AUD","name":"DESTINACAO_VALOR_AUD","comments":"","lastddltime":"13/05/2019 11:43:17","createdtime":"12/05/2019 11:02:47"}
,
{"schema":"ADM_SGV_AUD","name":"DETALHE_PROCESSAMENTO_AUD","comments":"","lastddltime":"13/05/2019 11:43:20","createdtime":"12/05/2019 11:02:47"}
,
{"schema":"ADM_SGV_AUD","name":"DOMICILIO_BANCARIO_AUD","comments":"","lastddltime":"13/05/2019 11:43:10","createdtime":"12/05/2019 11:02:48"}
,
{"schema":"ADM_SGV_AUD","name":"EMAIL_ENVIO_AUD","comments":"","lastddltime":"13/05/2019 11:43:27","createdtime":"12/05/2019 11:02:46"}
,
{"schema":"ADM_SGV_AUD","name":"EMPRESA_AUD","comments":"","lastddltime":"13/05/2019 11:43:01","createdtime":"12/05/2019 11:03:12"}
,
{"schema":"ADM_SGV_AUD","name":"EMPRESA_OPERACAO_AUD","comments":"","lastddltime":"13/05/2019 11:43:23","createdtime":"12/05/2019 11:02:46"}
,
{"schema":"ADM_SGV_AUD","name":"EMPRESA_RESPONSAVEL_AUD","comments":"","lastddltime":"13/05/2019 11:43:13","createdtime":"12/05/2019 11:02:48"}
,
{"schema":"ADM_SGV_AUD","name":"ENDERECO_AUD","comments":"","lastddltime":"13/05/2019 11:43:08","createdtime":"12/05/2019 11:02:49"}
,
{"schema":"ADM_SGV_AUD","name":"ENTIDADE_REVISAO","comments":"","lastddltime":"12/05/2019 12:54:14","createdtime":"12/05/2019 11:02:50"}
,
{"schema":"ADM_SGV_AUD","name":"ESTABELECIMENTO_AGRUPADO_AUD","comments":"","lastddltime":"13/05/2019 11:43:02","createdtime":"12/05/2019 11:02:49"}
,
{"schema":"ADM_SGV_AUD","name":"ESTABELECIMENTO_AUD","comments":"","lastddltime":"13/05/2019 11:43:01","createdtime":"12/05/2019 11:02:49"}
,
{"schema":"ADM_SGV_AUD","name":"ESTABELECIMENTO_EMPRESA_AUD","comments":"","lastddltime":"13/05/2019 11:43:02","createdtime":"12/05/2019 11:02:49"}
,
{"schema":"ADM_SGV_AUD","name":"ESTADO_ATUACAO_AUD","comments":"","lastddltime":"13/05/2019 11:43:02","createdtime":"12/05/2019 11:02:49"}
,
{"schema":"ADM_SGV_AUD","name":"ESTADO_AUD","comments":"","lastddltime":"13/05/2019 11:43:08","createdtime":"12/05/2019 11:02:49"}
,
{"schema":"ADM_SGV_AUD","name":"ESTOQUE_PIN_AUD","comments":"","lastddltime":"13/05/2019 11:43:17","createdtime":"12/05/2019 11:02:47"}
,
{"schema":"ADM_SGV_AUD","name":"ESTORNO_PIN_AUD","comments":"","lastddltime":"13/05/2019 11:43:17","createdtime":"12/05/2019 11:02:47"}
,
{"schema":"ADM_SGV_AUD","name":"EXPEDIENTE_AUD","comments":"","lastddltime":"13/05/2019 11:42:55","createdtime":"12/05/2019 11:02:50"}
,
{"schema":"ADM_SGV_AUD","name":"FABRICANTE_AUD","comments":"","lastddltime":"13/05/2019 11:43:14","createdtime":"12/05/2019 11:02:48"}
,
{"schema":"ADM_SGV_AUD","name":"FAIXA_VALOR_AUD","comments":"","lastddltime":"13/05/2019 11:43:21","createdtime":"12/05/2019 11:02:47"}
,
{"schema":"ADM_SGV_AUD","name":"FAVORITO_AUD","comments":"","lastddltime":"13/05/2019 11:42:55","createdtime":"12/05/2019 11:02:50"}
,
{"schema":"ADM_SGV_AUD","name":"FERIADO_AUD","comments":"","lastddltime":"13/05/2019 11:43:08","createdtime":"12/05/2019 11:02:49"}
,
{"schema":"ADM_SGV_AUD","name":"FILIAL_AUD","comments":"","lastddltime":"13/05/2019 11:43:21","createdtime":"12/05/2019 11:02:47"}
,
{"schema":"ADM_SGV_AUD","name":"FILIAL_CODIGO_AREA_AUD","comments":"","lastddltime":"13/05/2019 11:43:21","createdtime":"12/05/2019 11:02:47"}
,
{"schema":"ADM_SGV_AUD","name":"FORNECEDOR_AUD","comments":"","lastddltime":"13/05/2019 11:43:02","createdtime":"12/05/2019 11:02:49"}
,
{"schema":"ADM_SGV_AUD","name":"FORNECEDOR_NEG_LAYOUT_PIN_AUD","comments":"","lastddltime":"13/05/2019 11:43:17","createdtime":"12/05/2019 11:02:47"}
,
{"schema":"ADM_SGV_AUD","name":"FORNECEDOR_NEGOCIO_AUD","comments":"","lastddltime":"13/05/2019 11:43:02","createdtime":"12/05/2019 11:02:49"}
,
{"schema":"ADM_SGV_AUD","name":"FORNECEDOR_SGV_AUD","comments":"","lastddltime":"13/05/2019 11:43:21","createdtime":"12/05/2019 11:02:47"}
,
{"schema":"ADM_SGV_AUD","name":"FUNCIONALIDADE_AUD","comments":"","lastddltime":"13/05/2019 11:43:28","createdtime":"12/05/2019 11:02:46"}
,
{"schema":"ADM_SGV_AUD","name":"FUNCIONALIDADE_PERM_AUD","comments":"","lastddltime":"13/05/2019 11:43:28","createdtime":"12/05/2019 11:02:45"}
,
{"schema":"ADM_SGV_AUD","name":"GRUPO_RAMO_ATIVIDADE_AUD","comments":"","lastddltime":"13/05/2019 11:43:03","createdtime":"12/05/2019 11:02:49"}
,
{"schema":"ADM_SGV_AUD","name":"GRUPO_VENDA_AUD","comments":"","lastddltime":"13/05/2019 11:43:18","createdtime":"12/05/2019 11:02:47"}
,
{"schema":"ADM_SGV_AUD","name":"GRUPO_VENDA_AUTORIZADO_AUD","comments":"","lastddltime":"13/05/2019 11:43:18","createdtime":"12/05/2019 11:02:47"}
,
{"schema":"ADM_SGV_AUD","name":"GRUPO_VENDA_FORNECEDOR_AUD","comments":"","lastddltime":"13/05/2019 11:43:23","createdtime":"12/05/2019 11:02:47"}
,
{"schema":"ADM_SGV_AUD","name":"HISTORICO_ATENDIMENTO_AUD","comments":"","lastddltime":"13/05/2019 11:43:03","createdtime":"12/05/2019 11:02:49"}
,
{"schema":"ADM_SGV_AUD","name":"HISTORICO_CHIP_TERMINAL_AUD","comments":"","lastddltime":"13/05/2019 11:43:14","createdtime":"12/05/2019 11:02:48"}
,
{"schema":"ADM_SGV_AUD","name":"HISTORICO_NUM_SERIE_AUD","comments":"","lastddltime":"13/05/2019 11:43:14","createdtime":"12/05/2019 11:02:48"}
,
{"schema":"ADM_SGV_AUD","name":"IMAGEM_MERCHAN_AUD","comments":"","lastddltime":"13/05/2019 11:43:26","createdtime":"12/05/2019 11:02:46"}
,
{"schema":"ADM_SGV_AUD","name":"INSTRUCAO_ATIVACAO_AUD","comments":"","lastddltime":"13/05/2019 11:43:03","createdtime":"12/05/2019 11:02:49"}
,
{"schema":"ADM_SGV_AUD","name":"INTEGRADOR_AUD","comments":"","lastddltime":"13/05/2019 11:43:03","createdtime":"12/05/2019 11:03:12"}
,
{"schema":"ADM_SGV_AUD","name":"ITEM_AGENDADO_AUD","comments":"","lastddltime":"13/05/2019 11:43:18","createdtime":"12/05/2019 11:02:47"}
,
{"schema":"ADM_SGV_AUD","name":"ITEM_AGENDA_TELECARGA_AUD","comments":"","lastddltime":"13/05/2019 11:43:14","createdtime":"12/05/2019 11:02:48"}
,
{"schema":"ADM_SGV_AUD","name":"ITEM_APLICATIVO_AUD","comments":"","lastddltime":"13/05/2019 11:43:23","createdtime":"12/05/2019 11:02:46"}
,
{"schema":"ADM_SGV_AUD","name":"ITEM_CONTATO_AUD","comments":"","lastddltime":"13/05/2019 11:43:04","createdtime":"12/05/2019 11:02:49"}
,
{"schema":"ADM_SGV_AUD","name":"ITEM_ROTEIRO_AUD","comments":"","lastddltime":"13/05/2019 11:43:04","createdtime":"12/05/2019 11:02:49"}
,
{"schema":"ADM_SGV_AUD","name":"ITEM_SOLICITACAO_AUD","comments":"","lastddltime":"13/05/2019 11:42:58","createdtime":"12/05/2019 11:02:50"}
,
{"schema":"ADM_SGV_AUD","name":"ITEM_SOLICITADO_AUD","comments":"","lastddltime":"13/05/2019 11:43:18","createdtime":"12/05/2019 11:02:47"}
,
{"schema":"ADM_SGV_AUD","name":"ITEM_TRANSFERIDO_AUD","comments":"","lastddltime":"13/05/2019 11:43:18","createdtime":"12/05/2019 11:02:47"}
,
{"schema":"ADM_SGV_AUD","name":"LAYOUT_PIN_AUD","comments":"","lastddltime":"13/05/2019 11:43:18","createdtime":"12/05/2019 11:02:47"}
,
{"schema":"ADM_SGV_AUD","name":"LAYOUT_PIN_COMPOSICAO_AUD","comments":"","lastddltime":"13/05/2019 11:43:19","createdtime":"12/05/2019 11:02:47"}
,
{"schema":"ADM_SGV_AUD","name":"LIMITE_AUD","comments":"","lastddltime":"13/05/2019 11:43:10","createdtime":"12/05/2019 11:02:48"}
,
{"schema":"ADM_SGV_AUD","name":"LIMITE_SALDO_AUD","comments":"","lastddltime":"13/05/2019 11:43:10","createdtime":"12/05/2019 11:02:48"}
,
{"schema":"ADM_SGV_AUD","name":"LOG_ACESSO_AUD","comments":"","lastddltime":"13/05/2019 11:42:56","createdtime":"12/05/2019 11:02:50"}
,
{"schema":"ADM_SGV_AUD","name":"LOG_GRUPO_VENDA_AUD","comments":"","lastddltime":"13/05/2019 11:43:19","createdtime":"12/05/2019 11:02:47"}
,
{"schema":"ADM_SGV_AUD","name":"LOGRADOURO_AUD","comments":"","lastddltime":"13/05/2019 11:43:08","createdtime":"12/05/2019 11:02:49"}
,
{"schema":"ADM_SGV_AUD","name":"LOTE_BAIXA_AUD","comments":"","lastddltime":"13/05/2019 11:43:10","createdtime":"12/05/2019 11:02:48"}
,
{"schema":"ADM_SGV_AUD","name":"LOTE_BAIXA_DOC_BAIXADO_AUD","comments":"","lastddltime":"13/05/2019 11:43:11","createdtime":"12/05/2019 11:02:48"}
,
{"schema":"ADM_SGV_AUD","name":"LOTE_BAIXA_DOCUMENTO_AUD","comments":"","lastddltime":"13/05/2019 11:43:11","createdtime":"12/05/2019 11:02:48"}
,
{"schema":"ADM_SGV_AUD","name":"LOTE_BAIXA_DOCUMENTO_AUD_BKP","comments":"","lastddltime":"12/05/2019 11:02:45","createdtime":"12/05/2019 11:02:45"}
,
{"schema":"ADM_SGV_AUD","name":"LOTE_PIN_AUD","comments":"","lastddltime":"13/05/2019 11:43:19","createdtime":"12/05/2019 11:02:47"}
,
{"schema":"ADM_SGV_AUD","name":"LOTE_PIN_RESUMO_AUD","comments":"","lastddltime":"13/05/2019 11:43:19","createdtime":"12/05/2019 11:02:47"}
,
{"schema":"ADM_SGV_AUD","name":"MENSAGEM_AUD","comments":"","lastddltime":"13/05/2019 11:43:11","createdtime":"12/05/2019 11:02:48"}
,
{"schema":"ADM_SGV_AUD","name":"MENU_AUD","comments":"","lastddltime":"13/05/2019 11:42:56","createdtime":"12/05/2019 11:02:50"}
,
{"schema":"ADM_SGV_AUD","name":"MERCHAN_AUD","comments":"","lastddltime":"13/05/2019 11:43:26","createdtime":"12/05/2019 11:02:46"}
,
{"schema":"ADM_SGV_AUD","name":"MODELO_AUD","comments":"","lastddltime":"13/05/2019 11:43:14","createdtime":"12/05/2019 11:02:48"}
,
{"schema":"ADM_SGV_AUD","name":"MODELO_SIS_OPERACIONAL_AUD","comments":"","lastddltime":"13/05/2019 11:43:14","createdtime":"12/05/2019 11:02:48"}
,
{"schema":"ADM_SGV_AUD","name":"MODELO_TIPO_CONEXAO_AUD","comments":"","lastddltime":"13/05/2019 11:43:15","createdtime":"12/05/2019 11:02:48"}
,
{"schema":"ADM_SGV_AUD","name":"NEGOCIACAO_AUD","comments":"","lastddltime":"13/05/2019 11:43:11","createdtime":"12/05/2019 11:02:48"}
,
{"schema":"ADM_SGV_AUD","name":"NEGOCIACAO_CHIP_AUD","comments":"","lastddltime":"13/05/2019 11:42:58","createdtime":"12/05/2019 11:02:50"}
,
{"schema":"ADM_SGV_AUD","name":"NEGOCIACAO_TERMINAL_AUD","comments":"","lastddltime":"13/05/2019 11:42:58","createdtime":"12/05/2019 11:02:50"}
,
{"schema":"ADM_SGV_AUD","name":"NEGOCIACAO_TITULO_AUD","comments":"","lastddltime":"13/05/2019 11:43:11","createdtime":"12/05/2019 11:02:48"}
,
{"schema":"ADM_SGV_AUD","name":"NEGOCIO_AUD","comments":"","lastddltime":"13/05/2019 11:43:04","createdtime":"12/05/2019 11:02:49"}
,
{"schema":"ADM_SGV_AUD","name":"OCUPACAO_AUD","comments":"","lastddltime":"13/05/2019 11:43:04","createdtime":"12/05/2019 11:02:49"}
,
{"schema":"ADM_SGV_AUD","name":"OPERACAO_AUD","comments":"","lastddltime":"13/05/2019 11:43:23","createdtime":"12/05/2019 11:02:46"}
,
{"schema":"ADM_SGV_AUD","name":"OPERADORA_AUD","comments":"","lastddltime":"13/05/2019 11:43:04","createdtime":"12/05/2019 11:02:49"}
,
{"schema":"ADM_SGV_AUD","name":"PAIS_AUD","comments":"","lastddltime":"13/05/2019 11:43:08","createdtime":"12/05/2019 11:02:49"}
,
{"schema":"ADM_SGV_AUD","name":"PAPEL_AUD","comments":"","lastddltime":"13/05/2019 11:42:56","createdtime":"12/05/2019 11:02:50"}
,
{"schema":"ADM_SGV_AUD","name":"PAPEL_PERMISSAO_AUD","comments":"","lastddltime":"13/05/2019 11:42:56","createdtime":"12/05/2019 11:02:50"}
,
{"schema":"ADM_SGV_AUD","name":"PARAMETRO_AUD","comments":"","lastddltime":"13/05/2019 11:43:22","createdtime":"12/05/2019 11:02:47"}
,
{"schema":"ADM_SGV_AUD","name":"PARAMETRO_COBRANCA_AUD","comments":"","lastddltime":"13/05/2019 11:43:11","createdtime":"12/05/2019 11:02:48"}
,
{"schema":"ADM_SGV_AUD","name":"PARAMETRO_CONCESSIONARIA_AUD","comments":"","lastddltime":"13/05/2019 11:43:22","createdtime":"12/05/2019 11:02:47"}
,
{"schema":"ADM_SGV_AUD","name":"PARAMETRO_CONEXAO_AUD","comments":"","lastddltime":"13/05/2019 11:43:15","createdtime":"12/05/2019 11:02:48"}
,
{"schema":"ADM_SGV_AUD","name":"PARAMETRO_EMPRESA_AUD","comments":"","lastddltime":"13/05/2019 11:43:15","createdtime":"12/05/2019 11:02:48"}
,
{"schema":"ADM_SGV_AUD","name":"PARAMETRO_EMPRESA_TERMINAL_AUD","comments":"","lastddltime":"13/05/2019 11:43:15","createdtime":"12/05/2019 11:02:48"}
,
{"schema":"ADM_SGV_AUD","name":"PARAMETRO_ESTAB_EMPRESA_AUD","comments":"","lastddltime":"13/05/2019 11:43:24","createdtime":"12/05/2019 11:02:46"}
,
{"schema":"ADM_SGV_AUD","name":"PARAMETRO_GERAL_AUD","comments":"","lastddltime":"13/05/2019 11:43:05","createdtime":"12/05/2019 11:02:49"}
,
{"schema":"ADM_SGV_AUD","name":"PARAMETRO_SENHA_AUD","comments":"","lastddltime":"13/05/2019 11:42:56","createdtime":"12/05/2019 11:03:12"}
,
{"schema":"ADM_SGV_AUD","name":"PERMISSAO_AUD","comments":"","lastddltime":"13/05/2019 11:42:56","createdtime":"12/05/2019 11:02:50"}
,
{"schema":"ADM_SGV_AUD","name":"PLANO_NEGOCIACAO_AUD","comments":"","lastddltime":"13/05/2019 11:43:12","createdtime":"12/05/2019 11:02:48"}
,
{"schema":"ADM_SGV_AUD","name":"PLANO_NEGOCIACAO_PARCELA_AUD","comments":"","lastddltime":"13/05/2019 11:43:12","createdtime":"12/05/2019 11:02:48"}
,
{"schema":"ADM_SGV_AUD","name":"PORTFOLIO_AUD","comments":"","lastddltime":"13/05/2019 11:43:05","createdtime":"12/05/2019 11:02:49"}
,
{"schema":"ADM_SGV_AUD","name":"PREFERENCIA_AUD","comments":"","lastddltime":"13/05/2019 11:42:57","createdtime":"12/05/2019 11:02:50"}
,
{"schema":"ADM_SGV_AUD","name":"PROCESSAMENTO_AUD","comments":"","lastddltime":"13/05/2019 11:43:20","createdtime":"12/05/2019 11:02:47"}
,
{"schema":"ADM_SGV_AUD","name":"PROCESSO_AUD","comments":"","lastddltime":"13/05/2019 11:43:05","createdtime":"12/05/2019 11:02:49"}
,
{"schema":"ADM_SGV_AUD","name":"PROCESSO_CREDENC_FASE_AUD","comments":"","lastddltime":"13/05/2019 11:43:05","createdtime":"12/05/2019 11:02:49"}
,
{"schema":"ADM_SGV_AUD","name":"PROCESSO_CREDENCIAMENTO_AUD","comments":"","lastddltime":"13/05/2019 11:43:05","createdtime":"12/05/2019 11:02:49"}
,
{"schema":"ADM_SGV_AUD","name":"PROCESSO_CREDENC_TERMINAL_AUD","comments":"","lastddltime":"13/05/2019 11:43:15","createdtime":"12/05/2019 11:02:48"}
,
{"schema":"ADM_SGV_AUD","name":"PROCESSO_FASE_AUD","comments":"","lastddltime":"13/05/2019 11:43:06","createdtime":"12/05/2019 11:02:49"}
,
{"schema":"ADM_SGV_AUD","name":"PRODUTO_AUD","comments":"","lastddltime":"13/05/2019 11:43:06","createdtime":"12/05/2019 11:02:49"}
,
{"schema":"ADM_SGV_AUD","name":"PRODUTO_RESTRICAO_AUD","comments":"","lastddltime":"13/05/2019 11:43:29","createdtime":"12/05/2019 11:02:45"}
,
{"schema":"ADM_SGV_AUD","name":"PRODUTO_SEQUENCIA_AUD","comments":"","lastddltime":"13/05/2019 11:43:06","createdtime":"12/05/2019 11:02:49"}
,
{"schema":"ADM_SGV_AUD","name":"PRODUTO_SEQUENCIA_VALOR_AUD","comments":"","lastddltime":"13/05/2019 11:43:25","createdtime":"12/05/2019 11:02:46"}
,
{"schema":"ADM_SGV_AUD","name":"PRODUTO_VENDA_AUD","comments":"","lastddltime":"13/05/2019 11:43:19","createdtime":"12/05/2019 11:02:47"}
,
{"schema":"ADM_SGV_AUD","name":"PROPRIEDADES_PUBLICACAO_AUD","comments":"","lastddltime":"13/05/2019 11:43:26","createdtime":"12/05/2019 11:02:46"}
,
{"schema":"ADM_SGV_AUD","name":"PUBLICACAO_AUD","comments":"","lastddltime":"13/05/2019 11:43:26","createdtime":"12/05/2019 11:02:46"}
,
{"schema":"ADM_SGV_AUD","name":"RAMO_ATIVIDADE_AUD","comments":"","lastddltime":"13/05/2019 11:43:06","createdtime":"12/05/2019 11:02:49"}
,
{"schema":"ADM_SGV_AUD","name":"RECEBIVEL_AUD","comments":"","lastddltime":"13/05/2019 11:43:12","createdtime":"12/05/2019 11:02:48"}
,
{"schema":"ADM_SGV_AUD","name":"RECEPCAO_LOTE_PIN_AUD","comments":"","lastddltime":"13/05/2019 11:43:25","createdtime":"12/05/2019 11:02:46"}
,
{"schema":"ADM_SGV_AUD","name":"RECEPCAO_LOTE_PIN_PROC_AUD","comments":"","lastddltime":"13/05/2019 11:43:25","createdtime":"12/05/2019 11:02:46"}
,
{"schema":"ADM_SGV_AUD","name":"RECEPCAO_RELATORIO_WEB_AUD","comments":"","lastddltime":"13/05/2019 11:43:28","createdtime":"12/05/2019 11:02:46"}
,
{"schema":"ADM_SGV_AUD","name":"REDE_AUD","comments":"","lastddltime":"13/05/2019 11:43:06","createdtime":"12/05/2019 11:02:49"}
,
{"schema":"ADM_SGV_AUD","name":"RELACIONAMENTO_CONSULTOR_AUD","comments":"","lastddltime":"13/05/2019 11:42:59","createdtime":"12/05/2019 11:02:50"}
,
{"schema":"ADM_SGV_AUD","name":"RELATORIO_AUD","comments":"","lastddltime":"13/05/2019 11:43:27","createdtime":"12/05/2019 11:02:46"}
,
{"schema":"ADM_SGV_AUD","name":"RELATORIO_CONF_AUD","comments":"","lastddltime":"13/05/2019 11:43:26","createdtime":"12/05/2019 11:02:46"}
,
{"schema":"ADM_SGV_AUD","name":"RELATORIO_EMAIL_ENVIO_AUD","comments":"","lastddltime":"13/05/2019 11:43:27","createdtime":"12/05/2019 11:02:46"}
,
{"schema":"ADM_SGV_AUD","name":"RELATORIO_GERADO_AUD","comments":"","lastddltime":"13/05/2019 11:43:06","createdtime":"12/05/2019 11:03:12"}
,
{"schema":"ADM_SGV_AUD","name":"RELATORIO_TEMPLATE_AUD","comments":"","lastddltime":"13/05/2019 11:43:23","createdtime":"12/05/2019 11:02:46"}
,
{"schema":"ADM_SGV_AUD","name":"RELATORIO_WEB_AUD","comments":"","lastddltime":"13/05/2019 11:43:27","createdtime":"12/05/2019 11:02:46"}
,
{"schema":"ADM_SGV_AUD","name":"RELEASE_AUD","comments":"","lastddltime":"13/05/2019 11:43:28","createdtime":"12/05/2019 11:02:45"}
,
{"schema":"ADM_SGV_AUD","name":"RELEASE_ITEM_AUD","comments":"","lastddltime":"13/05/2019 11:43:28","createdtime":"12/05/2019 11:02:45"}
,
{"schema":"ADM_SGV_AUD","name":"RESIDUO_AUD","comments":"","lastddltime":"13/05/2019 11:43:12","createdtime":"12/05/2019 11:02:48"}
,
{"schema":"ADM_SGV_AUD","name":"RESTRICAO_VENDA_AUD","comments":"","lastddltime":"13/05/2019 11:43:29","createdtime":"12/05/2019 11:02:45"}
,
{"schema":"ADM_SGV_AUD","name":"ROTEIRO_AUD","comments":"","lastddltime":"13/05/2019 11:43:06","createdtime":"12/05/2019 11:02:49"}
,
{"schema":"ADM_SGV_AUD","name":"SENHA_USUARIO_AUD","comments":"","lastddltime":"13/05/2019 11:42:57","createdtime":"12/05/2019 11:02:50"}
,
{"schema":"ADM_SGV_AUD","name":"SISTEMA_OPERACIONAL_AUD","comments":"","lastddltime":"13/05/2019 11:43:15","createdtime":"12/05/2019 11:02:48"}
,
{"schema":"ADM_SGV_AUD","name":"SOCIO_PROPRIETARIO_AUD","comments":"","lastddltime":"13/05/2019 11:43:07","createdtime":"12/05/2019 11:02:49"}
,
{"schema":"ADM_SGV_AUD","name":"SOLICITACAO_COMPARTILHA_AUD","comments":"","lastddltime":"13/05/2019 11:42:59","createdtime":"12/05/2019 11:02:50"}
,
{"schema":"ADM_SGV_AUD","name":"SOLICITACAO_COMP_ESTOQUE_AUD","comments":"","lastddltime":"13/05/2019 11:42:59","createdtime":"12/05/2019 11:02:50"}
,
{"schema":"ADM_SGV_AUD","name":"SOLICIT_COMP_ESTOQUE_ITEM_AUD","comments":"","lastddltime":"13/05/2019 11:42:59","createdtime":"12/05/2019 11:02:50"}
,
{"schema":"ADM_SGV_AUD","name":"TABELA","comments":"","lastddltime":"12/05/2019 12:54:10","createdtime":"12/05/2019 11:02:45"}
,
{"schema":"ADM_SGV_AUD","name":"TERMINAL_AUD","comments":"","lastddltime":"13/05/2019 11:43:16","createdtime":"12/05/2019 11:02:48"}
,
{"schema":"ADM_SGV_AUD","name":"TERRITORIO_AUD","comments":"","lastddltime":"13/05/2019 11:43:07","createdtime":"12/05/2019 11:02:49"}
,
{"schema":"ADM_SGV_AUD","name":"TIPO_BAIRRO_AUD","comments":"","lastddltime":"13/05/2019 11:43:08","createdtime":"12/05/2019 11:02:49"}
,
{"schema":"ADM_SGV_AUD","name":"TIPO_BLOQUEIO_AUD","comments":"","lastddltime":"13/05/2019 11:43:19","createdtime":"12/05/2019 11:02:47"}
,
{"schema":"ADM_SGV_AUD","name":"TIPO_CONTATO_AUD","comments":"","lastddltime":"13/05/2019 11:43:07","createdtime":"12/05/2019 11:02:49"}
,
{"schema":"ADM_SGV_AUD","name":"TIPO_CORRECAO_AUD","comments":"","lastddltime":"13/05/2019 11:43:12","createdtime":"12/05/2019 11:02:48"}
,
{"schema":"ADM_SGV_AUD","name":"TIPO_DESCONTO_AUD","comments":"","lastddltime":"13/05/2019 11:43:12","createdtime":"12/05/2019 11:02:48"}
,
{"schema":"ADM_SGV_AUD","name":"TIPO_ENDERECO_AUD","comments":"","lastddltime":"13/05/2019 11:43:09","createdtime":"12/05/2019 11:02:49"}
,
{"schema":"ADM_SGV_AUD","name":"TIPO_LOGRADOURO_AUD","comments":"","lastddltime":"13/05/2019 11:43:09","createdtime":"12/05/2019 11:02:49"}
,
{"schema":"ADM_SGV_AUD","name":"TIPO_TRANSACAO_AUD","comments":"","lastddltime":"13/05/2019 11:43:20","createdtime":"12/05/2019 11:02:47"}
,
{"schema":"ADM_SGV_AUD","name":"TRANSACAO_AUD","comments":"","lastddltime":"13/05/2019 11:43:22","createdtime":"12/05/2019 11:02:47"}
,
{"schema":"ADM_SGV_AUD","name":"TRANSFERENCIA_LIMITE_AUD","comments":"","lastddltime":"13/05/2019 11:43:13","createdtime":"12/05/2019 11:02:48"}
,
{"schema":"ADM_SGV_AUD","name":"TRANSFERENCIA_PIN_AUD","comments":"","lastddltime":"13/05/2019 11:43:20","createdtime":"12/05/2019 11:02:47"}
,
{"schema":"ADM_SGV_AUD","name":"USUARIO_AUD","comments":"","lastddltime":"13/05/2019 11:42:57","createdtime":"12/05/2019 11:02:50"}
,
{"schema":"ADM_SGV_AUD","name":"VALOR_FIXO_AUD","comments":"","lastddltime":"13/05/2019 11:43:22","createdtime":"12/05/2019 11:02:47"}
,
{"schema":"ADM_SGV_AUD","name":"VALOR_NEGOCIADO_AUD","comments":"","lastddltime":"13/05/2019 11:43:20","createdtime":"12/05/2019 11:02:47"}
,
{"schema":"ADM_SGV_AUD","name":"VALOR_POSSIVEL_AUD","comments":"","lastddltime":"13/05/2019 11:43:07","createdtime":"12/05/2019 11:02:49"}
,
{"schema":"ADM_SGV_AUD","name":"VERSAO_APLICATIVO_AUD","comments":"","lastddltime":"13/05/2019 11:43:24","createdtime":"12/05/2019 11:02:46"}
,
{"schema":"ADM_SGV_AUD","name":"VIGENCIA_DDD_AUD","comments":"","lastddltime":"13/05/2019 11:43:24","createdtime":"12/05/2019 11:02:46"}
,
{"schema":"ADM_SGV_AUD","name":"VISITA_CONSULTOR_AUD","comments":"","lastddltime":"13/05/2019 11:42:59","createdtime":"12/05/2019 11:02:50"}
,
{"schema":"ADM_SGV_AUX","name":"EMAIL_ENVIO","comments":"","lastddltime":"12/05/2019 10:27:04","createdtime":"12/05/2019 10:26:33"}
,
{"schema":"ADM_SGV_AUX","name":"RELATORIO","comments":"","lastddltime":"12/05/2019 10:27:04","createdtime":"12/05/2019 10:26:33"}
,
{"schema":"ADM_SGV_AUX","name":"RELATORIO_CONF","comments":"","lastddltime":"13/05/2019 11:21:30","createdtime":"12/05/2019 10:26:33"}
,
{"schema":"ADM_SGV_AUX","name":"RELATORIO_EMAIL_ENVIO","comments":"","lastddltime":"13/05/2019 11:21:29","createdtime":"12/05/2019 10:26:33"}
,
{"schema":"ADM_SGV_AUX","name":"RELATORIO_EXECUCAO","comments":"","lastddltime":"13/05/2019 11:21:30","createdtime":"12/05/2019 10:26:33"}
,
{"schema":"ADM_SGV_CAD","name":"ACAO_GERENCIAL","comments":"A��es ligadas ao n�vel de gerenciamento do POS","lastddltime":"13/05/2019 10:55:34","createdtime":"11/05/2019 21:25:39"}
,
{"schema":"ADM_SGV_CAD","name":"ACAO_GERENCIAL_EMPRESA","comments":"Cadastro de a��o ger�ncia dispon�vel para a empresa.","lastddltime":"29/08/2019 21:02:14","createdtime":"11/05/2019 21:25:36"}
,
{"schema":"ADM_SGV_CAD","name":"ACAO_GERENCIAL_MODELO","comments":"Entidade que controla quais a��es gerenciais s�o suportadas por qual modelo de POS.","lastddltime":"13/05/2019 10:55:42","createdtime":"11/05/2019 21:25:36"}
,
{"schema":"ADM_SGV_CAD","name":"ACAO_MASSA","comments":"Cadastro de a��es (atualiza��o, inser��o) em massa","lastddltime":"13/05/2019 10:55:44","createdtime":"11/05/2019 21:25:36"}
,
{"schema":"ADM_SGV_CAD","name":"ACAO_PRODUTO","comments":"Cadastro das a��es especificas para o produto, a��es que apresnetadas no POS","lastddltime":"28/09/2019 17:03:17","createdtime":"11/05/2019 21:25:40"}
,
{"schema":"ADM_SGV_CAD","name":"ACAO_PRODUTO_EMPRESA","comments":"Identifica��o das a��es produtos dispon�veis para a Empresa.","lastddltime":"13/05/2019 10:55:39","createdtime":"11/05/2019 21:25:37"}
,
{"schema":"ADM_SGV_CAD","name":"ACAO_PRODUTO_EMP_VALOR","comments":"Entidade respons�vel por armazenar os registros de A��o Produto a n�vel de valor na Empresa.","lastddltime":"13/05/2019 10:55:43","createdtime":"11/05/2019 21:25:36"}
,
{"schema":"ADM_SGV_CAD","name":"ACAO_PRODUTO_VENDA","comments":"Identifica��o das a��es dispon�veis para o Produto Venda.","lastddltime":"31/05/2019 21:02:47","createdtime":"11/05/2019 21:25:37"}
,
{"schema":"ADM_SGV_CAD","name":"ACAO_TRANSFERENCIA","comments":"Identifica��o das a��es dispon�veis para transfer�ncia de empresa.","lastddltime":"13/05/2019 10:55:47","createdtime":"11/05/2019 21:25:36"}
,
{"schema":"ADM_SGV_CAD","name":"ACAO_TRANSFERENCIA_ESTAB","comments":"Identifica��o das a��es para transfer�ncia de estabelecimentos","lastddltime":"13/05/2019 10:55:48","createdtime":"11/05/2019 21:25:35"}
,
{"schema":"ADM_SGV_CAD","name":"ADQUIRENTE","comments":"Cadastro de adquirente","lastddltime":"13/05/2019 10:56:04","createdtime":"11/05/2019 21:25:33"}
,
{"schema":"ADM_SGV_CAD","name":"AGRUPAMENTO","comments":"Cadastramento de agrupamentos a serem aplicados aos estabelecimentos","lastddltime":"25/09/2019 21:02:33","createdtime":"11/05/2019 21:25:41"}
,
{"schema":"ADM_SGV_CAD","name":"AGRUPAMENTO_REDE_ACESSO","comments":"","lastddltime":"11/05/2019 23:10:21","createdtime":"11/05/2019 21:25:34"}
,
{"schema":"ADM_SGV_CAD","name":"AMBIENTE","comments":"Possibilitar a identifica��o do sistema","lastddltime":"11/05/2019 23:27:21","createdtime":"11/05/2019 21:25:38"}
,
{"schema":"ADM_SGV_CAD","name":"AREA_ATUACAO","comments":"Entidade identifica a �rea de atua��o da Filial.","lastddltime":"02/12/2019 21:03:12","createdtime":"11/05/2019 21:25:35"}
,
{"schema":"ADM_SGV_CAD","name":"AREA_CONSULTOR","comments":"Entidade respons�vel por armazena informa��es da �rea do Consultor","lastddltime":"04/09/2019 10:46:38","createdtime":"11/05/2019 21:25:35"}
,
{"schema":"ADM_SGV_CAD","name":"AREA_CONSULTOR_FERIAS","comments":"","lastddltime":"22/10/2019 14:34:49","createdtime":"01/10/2019 11:17:15"}
,
{"schema":"ADM_SGV_CAD","name":"AREA_CONSULTOR_ITEM","comments":"Entidade respons�vel por armazena informa��es dos Itens da �rea do Consultor","lastddltime":"17/05/2019 21:02:59","createdtime":"11/05/2019 21:25:35"}
,
{"schema":"ADM_SGV_CAD","name":"ATIVIDADE","comments":"Entidade respons�vel por Armazena uma atividade","lastddltime":"11/05/2019 23:27:21","createdtime":"11/05/2019 21:25:38"}
,
{"schema":"ADM_SGV_CAD","name":"ATIVIDADE_FORNECEDOR","comments":"Entidade respons�vel por armazenar a atividade de um fornecedor","lastddltime":"13/05/2019 10:55:37","createdtime":"11/05/2019 21:25:38"}
,
{"schema":"ADM_SGV_CAD","name":"ATIVIDADE_FORNECEDOR_CNAE","comments":"Entidade respons�vel por armazenar informa��es das atividades de um fornecedor","lastddltime":"13/05/2019 10:55:37","createdtime":"11/05/2019 21:25:38"}
,
{"schema":"ADM_SGV_CAD","name":"AUTO_COMPARTILHA","comments":"Entidade respons�vel por armazenar informa��es de configura��o de compartilhamento autom�tico entre empresas.","lastddltime":"13/05/2019 10:56:02","createdtime":"11/05/2019 21:25:34"}
,
{"schema":"ADM_SGV_CAD","name":"AUTO_COMPARTILHA_EMP","comments":"Entidade respons�vel por armazenar a Empresa (Destino) do AUTO_COMPARTILHA.","lastddltime":"13/05/2019 10:56:02","createdtime":"11/05/2019 21:25:34"}
,
{"schema":"ADM_SGV_CAD","name":"AUTO_COMPARTILHA_ITEM","comments":"Entidade respons�vel por relacionar uma informa��o de configura��o de Compartilhamento a um item.","lastddltime":"13/05/2019 10:56:02","createdtime":"11/05/2019 21:25:34"}
,
{"schema":"ADM_SGV_CAD","name":"BAIXA_AUTO_CONF","comments":"Entidade respons�vel por armazenar informa��es para processamento autom�tica da baixa vinda do JDE","lastddltime":"11/05/2019 23:27:10","createdtime":"11/05/2019 21:25:36"}
,
{"schema":"ADM_SGV_CAD","name":"BANCO_ADQUIRENTE","comments":"Relaciona as empresas e bancos ao cadastro de adquirentes","lastddltime":"13/05/2019 10:56:04","createdtime":"11/05/2019 21:25:33"}
,
{"schema":"ADM_SGV_CAD","name":"BANDEIRA","comments":"Armazena as bandeiras para uso das adquirentes","lastddltime":"11/05/2019 23:10:15","createdtime":"11/05/2019 21:25:33"}
,
{"schema":"ADM_SGV_CAD","name":"BANDEIRA_ADQUIRENTE","comments":"Relaciona as adquirentes �s bandeiras de cart�o de cr�dito/d�bito.","lastddltime":"13/05/2019 10:56:05","createdtime":"11/05/2019 21:25:33"}
,
{"schema":"ADM_SGV_CAD","name":"BLACKLIST","comments":"Entidade respons�vel por armazenar informa��es da Black List.","lastddltime":"11/05/2019 23:25:55","createdtime":"11/05/2019 21:25:35"}
,
{"schema":"ADM_SGV_CAD","name":"BONIFICACAO_ACAO_MASSA","comments":"Tabela de bonificacao acao massa.","lastddltime":"13/05/2019 10:56:11","createdtime":"11/05/2019 21:25:20"}
,
{"schema":"ADM_SGV_CAD","name":"BONUS","comments":"Informa��es sobre b�nus dos produtos","lastddltime":"11/05/2019 23:28:40","createdtime":"11/05/2019 21:25:41"}
,
{"schema":"ADM_SGV_CAD","name":"CANAL_VENDA","comments":"Entidade respons�vel por armazenar informa��es do cadastro de canal de venda","lastddltime":"11/05/2019 23:27:09","createdtime":"11/05/2019 21:25:36"}
,
{"schema":"ADM_SGV_CAD","name":"CANAL_VENDA_ESTAB","comments":"Entidade que armazena o relacionamento do CANAL_VENDA com ESTABELECIMENTO_EMPRESA","lastddltime":"20/09/2019 21:02:26","createdtime":"11/05/2019 21:25:38"}
,
{"schema":"ADM_SGV_CAD","name":"CANAL_VENDA_FORN_NEG","comments":"Entidade respons�vel pelo relacionamento do CANAL_VENDA com FORNECEDOR_NEGOCIADO","lastddltime":"13/05/2019 10:55:37","createdtime":"11/05/2019 21:25:38"}
,
{"schema":"ADM_SGV_CAD","name":"CARTELA","comments":"","lastddltime":"13/05/2019 10:56:17","createdtime":"11/05/2019 21:25:30"}
,
{"schema":"ADM_SGV_CAD","name":"CARTELA_NUMERO","comments":"","lastddltime":"13/05/2019 10:56:17","createdtime":"11/05/2019 21:25:30"}
,
{"schema":"ADM_SGV_CAD","name":"CLASSIFICACAO_PRODUTO","comments":"Entidade respons�vel por armazenar informa��es da classifica��o do produto","lastddltime":"15/05/2019 10:59:33","createdtime":"11/05/2019 21:25:35"}
,
{"schema":"ADM_SGV_CAD","name":"CNAE","comments":"Respons�vel por armazenar os CNAE (Classifica��o Nacional de Atividades Econ�micas)","lastddltime":"13/05/2019 10:56:05","createdtime":"11/05/2019 21:25:33"}
,
{"schema":"ADM_SGV_CAD","name":"CNAE_EMPRESA_ADQUIRENTE","comments":"Armazena os percentuais passados ao parceiro conforme a EMPRESA_ADQUIRENTE e CNAE_ID.","lastddltime":"13/05/2019 10:56:05","createdtime":"11/05/2019 21:25:33"}
,
{"schema":"ADM_SGV_CAD","name":"CODIGO_PROCESSAMENTO","comments":"","lastddltime":"11/05/2019 22:56:54","createdtime":"11/05/2019 21:25:31"}
,
{"schema":"ADM_SGV_CAD","name":"COMUNICADOR_SISTEMA","comments":"Entidade para configura��o do comunicador.","lastddltime":"22/05/2019 08:46:19","createdtime":"17/05/2019 15:55:56"}
,
{"schema":"ADM_SGV_CAD","name":"CONCILIACAO_CONF_FTP","comments":"Armazena as informa��es de concilia��o para configura��o de FTP","lastddltime":"11/05/2019 23:26:52","createdtime":"11/05/2019 21:25:36"}
,
{"schema":"ADM_SGV_CAD","name":"CONCILIACAO_CONF_REDETREL","comments":"","lastddltime":"10/07/2019 15:46:04","createdtime":"10/07/2019 10:27:28"}
,
{"schema":"ADM_SGV_CAD","name":"CONCILIACAO_MOVIMENTO","comments":"Armazena a concilia��o da movimenta��o","lastddltime":"13/05/2019 10:55:49","createdtime":"11/05/2019 21:25:35"}
,
{"schema":"ADM_SGV_CAD","name":"CONCILIACAO_SEQUENCIA","comments":"Armazena a concilia��o seq�encial","lastddltime":"13/05/2019 10:55:49","createdtime":"11/05/2019 21:25:35"}
,
{"schema":"ADM_SGV_CAD","name":"CONFIGURACAO_ACAO_MASSA","comments":"Entidade respons�vel por armazenar informa��es de configura��o da a��o massa","lastddltime":"13/05/2019 10:55:49","createdtime":"11/05/2019 21:25:35"}
,
{"schema":"ADM_SGV_CAD","name":"CONFIGURACAO_CONCILIACAO_GERAL","comments":"","lastddltime":"08/11/2019 10:29:52","createdtime":"04/11/2019 13:04:14"}
,
{"schema":"ADM_SGV_CAD","name":"CONFIGURACAO_EMPRESA","comments":"Entidade respons�vel por armazenar informa��es de configura��o da Empresa","lastddltime":"28/11/2019 14:00:15","createdtime":"11/05/2019 21:25:57"}
,
{"schema":"ADM_SGV_CAD","name":"CONFIGURACAO_EMPRESA_BKP","comments":"","lastddltime":"11/05/2019 21:25:57","createdtime":"11/05/2019 21:25:57"}
,
{"schema":"ADM_SGV_CAD","name":"CONFIGURACAO_EMPRESA_BKP0209","comments":"","lastddltime":"11/05/2019 21:25:36","createdtime":"11/05/2019 21:25:36"}
,
{"schema":"ADM_SGV_CAD","name":"CONFIGURACAO_EMPRESA_BKP2207","comments":"","lastddltime":"11/05/2019 21:25:36","createdtime":"11/05/2019 21:25:36"}
,
{"schema":"ADM_SGV_CAD","name":"CONFIGURACAO_ESTAB_ACAO_MASSA","comments":"Entidade respons�vel por armazenar as configura��es do estabelecimento_empresa para o par�metro a��o massa","lastddltime":"13/05/2019 10:55:56","createdtime":"11/05/2019 21:25:35"}
,
{"schema":"ADM_SGV_CAD","name":"CONSIGNACAO_ACAO_MASSA","comments":"Tabela para acao massa de consignacao","lastddltime":"13/05/2019 10:56:10","createdtime":"11/05/2019 21:25:19"}
,
{"schema":"ADM_SGV_CAD","name":"CONSUMO_BOBINA","comments":"","lastddltime":"13/05/2019 10:56:07","createdtime":"11/05/2019 21:25:32"}
,
{"schema":"ADM_SGV_CAD","name":"CONSUMO_BOBINA_DIARIO","comments":"Entidade respons�vel por armazenar informa��es de consumo di�rio de bobinas","lastddltime":"13/05/2019 10:55:59","createdtime":"11/05/2019 21:25:34"}
,
{"schema":"ADM_SGV_CAD","name":"CONTATO","comments":"Entidade respons�vel por armazenar informa��o do contato","lastddltime":"25/11/2019 21:03:09","createdtime":"11/05/2019 21:25:39"}
,
{"schema":"ADM_SGV_CAD","name":"CONTROLE_OPERACAO_REPASSE","comments":"Entidade respons�vel por armazenar o controle de opera��es repasse","lastddltime":"13/05/2019 10:55:58","createdtime":"11/05/2019 21:25:34"}
,
{"schema":"ADM_SGV_CAD","name":"CORRECAO_ESTAB_SUMARIZACAO","comments":"Entidade respons�vel por armazenar informa��es de corre��o de sumariza��o do estabelecimento_empresa","lastddltime":"27/11/2019 05:00:02","createdtime":"11/05/2019 21:25:34"}
,
{"schema":"ADM_SGV_CAD","name":"CORRESPONDENTE_BANCARIO","comments":"Armazena informa��es do cadastro de correspondente banc�rio","lastddltime":"13/05/2019 10:56:00","createdtime":"11/05/2019 21:25:34"}
,
{"schema":"ADM_SGV_CAD","name":"CORRESPONDENTE_BANC_ESTAB_TERM","comments":"Entidade respons�vel por armazenar informa��es do relacionamento entre as tabelas correspondente_bancario, estabelecimento_empresa e terminal","lastddltime":"13/05/2019 10:56:01","createdtime":"11/05/2019 21:25:34"}
,
{"schema":"ADM_SGV_CAD","name":"CREDENCIAL_EMPRESA_ADQUIRENTE","comments":"Descri��o da tabela.","lastddltime":"13/05/2019 10:56:11","createdtime":"11/05/2019 21:25:32"}
,
{"schema":"ADM_SGV_CAD","name":"CREDENCIAL_PARCEIRO","comments":"Tabela para credenciamento de parceiros","lastddltime":"13/05/2019 10:55:55","createdtime":"11/05/2019 21:25:35"}
,
{"schema":"ADM_SGV_CAD","name":"DE_PARA_N2_RV_CODIGO_ORIGEM","comments":"","lastddltime":"11/05/2019 21:38:28","createdtime":"11/05/2019 21:25:30"}
,
{"schema":"ADM_SGV_CAD","name":"DE_PARA_N2_RV_PRODUTOS","comments":"","lastddltime":"11/05/2019 21:38:28","createdtime":"11/05/2019 21:25:30"}
,
{"schema":"ADM_SGV_CAD","name":"DEPARA_TRANS","comments":"","lastddltime":"13/05/2019 10:56:03","createdtime":"11/05/2019 21:25:33"}
,
{"schema":"ADM_SGV_CAD","name":"DEPLOY_BANCO","comments":"Tabela para controle de vers�o de banco de dados","lastddltime":"11/05/2019 23:26:19","createdtime":"11/05/2019 21:25:35"}
,
{"schema":"ADM_SGV_CAD","name":"DEPLOY_BANCO_DET","comments":"","lastddltime":"13/05/2019 10:56:03","createdtime":"11/05/2019 21:25:34"}
,
{"schema":"ADM_SGV_CAD","name":"DEVOLUCAO_CREDITO_ACAO_MASSA","comments":"Tabela para acao massa de consignacao","lastddltime":"13/05/2019 10:56:14","createdtime":"11/05/2019 21:25:25"}
,
{"schema":"ADM_SGV_CAD","name":"EMPRESA","comments":"Entidade respons�vel por armazenar informa��es da empresa","lastddltime":"02/12/2019 10:30:32","createdtime":"11/05/2019 21:25:58"}
,
{"schema":"ADM_SGV_CAD","name":"EMPRESA_ADQUIRENTE","comments":"Armazena o vinculo da empresa com a adquirencia","lastddltime":"04/07/2019 21:04:24","createdtime":"11/05/2019 21:25:33"}
,
{"schema":"ADM_SGV_CAD","name":"EMPRESA_ERP_LOG","comments":"","lastddltime":"18/07/2019 11:22:05","createdtime":"18/07/2019 11:22:05"}
,
{"schema":"ADM_SGV_CAD","name":"EMPRESA_ERP_N2","comments":"","lastddltime":"13/05/2019 10:56:16","createdtime":"11/05/2019 21:25:30"}
,
{"schema":"ADM_SGV_CAD","name":"EMPRESA_OPERACAO","comments":"Entidade respons�vel por armazenar informa��es de opera��es de cada empresa","lastddltime":"13/05/2019 10:55:39","createdtime":"11/05/2019 21:25:37"}
,
{"schema":"ADM_SGV_CAD","name":"EPS","comments":"Entidade respons�vel por armazenar informa��es da empresa prestadora de servi�o","lastddltime":"13/05/2019 10:55:50","createdtime":"11/05/2019 21:25:35"}
,
{"schema":"ADM_SGV_CAD","name":"EPS_EMPRESA","comments":"Armazena a Empresa Prestadora de Servi�os","lastddltime":"13/05/2019 10:55:51","createdtime":"11/05/2019 21:25:35"}
,
{"schema":"ADM_SGV_CAD","name":"EQUIPAMENTO_PARAMETRO","comments":"Armazena os valores m�nimos e sugeridos para cobran�a de taxa extra conforme o tipo de tecnologia do terminal. ","lastddltime":"13/05/2019 10:56:06","createdtime":"11/05/2019 21:25:33"}
,
{"schema":"ADM_SGV_CAD","name":"ESTAB_ACAO_MASSA_FORMA_PGTO","comments":"Tabela que armazena os estabelecimentos para acao massa forma de pagamento.","lastddltime":"13/05/2019 10:56:08","createdtime":"11/05/2019 21:25:16"}
,
{"schema":"ADM_SGV_CAD","name":"ESTABELECIMENTO","comments":"Entidade respons�vel por armazenar informa��es dos estabelecimentos","lastddltime":"08/10/2019 21:03:48","createdtime":"11/05/2019 21:25:41"}
,
{"schema":"ADM_SGV_CAD","name":"ESTABELECIMENTO_ADQUIRENTE","comments":"","lastddltime":"21/11/2019 21:03:07","createdtime":"11/05/2019 21:25:33"}
,
{"schema":"ADM_SGV_CAD","name":"ESTABELECIMENTO_AGRUPADO","comments":"Entidade respons�vel por armazenar o agrupamento dos estabelecimentos","lastddltime":"08/07/2019 21:03:26","createdtime":"11/05/2019 21:25:40"}
,
{"schema":"ADM_SGV_CAD","name":"ESTABELECIMENTO_EMPRESA","comments":"Entidade respons�vel pelo relacionamento dos Estabelecimentos com as empresas","lastddltime":"02/12/2019 10:30:32","createdtime":"11/05/2019 21:25:40"}
,
{"schema":"ADM_SGV_CAD","name":"ESTABELECIMENTO_EMPRESA_BKP","comments":"","lastddltime":"11/05/2019 21:25:36","createdtime":"11/05/2019 21:25:36"}
,
{"schema":"ADM_SGV_CAD","name":"ESTABELECIMENTO_EMPRESA_PARAM","comments":"Parametros do estabelecimento empresa. sequence: ADM_SGV.ESTAB_EMPRESA_PARAM_SEQ_ID","lastddltime":"13/05/2019 10:56:13","createdtime":"11/05/2019 21:25:31"}
,
{"schema":"ADM_SGV_CAD","name":"ESTABELECIMENTO_EMPRESA_1507","comments":"","lastddltime":"11/05/2019 21:25:36","createdtime":"11/05/2019 21:25:36"}
,
{"schema":"ADM_SGV_CAD","name":"ESTABELECIMENTO_PARCEIRO","comments":"","lastddltime":"17/07/2019 12:05:19","createdtime":"11/05/2019 21:25:31"}
,
{"schema":"ADM_SGV_CAD","name":"ESTABELECIMENTO_SIG_CLARO","comments":"","lastddltime":"15/05/2019 10:59:33","createdtime":"11/05/2019 21:25:31"}
,
{"schema":"ADM_SGV_CAD","name":"ESTABELECIMENTO_SIG_OI","comments":"","lastddltime":"15/05/2019 10:59:33","createdtime":"11/05/2019 21:25:31"}
,
{"schema":"ADM_SGV_CAD","name":"ESTABELECIMENTO_SIG_TIM","comments":"","lastddltime":"15/05/2019 10:59:33","createdtime":"11/05/2019 21:25:31"}
,
{"schema":"ADM_SGV_CAD","name":"ESTABELECIMENTO_SIG_VIVO","comments":"","lastddltime":"03/07/2019 08:58:57","createdtime":"11/05/2019 21:25:31"}
,
{"schema":"ADM_SGV_CAD","name":"ESTAB_EMP_HISTORICO_SITUACAO","comments":"Armazena o hist�rico da situa��o do estabelecimento empresa","lastddltime":"15/05/2019 10:59:33","createdtime":"11/05/2019 21:25:35"}
,
{"schema":"ADM_SGV_CAD","name":"ESTAB_HISTORICO_RECADASTRA","comments":"Armazena hist�rico de estabelecimentos recadastrados","lastddltime":"13/05/2019 10:55:48","createdtime":"11/05/2019 21:25:35"}
,
{"schema":"ADM_SGV_CAD","name":"ESTADO_ATUACAO","comments":"Estados em que a Empresa possui atua��o","lastddltime":"13/05/2019 10:55:26","createdtime":"11/05/2019 21:25:42"}
,
{"schema":"ADM_SGV_CAD","name":"ETAPA","comments":"","lastddltime":"12/08/2019 21:02:01","createdtime":"11/05/2019 21:25:57"}
,
{"schema":"ADM_SGV_CAD","name":"ETAPA_PREMIO","comments":"","lastddltime":"13/05/2019 10:56:16","createdtime":"11/05/2019 21:25:30"}
,
{"schema":"ADM_SGV_CAD","name":"EXEC_ACAO_MASSA_ID_IN","comments":"Estrutura para armazenar a escolha de filtros do usu�rio nas a��es em massa.","lastddltime":"13/05/2019 10:56:06","createdtime":"11/05/2019 21:25:32"}
,
{"schema":"ADM_SGV_CAD","name":"EXECUCAO_ACAO_MASSA","comments":"Entidade respons�vel por armazenar informa��es da execu��o a��o massa","lastddltime":"20/05/2019 21:01:51","createdtime":"11/05/2019 21:25:36"}
,
{"schema":"ADM_SGV_CAD","name":"EXPEDIENTE_ACAO_MASSA","comments":"Tabela que armazena expediente da a��o em massa.","lastddltime":"13/05/2019 10:56:09","createdtime":"11/05/2019 21:25:18"}
,
{"schema":"ADM_SGV_CAD","name":"FILIAL_EMPRESA","comments":"Entidade que vai identificar as filiais de neg�cio agrupadas atrav�s da empresa.","lastddltime":"28/11/2019 17:07:29","createdtime":"11/05/2019 21:25:35"}
,
{"schema":"ADM_SGV_CAD","name":"FORNECEDOR","comments":"Cadastro de Fornecedor dos produtos comercializados pela Integradora","lastddltime":"15/05/2019 10:59:33","createdtime":"11/05/2019 21:25:42"}
,
{"schema":"ADM_SGV_CAD","name":"FORNECEDOR_NEGOCIO","comments":"Rela��o entre fornecedor e neg�cio","lastddltime":"15/05/2019 10:59:33","createdtime":"11/05/2019 21:25:41"}
,
{"schema":"ADM_SGV_CAD","name":"GRUPO_ECONOMICO","comments":"Entidade respons�vel por armazenar os cadastros de grupos econ�micos e facilitar o faturamento agrupado.","lastddltime":"15/05/2019 10:59:34","createdtime":"11/05/2019 21:25:34"}
,
{"schema":"ADM_SGV_CAD","name":"GRUPO_RAMO_ATIVIDADE","comments":"armazena o relacionamento do grupo ao ramo de atividade","lastddltime":"13/05/2019 10:55:27","createdtime":"11/05/2019 21:25:42"}
,
{"schema":"ADM_SGV_CAD","name":"GRUPO_VENDA_AUTORIZADO_FORNEC","comments":"Entidade respons�vel por armazenar informa��es dos Grupo venda autorizada dos Fornecedores","lastddltime":"13/05/2019 10:55:44","createdtime":"11/05/2019 21:25:36"}
,
{"schema":"ADM_SGV_CAD","name":"GRUPO_VENDA_BALANCEAMENTO","comments":"","lastddltime":"23/10/2019 16:03:54","createdtime":"21/10/2019 14:45:43"}
,
{"schema":"ADM_SGV_CAD","name":"GRUPO_VENDA_CRIPTOGRAFIA","comments":"","lastddltime":"05/09/2019 16:34:51","createdtime":"04/09/2019 08:32:45"}
,
{"schema":"ADM_SGV_CAD","name":"GRUPO_VENDA_FORNECEDOR","comments":"Entidade respons�vel pelo relacionamento de um Grupo venda com o Fornecedor","lastddltime":"13/05/2019 10:55:38","createdtime":"11/05/2019 21:25:38"}
,
{"schema":"ADM_SGV_CAD","name":"HABILITA_UPSELL","comments":"Tabela de parametriza��o da funcionalidade Upsell.","lastddltime":"13/05/2019 10:56:06","createdtime":"11/05/2019 21:25:32"}
,
{"schema":"ADM_SGV_CAD","name":"HIST_ERRO_GENERICO","comments":"Armazena o hist�rico de erros: auditoria, devolu��es, etc. ","lastddltime":"11/05/2019 22:56:57","createdtime":"11/05/2019 21:25:32"}
,
{"schema":"ADM_SGV_CAD","name":"HIST_IMPRESSAO_TERMO_ADESAO","comments":"Armazena o hist�rico de impress�o de termo de ades�o","lastddltime":"13/05/2019 10:55:52","createdtime":"11/05/2019 21:25:35"}
,
{"schema":"ADM_SGV_CAD","name":"HISTORICO_ATENDIMENTO","comments":"Armazena o Hist�rico de atendimento","lastddltime":"13/05/2019 10:55:35","createdtime":"11/05/2019 21:25:39"}
,
{"schema":"ADM_SGV_CAD","name":"HISTORICO_CONTRATO","comments":"Entidade respons�vel por armazenar a situa��o do Contrato do estabelecimento.","lastddltime":"13/05/2019 10:56:03","createdtime":"11/05/2019 21:25:34"}
,
{"schema":"ADM_SGV_CAD","name":"HISTORICO_ERRO_ACAO_MASSA","comments":"Entidade respons�vel por armazenar informa��es de erro da acao em massa","lastddltime":"13/05/2019 10:55:45","createdtime":"11/05/2019 21:25:36"}
,
{"schema":"ADM_SGV_CAD","name":"HISTORICO_ERRO_ROTA","comments":"Entidade respons�vel por armazenar informa��es de erro na rota","lastddltime":"11/05/2019 21:38:28","createdtime":"11/05/2019 21:25:34"}
,
{"schema":"ADM_SGV_CAD","name":"ICCID_ACAO_MASSA","comments":"Tabela que armazena iccid para acao em massa.","lastddltime":"13/05/2019 10:56:09","createdtime":"11/05/2019 21:25:17"}
,
{"schema":"ADM_SGV_CAD","name":"INJETOR_CONFIGURACAO","comments":"Entidade respons�vel por armazenar informa��o de inje��o de configura��o","lastddltime":"13/05/2019 10:55:55","createdtime":"11/05/2019 21:25:35"}
,
{"schema":"ADM_SGV_CAD","name":"INJETOR_MOVIMENTO","comments":"Entidade respons�vel por armazenar informa��o de inje��o de movimenta��o","lastddltime":"13/05/2019 10:55:52","createdtime":"11/05/2019 21:25:35"}
,
{"schema":"ADM_SGV_CAD","name":"INJETOR_MOVIMENTO_ERRO","comments":"Entidade respons�vel por armazenar informa��o de inje��o de movimenta��o com erro","lastddltime":"13/05/2019 10:55:52","createdtime":"11/05/2019 21:25:35"}
,
{"schema":"ADM_SGV_CAD","name":"INSTITUICAO","comments":"","lastddltime":"11/05/2019 22:56:54","createdtime":"11/05/2019 21:25:57"}
,
{"schema":"ADM_SGV_CAD","name":"INSTRUCAO_ATIVACAO","comments":"Instru��es de ativa��o e informa��es do produto","lastddltime":"11/05/2019 23:28:40","createdtime":"11/05/2019 21:25:41"}
,
{"schema":"ADM_SGV_CAD","name":"INTEGRADOR","comments":"Cadastro do Integrado ao qual as empresas estar�o vinculadas","lastddltime":"11/05/2019 23:28:42","createdtime":"11/05/2019 21:25:59"}
,
{"schema":"ADM_SGV_CAD","name":"INTEGRADOR_PARCEIRO","comments":"Entidade para cadasto do integrador da venda repasse do SFA.","lastddltime":"11/05/2019 22:56:55","createdtime":"11/05/2019 21:25:32"}
,
{"schema":"ADM_SGV_CAD","name":"INTEGRADOR_PARCEIRO_EMPRESA","comments":"Entidade para fazer a liga��o entre o Inegrador Parceiro e Empresa.","lastddltime":"13/05/2019 10:56:12","createdtime":"11/05/2019 21:25:32"}
,
{"schema":"ADM_SGV_CAD","name":"ISENCAO_TAXA","comments":"Armazena a isen��o configuraca para EMPRESA_ADQUIRENTE","lastddltime":"13/05/2019 10:56:08","createdtime":"11/05/2019 21:25:32"}
,
{"schema":"ADM_SGV_CAD","name":"ITEM_CONTATO","comments":"Entidade respons�vel por armazenar informa��es do item contato","lastddltime":"15/05/2019 10:59:34","createdtime":"11/05/2019 21:25:38"}
,
{"schema":"ADM_SGV_CAD","name":"ITEM_ROTEIRO","comments":"Entidade respons�vel por armazenar Itens do roteiro","lastddltime":"31/05/2019 21:01:35","createdtime":"11/05/2019 21:25:39"}
,
{"schema":"ADM_SGV_CAD","name":"LAYOUT_COMPROVANTE","comments":"Entidade respons�vel por armazenar informa��es de layout do comprovante","lastddltime":"04/06/2019 21:01:35","createdtime":"11/05/2019 21:25:34"}
,
{"schema":"ADM_SGV_CAD","name":"LIMITE_ACAO_MASSA","comments":"","lastddltime":"13/05/2019 10:56:12","createdtime":"11/05/2019 21:25:22"}
,
{"schema":"ADM_SGV_CAD","name":"LOG_ECHO_PARCEIROS","comments":"Armazenar os logs das mensagens de echo dos parceiros","lastddltime":"11/05/2019 23:10:16","createdtime":"11/05/2019 21:25:33"}
,
{"schema":"ADM_SGV_CAD","name":"LOG_ERRO_TAXA_EXTRA","comments":"","lastddltime":"11/05/2019 22:13:49","createdtime":"11/05/2019 21:25:32"}
,
{"schema":"ADM_SGV_CAD","name":"LOG_HISTORICO_SMS","comments":"Tabela que armazena log historico sms.","lastddltime":"13/05/2019 10:56:07","createdtime":"11/05/2019 21:25:32"}
,
{"schema":"ADM_SGV_CAD","name":"LOG_INATIVACAO","comments":"Entidade respons�vel por armazenar o log de processo de inativa��o","lastddltime":"13/05/2019 10:55:48","createdtime":"11/05/2019 21:25:35"}
,
{"schema":"ADM_SGV_CAD","name":"MCC","comments":"Tabela de MCC","lastddltime":"11/05/2019 22:56:56","createdtime":"11/05/2019 21:25:32"}
,
{"schema":"ADM_SGV_CAD","name":"MCC_EXCEL","comments":"","lastddltime":"11/05/2019 21:38:28","createdtime":"11/05/2019 21:25:32"}
,
{"schema":"ADM_SGV_CAD","name":"MC_CONTATO","comments":"Entidade respons�vel por complementar o cadastro do contato","lastddltime":"11/05/2019 23:26:23","createdtime":"11/05/2019 21:25:35"}
,
{"schema":"ADM_SGV_CAD","name":"MENSAGEM_FORNECEDOR","comments":"Entidade para fazer o controle das mensagens da operadora.","lastddltime":"13/05/2019 10:56:08","createdtime":"11/05/2019 21:25:32"}
,
{"schema":"ADM_SGV_CAD","name":"MENSAGEM_FORNECEDOR_LIDA","comments":"Entidade para fazer o controle das mensagens da operadora.","lastddltime":"13/05/2019 10:56:08","createdtime":"11/05/2019 21:25:32"}
,
{"schema":"ADM_SGV_CAD","name":"MLOG$_ITEM_CONTATO","comments":"snapshot log for master table ADM_SGV_CAD.ITEM_CONTATO","lastddltime":"11/05/2019 21:38:29","createdtime":"11/05/2019 21:25:36"}
,
{"schema":"ADM_SGV_CAD","name":"MLOG$_RAMO_ATIVIDADE","comments":"snapshot log for master table ADM_SGV_CAD.RAMO_ATIVIDADE","lastddltime":"11/05/2019 21:38:28","createdtime":"11/05/2019 21:25:35"}
,
{"schema":"ADM_SGV_CAD","name":"NEGOCIO","comments":"Cadastro de Neg�cio. Sendo que: Neg�cio � a entidade externa que popula o arquivo de transa��es (movimento) no sistema. Possui entidades e processos pr�prios. Compartilha as informa��es gerais da organiza��o e utiliza os mesmos conceitos das informa��es compartilhadas. Busca no sistema as informa��es em rela��o ao seu comportamento na venda","lastddltime":"11/05/2019 23:28:40","createdtime":"11/05/2019 21:25:41"}
,
{"schema":"ADM_SGV_CAD","name":"NFE_NEGOCIO","comments":"Nfe por negocio","lastddltime":"13/05/2019 10:56:07","createdtime":"11/05/2019 21:25:32"}
,
{"schema":"ADM_SGV_CAD","name":"NOTICIA","comments":"Cadastro de Noticias","lastddltime":"11/05/2019 23:26:19","createdtime":"11/05/2019 21:25:57"}
,
{"schema":"ADM_SGV_CAD","name":"NOTICIA_ANEXOS","comments":"Entidade respons�vel por armazenar os anexos das noticias","lastddltime":"13/05/2019 10:55:55","createdtime":"11/05/2019 21:25:35"}
,
{"schema":"ADM_SGV_CAD","name":"NOTICIA_DESTINOS","comments":"Destinos das Noticias","lastddltime":"13/05/2019 10:55:55","createdtime":"11/05/2019 21:25:35"}
,
{"schema":"ADM_SGV_CAD","name":"NOTICIA_REGRAS","comments":"Regras para visualiza��o da Not�cia","lastddltime":"13/05/2019 10:55:55","createdtime":"11/05/2019 21:25:35"}
,
{"schema":"ADM_SGV_CAD","name":"OCUPACAO","comments":"Entidade de cadastro de ocupa��o. Tabela que ser� importada do Minsit�rio do Trabalho","lastddltime":"11/05/2019 23:27:40","createdtime":"11/05/2019 21:25:39"}
,
{"schema":"ADM_SGV_CAD","name":"OFERTA_FORNECEDOR","comments":"Entidade para fazer o controle de Upstream.","lastddltime":"13/05/2019 10:56:09","createdtime":"11/05/2019 21:25:32"}
,
{"schema":"ADM_SGV_CAD","name":"OFERTA_FORNECEDOR_DDD","comments":"Tabela de parametrizacao do empresa x DDD para envio UPSTREM.","lastddltime":"13/05/2019 10:56:14","createdtime":"11/05/2019 21:25:31"}
,
{"schema":"ADM_SGV_CAD","name":"OFERTA_FORNECEDOR_FORNECEDOR","comments":"Tabela de parametrizacao do empresa x fornecedor para envio UPSTREM.","lastddltime":"13/05/2019 10:56:13","createdtime":"11/05/2019 21:25:31"}
,
{"schema":"ADM_SGV_CAD","name":"OPERACAO","comments":"Entidade respons�vel por armazenar informa��es das opera��es","lastddltime":"11/05/2019 23:27:18","createdtime":"11/05/2019 21:25:37"}
,
{"schema":"ADM_SGV_CAD","name":"OPERACAO_ITEM","comments":"Entidade respons�vel por armazenar os itens da opera��o","lastddltime":"13/05/2019 10:55:53","createdtime":"11/05/2019 21:25:35"}
,
{"schema":"ADM_SGV_CAD","name":"OPERACAO_ITEM_EMPRESA","comments":"Entidade respons�vel pelo relacionamento dos itens da opera��o com a empresa","lastddltime":"13/05/2019 10:55:53","createdtime":"11/05/2019 21:25:35"}
,
{"schema":"ADM_SGV_CAD","name":"OPERACAO_ITEM_EMPRESA_NIVEL","comments":"Entidade respons�vel por armazenar as informa��es das opera��es item empresa por n�vel","lastddltime":"13/05/2019 10:55:54","createdtime":"11/05/2019 21:25:35"}
,
{"schema":"ADM_SGV_CAD","name":"OPERACAO_ITEM_EMPRESA_PRODUTO","comments":"Tem como objetivo armazenar os produtos que foram vinculados a opera��o repasse no portf�lio da empresa possui vinculo com a tabela operacao_item_empresa.","lastddltime":"13/05/2019 10:55:54","createdtime":"11/05/2019 21:25:35"}
,
{"schema":"ADM_SGV_CAD","name":"OPERACAO_ITEM_USUARIO","comments":"Entidade respons�vel por armazenar informa��es das opera��es itens empresa por usu�rio","lastddltime":"13/05/2019 10:55:56","createdtime":"11/05/2019 21:25:35"}
,
{"schema":"ADM_SGV_CAD","name":"OPERACAO_REPASSE_ACAO_MASSA","comments":"Entidade respons�vel por armazenar informa��es de execu��o a��o massa de opera��o repasse ","lastddltime":"13/05/2019 10:55:59","createdtime":"11/05/2019 21:25:34"}
,
{"schema":"ADM_SGV_CAD","name":"OPERADORA","comments":"Operadora que fornece as documenta��es","lastddltime":"11/05/2019 23:28:23","createdtime":"11/05/2019 21:25:40"}
,
{"schema":"ADM_SGV_CAD","name":"OPERADORA_NOME","comments":"","lastddltime":"25/09/2019 08:30:51","createdtime":"24/09/2019 09:09:59"}
,
{"schema":"ADM_SGV_CAD","name":"PARAMETRO_ACAO_ESTAB_EMPRESA","comments":"Entidade respons�vel por armazenar os par�metros da execu��o a��o massa por estabelecimento_empresa","lastddltime":"13/05/2019 10:55:46","createdtime":"11/05/2019 21:25:36"}
,
{"schema":"ADM_SGV_CAD","name":"PARAMETRO_ACAO_MASSA","comments":"Entidade respons�vel por armazenar informa��es a serem utilizados na sele��o/altera��o em massa de dados que sofreram altera��es em massa","lastddltime":"13/05/2019 10:55:49","createdtime":"11/05/2019 21:25:35"}
,
{"schema":"ADM_SGV_CAD","name":"PARAMETRO_ACAO_PRODUTO_VENDA","comments":"Entidade respons�vel por armazenar informa��es a serem utilizados na sele��o/altera��o em massa de dados que sofreram altera��es em massa","lastddltime":"13/05/2019 10:55:46","createdtime":"11/05/2019 21:25:36"}
,
{"schema":"ADM_SGV_CAD","name":"PARAMETRO_BATCH","comments":"DESCONTINUADA","lastddltime":"11/05/2019 23:27:10","createdtime":"11/05/2019 21:25:36"}
,
{"schema":"ADM_SGV_CAD","name":"PARAMETRO_DESTINACAO_VALOR","comments":"Entidade respons�vel por armazenar par�metro para altera��o em massa","lastddltime":"13/05/2019 10:55:46","createdtime":"11/05/2019 21:25:36"}
,
{"schema":"ADM_SGV_CAD","name":"PARAMETRO_GERAL","comments":"Entidade respons�vel por armazenar um par�metro geral","lastddltime":"11/05/2019 23:27:35","createdtime":"11/05/2019 21:25:38"}
,
{"schema":"ADM_SGV_CAD","name":"PARAMETRO_MASSA_DEST_VAL","comments":"Armazenar os par�metros de destina��o valor que ser�o aplicados na atualiza��o em massa","lastddltime":"13/05/2019 10:56:01","createdtime":"11/05/2019 21:25:34"}
,
{"schema":"ADM_SGV_CAD","name":"PARAMETRO_MENSAGEM","comments":"Entidade respons�vel por armazenar as informa��es da mensagem para execu��o a��o massa","lastddltime":"13/05/2019 10:55:47","createdtime":"11/05/2019 21:25:36"}
,
{"schema":"ADM_SGV_CAD","name":"PARAMETRO_PARAMETRO_COBRANCA","comments":"Entidade respons�vel por armazenar o par�metro de cobran�a da execu��o a��o massa","lastddltime":"13/05/2019 10:55:47","createdtime":"11/05/2019 21:25:36"}
,
{"schema":"ADM_SGV_CAD","name":"PARAMETRO_PRODUTO_VENDA","comments":"Entidade respons�vel por armazenar informa��es de par�metros para execu��o a��o massa","lastddltime":"13/05/2019 10:55:45","createdtime":"11/05/2019 21:25:36"}
,
{"schema":"ADM_SGV_CAD","name":"PARAMETRO_SERVIDOR_EMAIL","comments":"","lastddltime":"15/05/2019 09:43:37","createdtime":"15/05/2019 09:43:37"}
,
{"schema":"ADM_SGV_CAD","name":"PARAMETRO_TIPO_CORRECAO","comments":"Armazena informa��es de par�metro do tipo_corre��o para a��o em massa","lastddltime":"13/05/2019 10:55:47","createdtime":"11/05/2019 21:25:36"}
,
{"schema":"ADM_SGV_CAD","name":"PARAMETRO_TRANSFERENCIA_ESTAB","comments":"Armazena informa��es do par�metro transfer�ncia estabelecimento para execu��o a��o em massa","lastddltime":"13/05/2019 10:55:46","createdtime":"11/05/2019 21:25:36"}
,
{"schema":"ADM_SGV_CAD","name":"PARAMETRO_VALOR_NEGOCIADO","comments":"Entidade respons�vel pelo par�metro de valor negociado","lastddltime":"13/05/2019 10:55:46","createdtime":"11/05/2019 21:25:36"}
,
{"schema":"ADM_SGV_CAD","name":"PARCEIRO","comments":"","lastddltime":"11/05/2019 22:56:54","createdtime":"11/05/2019 21:25:31"}
,
{"schema":"ADM_SGV_CAD","name":"PERIODICIDADE_ACAO_MASSA","comments":"","lastddltime":"27/06/2019 21:00:58","createdtime":"11/05/2019 21:25:26"}
,
{"schema":"ADM_SGV_CAD","name":"POLITICA_COMERCIAL","comments":"Armazena informa��es da politica_comercial por produto","lastddltime":"13/05/2019 10:55:59","createdtime":"11/05/2019 21:25:34"}
,
{"schema":"ADM_SGV_CAD","name":"PORTFOLIO","comments":"Rela��o entre fornecedor/empresa/neg�cio","lastddltime":"11/06/2019 21:03:01","createdtime":"11/05/2019 21:25:41"}
,
{"schema":"ADM_SGV_CAD","name":"PREMIO","comments":"","lastddltime":"03/06/2019 15:39:26","createdtime":"11/05/2019 21:25:30"}
,
{"schema":"ADM_SGV_CAD","name":"PROCESSAMENTO_BATCH","comments":"Entidade respons�vel pela troca de consultor (DESCONTINUADA)","lastddltime":"13/05/2019 10:55:41","createdtime":"11/05/2019 21:25:36"}
,
{"schema":"ADM_SGV_CAD","name":"PROCESSAMENTO_CURVA","comments":"","lastddltime":"11/05/2019 23:10:15","createdtime":"11/05/2019 21:25:33"}
,
{"schema":"ADM_SGV_CAD","name":"PROCESSO","comments":"Armazena informa��es do processo","lastddltime":"13/05/2019 10:55:36","createdtime":"11/05/2019 21:25:38"}
,
{"schema":"ADM_SGV_CAD","name":"PROCESSO_CREDENC_FASE","comments":"Armazena informa��es do processo de credenciamento fase","lastddltime":"13/05/2019 10:55:36","createdtime":"11/05/2019 21:25:38"}
,
{"schema":"ADM_SGV_CAD","name":"PROCESSO_CREDENCIAMENTO","comments":"Armazena informa��es do processo de credenciamento","lastddltime":"13/05/2019 10:55:36","createdtime":"11/05/2019 21:25:38"}
,
{"schema":"ADM_SGV_CAD","name":"PROCESSO_FASE","comments":"Armazena informa��es do PROCESSO_FASE","lastddltime":"13/05/2019 10:55:36","createdtime":"11/05/2019 21:25:38"}
,
{"schema":"ADM_SGV_CAD","name":"PRODUTO","comments":"Cadastro de Produtos. Sendo que: Produto trata-se da menor unidade diferenci�vel de um item de venda. Um item de venda, deve ser caracterizado ou n�o de acordo com a necessidade de diferencia��o nos diversos n�veis do neg�cio","lastddltime":"12/08/2019 16:32:55","createdtime":"11/05/2019 21:25:41"}
,
{"schema":"ADM_SGV_CAD","name":"PRODUTO_CORRESP_BANCARIO","comments":"Entidade respons�vel por armazenar informa��es de produto do correspondente banc�rio","lastddltime":"13/05/2019 10:56:01","createdtime":"11/05/2019 21:25:34"}
,
{"schema":"ADM_SGV_CAD","name":"PRODUTO_INSUMO","comments":"Entidade armazena informa��o da taxa de convenio, quer ser� usado na Entidade AGRUPAMENTO para identificar qual a taxa que ser� cobrado para aquele grupo de empresas","lastddltime":"13/05/2019 10:56:01","createdtime":"11/05/2019 21:25:34"}
,
{"schema":"ADM_SGV_CAD","name":"PRODUTO_PARAMETRO","comments":"","lastddltime":"24/07/2019 15:14:03","createdtime":"24/07/2019 15:14:02"}
,
{"schema":"ADM_SGV_CAD","name":"PRODUTO_PARCEIRO","comments":"","lastddltime":"04/10/2019 13:24:40","createdtime":"21/08/2019 16:35:08"}
,
{"schema":"ADM_SGV_CAD","name":"PRODUTO_RESTRICAO","comments":"Armazena informa��es de restri��o de produtos","lastddltime":"13/05/2019 10:55:42","createdtime":"11/05/2019 21:25:36"}
,
{"schema":"ADM_SGV_CAD","name":"PRODUTO_SEQUENCIA","comments":"Entidade que controla a ordena��o do menu","lastddltime":"04/09/2019 14:33:09","createdtime":"11/05/2019 21:25:39"}
,
{"schema":"ADM_SGV_CAD","name":"PRODUTO_SEQUENCIA_VALOR","comments":"Armazena o sequencia_valor do produto","lastddltime":"13/05/2019 10:55:40","createdtime":"11/05/2019 21:25:37"}
,
{"schema":"ADM_SGV_CAD","name":"PRODUTO_TRANS","comments":"","lastddltime":"13/05/2019 10:56:03","createdtime":"11/05/2019 21:25:34"}
,
{"schema":"ADM_SGV_CAD","name":"PROSPECCAO_ADQ","comments":"","lastddltime":"11/05/2019 23:10:20","createdtime":"11/05/2019 21:25:34"}
,
{"schema":"ADM_SGV_CAD","name":"RAMO_ATIVIDADE","comments":"Cadastro de Ramos de Atividades. Tabela importada - CNAE 2.0, do site da Receita Federal, onde haver� uniformidade de nome de atividades","lastddltime":"15/05/2019 10:59:34","createdtime":"11/05/2019 21:25:42"}
,
{"schema":"ADM_SGV_CAD","name":"RECEPCAO_RELATORIO_WEB","comments":"Armazena informa��es do relat�rios web","lastddltime":"13/05/2019 10:55:40","createdtime":"11/05/2019 21:25:36"}
,
{"schema":"ADM_SGV_CAD","name":"RECEPCAO_RELATORIO_WEB_BKP0209","comments":"","lastddltime":"11/05/2019 21:25:36","createdtime":"11/05/2019 21:25:36"}
,
{"schema":"ADM_SGV_CAD","name":"RECEPCAO_RELATORIO_WEB_PARAM","comments":"","lastddltime":"11/05/2019 21:56:47","createdtime":"11/05/2019 21:25:57"}
,
{"schema":"ADM_SGV_CAD","name":"REDE","comments":"Entidade que representa a rede ao qual o estabelecimento faz parte","lastddltime":"24/07/2019 08:34:58","createdtime":"11/05/2019 21:25:42"}
,
{"schema":"ADM_SGV_CAD","name":"REDE_ASSOCIATIVA","comments":"","lastddltime":"15/05/2019 10:59:34","createdtime":"11/05/2019 21:25:30"}
,
{"schema":"ADM_SGV_CAD","name":"REDE_AUX_ACAO_MASSA","comments":"Tabela auxiliar para ajudar em a��o massa com rede.","lastddltime":"13/05/2019 10:56:14","createdtime":"11/05/2019 21:25:24"}
,
{"schema":"ADM_SGV_CAD","name":"REGRA_FRAUDE_LIMITE","comments":"Armazena as regras para FRAUDE_LIMITE","lastddltime":"11/05/2019 23:26:21","createdtime":"11/05/2019 21:25:35"}
,
{"schema":"ADM_SGV_CAD","name":"RELATORIO_EMPRESA_DW","comments":"Entidade respons�vel por armazenar informa��es do cadastro de vinculo entre o relatorio web e a empresa para permitir gerar utilizando a base do DW.","lastddltime":"11/05/2019 21:57:21","createdtime":"11/05/2019 21:25:32"}
,
{"schema":"ADM_SGV_CAD","name":"RELATORIO_GERADO","comments":"Entidade respons�vel por armazenar informa��es dos relat�rios gerados.","lastddltime":"11/05/2019 23:27:21","createdtime":"11/05/2019 21:25:58"}
,
{"schema":"ADM_SGV_CAD","name":"RELATORIO_TEMPLATE","comments":"Entidade respons�vel por armazenar o template dos relat�rios.","lastddltime":"13/05/2019 10:55:38","createdtime":"11/05/2019 21:25:58"}
,
{"schema":"ADM_SGV_CAD","name":"RELATORIO_WEB","comments":"Entidade respons�vel por armazenar informa��es do relatorio web.","lastddltime":"11/05/2019 23:27:18","createdtime":"11/05/2019 21:25:36"}
,
{"schema":"ADM_SGV_CAD","name":"RELEASE","comments":"Armazena cadastro de release","lastddltime":"11/05/2019 23:27:10","createdtime":"11/05/2019 21:25:36"}
,
{"schema":"ADM_SGV_CAD","name":"RELEASE_ITEM","comments":"Entidade respons�vel por relacionar um release a um item","lastddltime":"13/05/2019 10:55:41","createdtime":"11/05/2019 21:25:36"}
,
{"schema":"ADM_SGV_CAD","name":"RESTRICAO_INATIVACAO_ESTAB","comments":"Tabela respons�vel por registrar a �ltima restri��o encontrada para a inativa��o do ESTABELECIMENTO_EMPRESA.","lastddltime":"11/05/2019 23:10:15","createdtime":"11/05/2019 21:25:33"}
,
{"schema":"ADM_SGV_CAD","name":"RESTRICAO_VENDA","comments":"Armazena informa��es de cadastro de restri��o de venda","lastddltime":"13/05/2019 10:55:43","createdtime":"11/05/2019 21:25:36"}
,
{"schema":"ADM_SGV_CAD","name":"RESTRICAO_VENDA_ITEM","comments":"Entidade respons�vel por relacionar uma restri��o de venda a um item.","lastddltime":"11/05/2019 23:27:09","createdtime":"11/05/2019 21:25:36"}
,
{"schema":"ADM_SGV_CAD","name":"RESTRICAO_VENDA_PRODUTO","comments":"Entidade respons�vel por relacionar ma restri��o de venda a um produto","lastddltime":"13/05/2019 10:55:48","createdtime":"11/05/2019 21:25:35"}
,
{"schema":"ADM_SGV_CAD","name":"ROTA","comments":"Respons�vel por armazenar informa��es das Rotas","lastddltime":"01/10/2019 11:31:11","createdtime":"11/05/2019 21:25:35"}
,
{"schema":"ADM_SGV_CAD","name":"ROTA_ITEM","comments":"Tabela respons�vel pelo cadastro das rotas di�rias do consultor","lastddltime":"13/05/2019 10:55:58","createdtime":"11/05/2019 21:25:34"}
,
{"schema":"ADM_SGV_CAD","name":"ROTEIRO","comments":"Roteiro de seq��ncia utilizada para apresenta��o de a��es no menu da POS","lastddltime":"26/07/2019 08:47:05","createdtime":"11/05/2019 21:25:40"}
,
{"schema":"ADM_SGV_CAD","name":"ROTEIRO_VIEW_LAYOUT","comments":"Descri��o da tabela.","lastddltime":"01/07/2019 21:01:58","createdtime":"11/05/2019 21:25:32"}
,
{"schema":"ADM_SGV_CAD","name":"RUPD$_ITEM_CONTATO","comments":"temporary updatable snapshot log","lastddltime":"11/05/2019 21:25:57","createdtime":"11/05/2019 21:25:57"}
,
{"schema":"ADM_SGV_CAD","name":"RUPD$_RAMO_ATIVIDADE","comments":"temporary updatable snapshot log","lastddltime":"11/05/2019 21:25:57","createdtime":"11/05/2019 21:25:57"}
,
{"schema":"ADM_SGV_CAD","name":"SERVICOS_OS","comments":"Servi�os executados no estabelecimento via O.S. ","lastddltime":"13/05/2019 10:55:51","createdtime":"11/05/2019 21:25:35"}
,
{"schema":"ADM_SGV_CAD","name":"SOCIO_PROPRIETARIO","comments":"Cadastro dos S�cios propriet�rios: Empresa Estabelecimento","lastddltime":"11/05/2019 23:28:38","createdtime":"11/05/2019 21:25:41"}
,
{"schema":"ADM_SGV_CAD","name":"SOLICITACAO_PRODUTO","comments":"Entidade respons�vel por armazenar solicita��es de produtos","lastddltime":"13/05/2019 10:55:59","createdtime":"11/05/2019 21:25:34"}
,
{"schema":"ADM_SGV_CAD","name":"SOLICITACAO_PRODUTO_CONFIG","comments":"Armazenar as configura��es da funcionalidade de solicita��o de produtos","lastddltime":"13/05/2019 10:56:01","createdtime":"11/05/2019 21:25:34"}
,
{"schema":"ADM_SGV_CAD","name":"SUSPEITA_FRAUDE","comments":"Tabela que armazena estabelecimentos que ca�ram na regra de antifraude","lastddltime":"18/06/2019 15:11:24","createdtime":"11/05/2019 21:25:35"}
,
{"schema":"ADM_SGV_CAD","name":"TABELA_PARAMETRO","comments":"Tabela respons�vel por armazenar os par�metros das estabelecimentos","lastddltime":"13/05/2019 10:55:40","createdtime":"11/05/2019 21:25:57"}
,
{"schema":"ADM_SGV_CAD","name":"TAXA","comments":"Tabela de rela��o de taxas bandeira e cnae","lastddltime":"13/05/2019 10:56:11","createdtime":"11/05/2019 21:25:32"}
,
{"schema":"ADM_SGV_CAD","name":"TAXA_FAIXA","comments":"Tabela de Informa��es de Taxa","lastddltime":"13/05/2019 10:56:11","createdtime":"11/05/2019 21:25:32"}
,
{"schema":"ADM_SGV_CAD","name":"TERMO_ADESAO","comments":"Entidade respons�vel por armazenar informa��es do cadastro de Termo de Ades�o da empresa.","lastddltime":"13/05/2019 10:56:03","createdtime":"11/05/2019 21:25:34"}
,
{"schema":"ADM_SGV_CAD","name":"TERMO_ADESAO_ESTAB_EMPRESA","comments":"Entidade respons�vel por armazenar informa��es do termo de ades�o por estabelecimento_empresa","lastddltime":"28/06/2019 21:03:19","createdtime":"11/05/2019 21:25:35"}
,
{"schema":"ADM_SGV_CAD","name":"TERRITORIO","comments":"Territ�rio o qual a empresa faz parte.","lastddltime":"13/05/2019 10:55:26","createdtime":"11/05/2019 21:25:42"}
,
{"schema":"ADM_SGV_CAD","name":"TIPO_ASSUNTO_MENSAGEM","comments":"Tipo de Assunto da Mensagem","lastddltime":"11/05/2019 23:26:21","createdtime":"11/05/2019 21:25:35"}
,
{"schema":"ADM_SGV_CAD","name":"TIPO_CONTATO","comments":"Registro dos tipos de contatos os quais o contato possui","lastddltime":"29/07/2019 16:29:07","createdtime":"11/05/2019 21:25:39"}
,
{"schema":"ADM_SGV_CAD","name":"TIPO_LOCAL_PDV","comments":"","lastddltime":"11/05/2019 22:56:55","createdtime":"11/05/2019 21:25:32"}
,
{"schema":"ADM_SGV_CAD","name":"TIPO_PDV","comments":"","lastddltime":"11/05/2019 22:56:55","createdtime":"11/05/2019 21:25:31"}
,
{"schema":"ADM_SGV_CAD","name":"TIPO_POSITIVACAO","comments":"","lastddltime":"11/05/2019 22:56:55","createdtime":"11/05/2019 21:25:32"}
,
{"schema":"ADM_SGV_CAD","name":"TMP_CNAE_MCC","comments":"","lastddltime":"11/05/2019 22:18:26","createdtime":"11/05/2019 21:25:32"}
,
{"schema":"ADM_SGV_CAD","name":"TMP_MCC_TAXA","comments":"","lastddltime":"11/05/2019 21:38:28","createdtime":"11/05/2019 21:25:32"}
,
{"schema":"ADM_SGV_CAD","name":"TROCA_AREA_ACAO_MASSA","comments":"","lastddltime":"03/06/2019 15:39:21","createdtime":"29/05/2019 09:52:32"}
,
{"schema":"ADM_SGV_CAD","name":"TROCA_AREA_ACAO_MASSA_EXEC","comments":"","lastddltime":"03/06/2019 15:39:23","createdtime":"29/05/2019 09:52:33"}
,
{"schema":"ADM_SGV_CAD","name":"TROCA_AREA_ARQUIVO","comments":"","lastddltime":"03/06/2019 15:39:19","createdtime":"29/05/2019 09:52:31"}
,
{"schema":"ADM_SGV_CAD","name":"VALOR_POSSIVEL","comments":"Valores de produtos lan�ados para produtos que s�o multi valorados","lastddltime":"13/05/2019 10:55:34","createdtime":"11/05/2019 21:25:39"}
,
{"schema":"ADM_SGV_CAD","name":"VENCIMENTO_TAXA","comments":"Armazena a(s) possivel(is) data(s) de vencimento(s) das taxas para um adquirente.","lastddltime":"13/05/2019 10:56:06","createdtime":"11/05/2019 21:25:32"}
,
{"schema":"ADM_SGV_CAD","name":"WHITELIST_ANTIFRAUDE","comments":"Armazena os poss�veis PDVs que ser�o ignorados no processo de bloqueio antifraude","lastddltime":"13/05/2019 10:56:11","createdtime":"11/05/2019 21:25:32"}
,
{"schema":"ADM_SGV_CAD","name":"WHITE_LIST_RV_N2","comments":"","lastddltime":"11/05/2019 21:38:28","createdtime":"11/05/2019 21:25:30"}
,
{"schema":"ADM_SGV_CEP","name":"BAIRRO","comments":"Entidade respons�vel por armazenar informa��es do Bairro","lastddltime":"15/05/2019 10:59:34","createdtime":"11/05/2019 21:25:29"}
,
{"schema":"ADM_SGV_CEP","name":"CEP","comments":"Entidade respons�vel por armazenar informa��es do CEP","lastddltime":"13/05/2019 10:55:22","createdtime":"11/05/2019 21:25:29"}
,
{"schema":"ADM_SGV_CEP","name":"CEP_ALTERADO","comments":"","lastddltime":"13/05/2019 10:55:26","createdtime":"11/05/2019 21:25:28"}
,
{"schema":"ADM_SGV_CEP","name":"CIDADE","comments":"Entidade respons�vel pelo Cadastro geral de cidades","lastddltime":"15/05/2019 10:59:34","createdtime":"11/05/2019 21:25:29"}
,
{"schema":"ADM_SGV_CEP","name":"ENDERECO","comments":"Entidade respons�vel pelo Cadastro geral de endere�os","lastddltime":"28/06/2019 21:03:28","createdtime":"11/05/2019 21:25:29"}
,
{"schema":"ADM_SGV_CEP","name":"ESTADO","comments":"Entidade respons�vel pelo Cadastro geral de estado","lastddltime":"15/05/2019 10:59:34","createdtime":"11/05/2019 21:25:30"}
,
{"schema":"ADM_SGV_CEP","name":"FERIADO","comments":"Entidade respons�vel por manter os feriados no sistema, estes servir�o de base para a gera��o de cobran�as","lastddltime":"13/05/2019 10:55:23","createdtime":"11/05/2019 21:25:29"}
,
{"schema":"ADM_SGV_CEP","name":"FERIADO_BKP","comments":"","lastddltime":"11/05/2019 21:25:28","createdtime":"11/05/2019 21:25:28"}
,
{"schema":"ADM_SGV_CEP","name":"FILA_ATUALIZA_MC_ENDERECO","comments":"","lastddltime":"11/05/2019 22:52:54","createdtime":"11/05/2019 21:25:23"}
,
{"schema":"ADM_SGV_CEP","name":"LOCALIZACAO_ADICIONAL","comments":"","lastddltime":"13/05/2019 10:55:24","createdtime":"11/05/2019 21:25:28"}
,
{"schema":"ADM_SGV_CEP","name":"LOGRADOURO","comments":"Cadastro geral de logradouros","lastddltime":"15/05/2019 10:59:34","createdtime":"11/05/2019 21:25:29"}
,
{"schema":"ADM_SGV_CEP","name":"MATRIZ_DISTANCIA","comments":"","lastddltime":"13/05/2019 10:55:25","createdtime":"11/05/2019 21:25:28"}
,
{"schema":"ADM_SGV_CEP","name":"MC_ENDERECO","comments":"Entidade respons�vel por armazenar as informa��es de endere�o complementar","lastddltime":"18/11/2019 17:53:49","createdtime":"11/05/2019 21:25:29"}
,
{"schema":"ADM_SGV_CEP","name":"PAIS","comments":"Entidade respons�vel por armazenar o cadastro dos pa�ses","lastddltime":"11/05/2019 22:56:50","createdtime":"11/05/2019 21:25:30"}
,
{"schema":"ADM_SGV_CEP","name":"TIPO_BAIRRO","comments":"Entidade respons�vel por armazenar a Identifica��o do tipo de bairro.","lastddltime":"11/05/2019 22:56:50","createdtime":"11/05/2019 21:25:30"}
,
{"schema":"ADM_SGV_CEP","name":"TIPO_ENDERECO","comments":"Entidade respons�vel pela identifica��o do tipos de endere�o","lastddltime":"11/05/2019 22:56:50","createdtime":"11/05/2019 21:25:30"}
,
{"schema":"ADM_SGV_CEP","name":"TIPO_LOGRADOURO","comments":"Entidade respons�vel pelo Cadastro de Tipo de Logradouro","lastddltime":"15/05/2019 10:59:34","createdtime":"11/05/2019 21:25:29"}
,
{"schema":"ADM_SGV_COB","name":"AGENDAMENTO","comments":"Entidade respons�vel por armazenar informa��es do agendamento","lastddltime":"12/05/2019 12:24:44","createdtime":"12/05/2019 11:02:37"}
,
{"schema":"ADM_SGV_COB","name":"BAIXA","comments":"Armazena informa��es de baixa de t�tulos","lastddltime":"18/06/2019 21:00:58","createdtime":"12/05/2019 11:02:39"}
,
{"schema":"ADM_SGV_COB","name":"BANCO","comments":"Banco ao qual a ag�ncia e a conta banc�ria est�o vinculadas","lastddltime":"12/05/2019 12:34:33","createdtime":"12/05/2019 11:02:41"}
,
{"schema":"ADM_SGV_COB","name":"BANCO_DET_COBRANCA","comments":"Detalhes de cobran�a utilizada para gera��o de boletos","lastddltime":"13/05/2019 11:42:44","createdtime":"12/05/2019 11:02:39"}
,
{"schema":"ADM_SGV_COB","name":"BLOQUEIO_COBRANCA","comments":"Armazena informa��es de bloqueio de cobran�as","lastddltime":"13/05/2019 11:42:46","createdtime":"12/05/2019 11:02:38"}
,
{"schema":"ADM_SGV_COB","name":"CICLO_RECEBIMENTO","comments":"Conjunto de Regras de receb�veis, para consolida��o dos receb�veis.","lastddltime":"20/09/2019 21:02:36","createdtime":"12/05/2019 11:02:40"}
,
{"schema":"ADM_SGV_COB","name":"COBRANCA","comments":"Dados de Lan�amento da cobran�a para gera��o de boleto.","lastddltime":"05/12/2019 15:13:24","createdtime":"12/05/2019 11:02:39"}
,
{"schema":"ADM_SGV_COB","name":"COBRANCA_INADIMPLENTE","comments":"Tabela com as cobran�as vencidas e n�o pagas somente, com o intuito de aliviar a quantidade de consultas na tabela COBRAN�A.","lastddltime":"12/05/2019 12:24:44","createdtime":"12/05/2019 11:02:38"}
,
{"schema":"ADM_SGV_COB","name":"COBRANCA_INCONSISTENTE","comments":"Armazena informa��es das cobran�as inconsistentes.","lastddltime":"13/05/2019 11:42:50","createdtime":"12/05/2019 11:02:36"}
,
{"schema":"ADM_SGV_COB","name":"COBRANCA_PRORROGADA","comments":"","lastddltime":"06/12/2019 10:21:33","createdtime":"06/12/2019 10:21:32"}
,
{"schema":"ADM_SGV_COB","name":"CONTROLE_SALDO","comments":"Entidade armazena informa��es de controle de saldo","lastddltime":"13/05/2019 11:42:46","createdtime":"12/05/2019 11:02:38"}
,
{"schema":"ADM_SGV_COB","name":"CONVENIO_BANCARIO","comments":"Entidade para cadastro dos conv�nios das empresas para gera��o de boleto","lastddltime":"30/09/2019 21:03:03","createdtime":"12/05/2019 11:02:41"}
,
{"schema":"ADM_SGV_COB","name":"DEBITO_TX_EXTRA_BONIFICADO","comments":"Descrição da tabela.","lastddltime":"13/05/2019 11:42:52","createdtime":"12/05/2019 11:02:36"}
,
{"schema":"ADM_SGV_COB","name":"DOMICILIO_BANCARIO","comments":"Entidade para cadastro de contas banc�rias. Podendo ser conta da empresa, no caso conta para gera��o de boleto, ou conta de estabelecimento (domic�lio banc�rio).","lastddltime":"13/05/2019 11:42:37","createdtime":"12/05/2019 11:02:40"}
,
{"schema":"ADM_SGV_COB","name":"DOMICILIO_BANCARIO_BANDEIRA","comments":"Relaciona os domic�lios banc�rios �s bandeiras de cart�o de cr�dito/d�bito","lastddltime":"13/05/2019 11:42:52","createdtime":"12/05/2019 11:02:36"}
,
{"schema":"ADM_SGV_COB","name":"ERRO_CONTROLE_SALDO","comments":"Entidade que armazena os erros do controle de saldo ","lastddltime":"12/05/2019 11:16:59","createdtime":"12/05/2019 11:02:38"}
,
{"schema":"ADM_SGV_COB","name":"FILA_BLOQUEIO_DESBLOQUEIO_ADQ","comments":"Tabela que armazena dados para a fila de bloqueio e desbloqueio ADQ.","lastddltime":"13/05/2019 11:42:55","createdtime":"12/05/2019 11:02:25"}
,
{"schema":"ADM_SGV_COB","name":"FILA_PRC_COBRANCA","comments":"Fila para consumo das cobran�as.","lastddltime":"13/05/2019 11:42:53","createdtime":"12/05/2019 11:02:23"}
,
{"schema":"ADM_SGV_COB","name":"GERA_RECEBIVEIS_LOG","comments":"Armazena informa��es de log do GERA_RECEBIVEIS","lastddltime":"13/05/2019 11:42:45","createdtime":"12/05/2019 11:02:39"}
,
{"schema":"ADM_SGV_COB","name":"HIERARQUIA_COBRANCA","comments":"Tabela que armazena a hierarquia da cobran�a.","lastddltime":"11/06/2019 09:18:19","createdtime":"12/05/2019 11:02:36"}
,
{"schema":"ADM_SGV_COB","name":"HIST_ERRO_BOLETO_PARTICIONADO","comments":"Armazena informa��es de hist�rico de erros do boleto particionado","lastddltime":"12/05/2019 11:16:58","createdtime":"12/05/2019 11:02:36"}
,
{"schema":"ADM_SGV_COB","name":"HISTORICO_ERRO_REMESSA","comments":"Armazena informa��es de erro ao gerar lote_arquivo_remessa","lastddltime":"13/05/2019 11:42:51","createdtime":"12/05/2019 11:02:36"}
,
{"schema":"ADM_SGV_COB","name":"IMPRESSAO_COBRANCA","comments":"Tabela utilizada para armazenar o contexto de uma impress�o de boleto pelo POS.","lastddltime":"13/05/2019 11:42:54","createdtime":"12/05/2019 11:02:35"}
,
{"schema":"ADM_SGV_COB","name":"LANCAMENTO_TAXA","comments":"","lastddltime":"13/05/2019 11:42:52","createdtime":"12/05/2019 11:02:36"}
,
{"schema":"ADM_SGV_COB","name":"LANCAMENTO_TRANSACAO","comments":"Armazena informa��es de lan�amento de transa��o","lastddltime":"13/05/2019 11:42:51","createdtime":"12/05/2019 11:02:36"}
,
{"schema":"ADM_SGV_COB","name":"LANCAMENTO_TRANSACAO_COBRANCA","comments":"Armazena informa��es de lan�amento de transa��o das cobran�as","lastddltime":"13/05/2019 11:42:51","createdtime":"12/05/2019 11:02:36"}
,
{"schema":"ADM_SGV_COB","name":"LAYOUT_RET_BANCARIO","comments":"","lastddltime":"12/05/2019 12:24:43","createdtime":"12/05/2019 11:02:36"}
,
{"schema":"ADM_SGV_COB","name":"LAYOUT_RET_BANCARIO_ITEM","comments":"","lastddltime":"12/05/2019 12:24:43","createdtime":"12/05/2019 11:02:36"}
,
{"schema":"ADM_SGV_COB","name":"LIMITE","comments":"Cadastro de Limites. Sendo que: Os limites ser�o: total(todos os neg�cio), ou por neg�cio. Quando o neg�cio n�o tem limite cadastrado, ele ir� compartilhar do limite total","lastddltime":"15/05/2019 10:59:34","createdtime":"12/05/2019 11:02:40"}
,
{"schema":"ADM_SGV_COB","name":"LIMITE_SALDO_BAIXA","comments":"Esta tabela ficar� respons�vel por salvar as altera��es de saldo provindas da tabela BAIXA","lastddltime":"12/05/2019 12:24:43","createdtime":"12/05/2019 11:02:16"}
,
{"schema":"ADM_SGV_COB","name":"LIMITE_SALDO_LIMITE","comments":"Esta tabela ficar� respons�vel por salvar as altera��es de saldo provindas da tabela LIMITE.","lastddltime":"12/05/2019 12:24:43","createdtime":"12/05/2019 11:02:09"}
,
{"schema":"ADM_SGV_COB","name":"LOG_ENVIO_COBRANCA","comments":"Armazena informa��o no log de envio de cobran�as","lastddltime":"13/05/2019 11:42:48","createdtime":"12/05/2019 11:02:38"}
,
{"schema":"ADM_SGV_COB","name":"LOG_GERACAO_COBRANCA","comments":"Armazena informa��es do log da gera��o de cobran�a","lastddltime":"12/05/2019 11:39:09","createdtime":"12/05/2019 11:02:39"}
,
{"schema":"ADM_SGV_COB","name":"LOTE_ARQUIVO_REMESSA","comments":"Armazena informa��es do lote_arquivo_remessa para gera��o de remessa","lastddltime":"24/10/2019 15:33:27","createdtime":"12/05/2019 11:02:37"}
,
{"schema":"ADM_SGV_COB","name":"LOTE_ARQUIVO_REMESSA_CT","comments":"Armazena informa��es de controle do lote_arquivo_remessa","lastddltime":"24/10/2019 15:33:27","createdtime":"12/05/2019 11:02:36"}
,
{"schema":"ADM_SGV_COB","name":"LOTE_ARQUIVO_REMESSA_ITEM","comments":"Armazena informa��es do lote_arquivo_remessa para gera��o de remessa","lastddltime":"24/10/2019 15:33:28","createdtime":"12/05/2019 11:02:37"}
,
{"schema":"ADM_SGV_COB","name":"LOTE_BAIXA","comments":"Entidade que armazena o lote de baixa banc�ria","lastddltime":"12/05/2019 12:34:33","createdtime":"12/05/2019 11:02:41"}
,
{"schema":"ADM_SGV_COB","name":"LOTE_BAIXA_DOC_BAIXADO","comments":"Identifica��o das baixas realizadas aos documentos baixados.","lastddltime":"13/05/2019 11:42:43","createdtime":"12/05/2019 11:02:39"}
,
{"schema":"ADM_SGV_COB","name":"LOTE_BAIXA_DOCUMENTO","comments":"Identifica��o do documento do lote","lastddltime":"04/09/2019 09:41:19","createdtime":"12/05/2019 11:02:41"}
,
{"schema":"ADM_SGV_COB","name":"LOTE_BAIXA_DOCUMENTO_DUP","comments":"","lastddltime":"12/05/2019 11:16:58","createdtime":"12/05/2019 11:02:37"}
,
{"schema":"ADM_SGV_COB","name":"LOTE_COBRANCA","comments":"Armazenar informa��es do lote de cobran�a","lastddltime":"13/05/2019 11:42:46","createdtime":"12/05/2019 11:02:38"}
,
{"schema":"ADM_SGV_COB","name":"LOTE_COBRANCA_ITEM","comments":"Armazenar informa��es dos itens do lote de cobran�a","lastddltime":"13/05/2019 11:42:47","createdtime":"12/05/2019 11:02:38"}
,
{"schema":"ADM_SGV_COB","name":"LOTE_COBRANCA_ITEM_DETALHE","comments":"Armazena informa��es de detalhe dos itens das cobran�as","lastddltime":"13/05/2019 11:42:47","createdtime":"12/05/2019 11:02:38"}
,
{"schema":"ADM_SGV_COB","name":"MENSAGEM","comments":"Mensagem que ser� apresentada no boleto","lastddltime":"13/05/2019 11:42:39","createdtime":"12/05/2019 11:02:40"}
,
{"schema":"ADM_SGV_COB","name":"MENSAGEM_COBRANCA_PRORROGADA","comments":"","lastddltime":"09/12/2019 10:18:05","createdtime":"05/12/2019 15:54:53"}
,
{"schema":"ADM_SGV_COB","name":"NEGOCIACAO","comments":"Entidade respons�vel pela negocia��o que � realizada entre a empresa e o estabelecimento dos t�tulos vencidos e n�o pagos","lastddltime":"02/10/2019 21:02:40","createdtime":"12/05/2019 11:02:40"}
,
{"schema":"ADM_SGV_COB","name":"NEGOCIACAO_RESTABELECE","comments":"Armazena negocia��o para restabelecer valor","lastddltime":"13/05/2019 11:42:46","createdtime":"12/05/2019 11:02:38"}
,
{"schema":"ADM_SGV_COB","name":"NEGOCIACAO_TITULO","comments":"Identifica��o das baixas dos t�tulos negociados.","lastddltime":"13/05/2019 11:42:44","createdtime":"12/05/2019 11:02:39"}
,
{"schema":"ADM_SGV_COB","name":"OCORRENCIA_RETORNO","comments":"Armazena informa��es de retorno das ocorr�ncias","lastddltime":"13/05/2019 11:42:46","createdtime":"12/05/2019 11:02:38"}
,
{"schema":"ADM_SGV_COB","name":"PARAMETRO_COBRANCA","comments":"Conjunto de Regras de cobran�a, para consolida��o dos receb�veis e gera��o dos t�tulos de cobran�a","lastddltime":"19/11/2019 21:03:05","createdtime":"12/05/2019 11:02:40"}
,
{"schema":"ADM_SGV_COB","name":"PLANO_NEGOCIACAO","comments":"Entidade respons�vel pela configura��o do Plano de Negocia��o que ser� seguido pelo Integrado, para executar suas negocia��es com os estabelecimentos dos t�tulos vencidos e n�o pagos","lastddltime":"12/05/2019 12:34:33","createdtime":"12/05/2019 11:02:41"}
,
{"schema":"ADM_SGV_COB","name":"PLANO_NEGOCIACAO_PARCELA","comments":"Identifica��o das poss�veis parcelas de um Plano de Negocia��o","lastddltime":"13/05/2019 11:42:38","createdtime":"12/05/2019 11:02:40"}
,
{"schema":"ADM_SGV_COB","name":"PRIMEIRO_ALUGUEL","comments":"","lastddltime":"13/05/2019 11:42:53","createdtime":"12/05/2019 11:02:35"}
,
{"schema":"ADM_SGV_COB","name":"RECEBIVEL","comments":"Entidade respons�vel pelos lan�amentos de receb�veis relacionados ao estabelecimento. Oriundos de: - transa��es valorizadas; - res�duos; - dep�sitos; - descontos. Que est�o presentes na entidade de Transa��es.","lastddltime":"13/05/2019 11:42:42","createdtime":"12/05/2019 11:02:39"}
,
{"schema":"ADM_SGV_COB","name":"RECEBIVEL_MENSAGEM","comments":"Mensagens geradas pelo processamento dos receb�veis","lastddltime":"13/05/2019 11:42:44","createdtime":"12/05/2019 11:02:39"}
,
{"schema":"ADM_SGV_COB","name":"RECEPCAO_ARQUIVO","comments":"Armazena informa��es da recep��o de arquivos das empresas","lastddltime":"13/05/2019 11:42:45","createdtime":"12/05/2019 11:02:39"}
,
{"schema":"ADM_SGV_COB","name":"RECEPCAO_LOTE_BAIXA","comments":"Armazena informa��es de recep��o de um lote_baixa","lastddltime":"13/05/2019 11:42:45","createdtime":"12/05/2019 11:03:12"}
,
{"schema":"ADM_SGV_COB","name":"RECEPCAO_LOTE_BAIXA_LOG","comments":"","lastddltime":"12/05/2019 12:24:43","createdtime":"12/05/2019 11:02:36"}
,
{"schema":"ADM_SGV_COB","name":"REGISTRO_COBRANCA","comments":"Tabela para armazenar a geanalogia de uma cobran�a","lastddltime":"13/05/2019 11:42:53","createdtime":"12/05/2019 11:02:36"}
,
{"schema":"ADM_SGV_COB","name":"RESIDUO","comments":"Valores a maior ou a menor que foram as diferen�as do valor pago pelo t�tulo.","lastddltime":"19/09/2019 21:03:39","createdtime":"12/05/2019 11:02:39"}
,
{"schema":"ADM_SGV_COB","name":"SALDOS","comments":"Armazena cadastro de saldo","lastddltime":"15/05/2019 10:59:34","createdtime":"12/05/2019 11:02:38"}
,
{"schema":"ADM_SGV_COB","name":"TAXA_EXTRA","comments":"Armazenar informa��es de taxa extra","lastddltime":"15/10/2019 21:01:30","createdtime":"12/05/2019 11:02:37"}
,
{"schema":"ADM_SGV_COB","name":"TIPO_CORRECAO","comments":"Tipo de corre��o (juros, multa, mora, taxa)","lastddltime":"13/05/2019 11:42:40","createdtime":"12/05/2019 11:02:40"}
,
{"schema":"ADM_SGV_COB","name":"TIPO_DESCONTO","comments":"Tipos de descontos quem podem estar vinculados a uma parcela de um plano de parcelamento","lastddltime":"13/05/2019 11:42:37","createdtime":"12/05/2019 11:02:40"}
,
{"schema":"ADM_SGV_COB","name":"TRANSFERENCIA_LIMITE","comments":"Entidade para controle da transfer�ncia de limites (saldo bonificado) do estabelecimento.","lastddltime":"13/05/2019 11:42:44","createdtime":"12/05/2019 11:02:39"}
,
{"schema":"ADM_SGV_DNE","name":"ATUALIZA_LOG_BAIRRO","comments":"","lastddltime":"11/05/2019 21:38:31","createdtime":"11/05/2019 21:25:56"}
,
{"schema":"ADM_SGV_DNE","name":"ATUALIZA_LOG_CPC","comments":"","lastddltime":"11/05/2019 21:38:31","createdtime":"11/05/2019 21:25:56"}
,
{"schema":"ADM_SGV_DNE","name":"ATUALIZA_LOG_FAIXA_BAIRRO","comments":"","lastddltime":"11/05/2019 21:38:31","createdtime":"11/05/2019 21:25:56"}
,
{"schema":"ADM_SGV_DNE","name":"ATUALIZA_LOG_FAIXA_CPC","comments":"","lastddltime":"11/05/2019 21:38:31","createdtime":"11/05/2019 21:25:56"}
,
{"schema":"ADM_SGV_DNE","name":"ATUALIZA_LOG_FAIXA_LOCALIDADE","comments":"","lastddltime":"11/05/2019 21:38:31","createdtime":"11/05/2019 21:25:56"}
,
{"schema":"ADM_SGV_DNE","name":"ATUALIZA_LOG_FAIXA_UOP","comments":"","lastddltime":"11/05/2019 21:38:31","createdtime":"11/05/2019 21:25:56"}
,
{"schema":"ADM_SGV_DNE","name":"ATUALIZA_LOG_GRANDE_USUARIO","comments":"","lastddltime":"11/05/2019 21:38:31","createdtime":"11/05/2019 21:25:56"}
,
{"schema":"ADM_SGV_DNE","name":"ATUALIZA_LOG_LOCALIDADE","comments":"","lastddltime":"11/05/2019 21:38:30","createdtime":"11/05/2019 21:25:56"}
,
{"schema":"ADM_SGV_DNE","name":"ATUALIZA_LOG_LOGRADOURO","comments":"","lastddltime":"11/05/2019 21:38:31","createdtime":"11/05/2019 21:25:56"}
,
{"schema":"ADM_SGV_DNE","name":"ATUALIZA_LOG_NUM_SEC","comments":"","lastddltime":"11/05/2019 21:38:31","createdtime":"11/05/2019 21:25:56"}
,
{"schema":"ADM_SGV_DNE","name":"ATUALIZA_LOG_UNID_OPER","comments":"","lastddltime":"11/05/2019 21:38:31","createdtime":"11/05/2019 21:25:56"}
,
{"schema":"ADM_SGV_DNE","name":"CONTROLE_IMPORTACAO_DNE","comments":"Tabela que armazena o controle da importa��o DNE.","lastddltime":"11/05/2019 23:32:48","createdtime":"11/05/2019 21:25:56"}
,
{"schema":"ADM_SGV_DNE","name":"ENDERECO_INC","comments":"","lastddltime":"13/05/2019 10:54:51","createdtime":"11/05/2019 21:25:56"}
,
{"schema":"ADM_SGV_DNE","name":"ESTABELECIMENTO_CONC_DNE","comments":"","lastddltime":"13/05/2019 10:54:52","createdtime":"11/05/2019 21:25:56"}
,
{"schema":"ADM_SGV_DNE","name":"HISTORICO_ATUALIZACAO_DNE","comments":"Tabela que armazena historico de atualizao do DNE.","lastddltime":"11/05/2019 23:32:48","createdtime":"11/05/2019 21:25:56"}
,
{"schema":"ADM_SGV_DNE","name":"LOG_BAIRRO","comments":"","lastddltime":"13/05/2019 10:54:49","createdtime":"11/05/2019 21:25:57"}
,
{"schema":"ADM_SGV_DNE","name":"LOG_CPC","comments":"","lastddltime":"26/06/2019 08:37:15","createdtime":"11/05/2019 21:25:57"}
,
{"schema":"ADM_SGV_DNE","name":"LOG_FAIXA_BAIRRO","comments":"","lastddltime":"11/05/2019 23:32:51","createdtime":"11/05/2019 21:25:57"}
,
{"schema":"ADM_SGV_DNE","name":"LOG_FAIXA_CPC","comments":"","lastddltime":"11/05/2019 23:32:52","createdtime":"11/05/2019 21:25:57"}
,
{"schema":"ADM_SGV_DNE","name":"LOG_FAIXA_LOCALIDADE","comments":"","lastddltime":"11/05/2019 23:32:52","createdtime":"11/05/2019 21:25:57"}
,
{"schema":"ADM_SGV_DNE","name":"LOG_FAIXA_UF","comments":"","lastddltime":"11/05/2019 23:32:52","createdtime":"11/05/2019 21:25:57"}
,
{"schema":"ADM_SGV_DNE","name":"LOG_FAIXA_UOP","comments":"","lastddltime":"11/05/2019 23:32:49","createdtime":"11/05/2019 21:25:56"}
,
{"schema":"ADM_SGV_DNE","name":"LOG_GRANDE_USUARIO","comments":"","lastddltime":"15/05/2019 10:59:35","createdtime":"11/05/2019 21:25:57"}
,
{"schema":"ADM_SGV_DNE","name":"LOG_LOCALIDADE","comments":"","lastddltime":"15/05/2019 10:59:35","createdtime":"11/05/2019 21:25:57"}
,
{"schema":"ADM_SGV_DNE","name":"LOG_LOGRADOURO","comments":"","lastddltime":"15/05/2019 10:59:35","createdtime":"11/05/2019 21:25:57"}
,
{"schema":"ADM_SGV_DNE","name":"LOG_NUM_SEC","comments":"","lastddltime":"11/05/2019 23:32:49","createdtime":"11/05/2019 21:25:57"}
,
{"schema":"ADM_SGV_DNE","name":"LOG_UNID_OPER","comments":"","lastddltime":"15/05/2019 10:59:35","createdtime":"11/05/2019 21:25:57"}
,
{"schema":"ADM_SGV_DNE","name":"LOG_VAR_BAI","comments":"","lastddltime":"11/05/2019 23:32:52","createdtime":"11/05/2019 21:25:57"}
,
{"schema":"ADM_SGV_DNE","name":"LOG_VAR_LOC","comments":"","lastddltime":"11/05/2019 23:32:52","createdtime":"11/05/2019 21:25:57"}
,
{"schema":"ADM_SGV_DNE","name":"LOG_VAR_LOG","comments":"","lastddltime":"11/05/2019 23:32:49","createdtime":"11/05/2019 21:25:57"}
,
{"schema":"ADM_SGV_EQP","name":"AGENDA_TELECARGA","comments":"Classe que guarda os logs de altera��o no agendamento de atualiza��o de terminal","lastddltime":"13/05/2019 11:42:27","createdtime":"12/05/2019 11:02:55"}
,
{"schema":"ADM_SGV_EQP","name":"APLICATIVO","comments":"Cadastro de Aplicativos para terminal","lastddltime":"22/05/2019 21:02:56","createdtime":"12/05/2019 11:02:56"}
,
{"schema":"ADM_SGV_EQP","name":"APLICATIVO_ATUALIZACAO","comments":"Entidade respons�vel por armazenar o hist�rico de atualiza��o do terminal.","lastddltime":"13/05/2019 11:42:28","createdtime":"12/05/2019 11:02:54"}
,
{"schema":"ADM_SGV_EQP","name":"APLICATIVO_MODELO","comments":"Modelos suportados pelo pacote de telecarga.","lastddltime":"11/07/2019 21:01:22","createdtime":"12/05/2019 11:02:52"}
,
{"schema":"ADM_SGV_EQP","name":"APLICATIVO_SO","comments":"Associa��o entre Aplicativo e Sistema Operacional","lastddltime":"13/05/2019 11:42:23","createdtime":"12/05/2019 11:02:56"}
,
{"schema":"ADM_SGV_EQP","name":"ARRANJO","comments":"Armazenar as configura��es de arranjos de servi�os do sistema de atendimento.","lastddltime":"12/05/2019 12:54:14","createdtime":"12/05/2019 11:02:52"}
,
{"schema":"ADM_SGV_EQP","name":"BINARIO","comments":"Armazena informa��es dos aplicativos bin�rios instalados no POS","lastddltime":"12/05/2019 12:54:16","createdtime":"12/05/2019 11:02:53"}
,
{"schema":"ADM_SGV_EQP","name":"CHIP","comments":"Cadastro de chip","lastddltime":"13/05/2019 11:42:24","createdtime":"12/05/2019 11:02:55"}
,
{"schema":"ADM_SGV_EQP","name":"CHIP_BKP","comments":"","lastddltime":"12/05/2019 11:02:52","createdtime":"12/05/2019 11:02:52"}
,
{"schema":"ADM_SGV_EQP","name":"CONTROLE_TELECARGA","comments":"","lastddltime":"03/12/2019 16:49:17","createdtime":"25/09/2019 11:53:26"}
,
{"schema":"ADM_SGV_EQP","name":"CONTROLE_TELECARGA_LOG","comments":"","lastddltime":"25/09/2019 11:53:42","createdtime":"25/09/2019 11:53:29"}
,
{"schema":"ADM_SGV_EQP","name":"EMPRESA_RESPONSAVEL","comments":"Empresa respons�vel pelo terminal f�sico (manuten��o, apoio), � a 1� empresa � qual o terminal � alocado. Esse cadastro � feito automaticamente  no momento da aloca��o o terminal � empresa. Caso  haja desaloca��o de terminal para essa empresa, dever� ser preenchida a data final da aloca��o, e dever� ser selecionada a pr�xima empresa que ser� respons�vel","lastddltime":"13/05/2019 11:42:27","createdtime":"12/05/2019 11:02:55"}
,
{"schema":"ADM_SGV_EQP","name":"FABRICANTE","comments":"Classe de cadastro b�sico de Fabricantes dos modelos de Terminais","lastddltime":"12/05/2019 12:54:18","createdtime":"12/05/2019 11:02:56"}
,
{"schema":"ADM_SGV_EQP","name":"FILA_MERCHAN","comments":"Fila de processamento para contabiliza��o da quantidade de merchans impressos.","lastddltime":"12/05/2019 11:17:48","createdtime":"12/05/2019 11:02:22"}
,
{"schema":"ADM_SGV_EQP","name":"FILA_TERMINAL_AGENDAMENTO","comments":"Armazena informa��es dos terminais na fila para serem agendados","lastddltime":"12/05/2019 12:54:15","createdtime":"12/05/2019 11:02:52"}
,
{"schema":"ADM_SGV_EQP","name":"HISTORICO_CHIP_TERMINAL","comments":"Entidade que guarda os hist�ricos dos n�meros de s�rie que esse terminal j� possuiu","lastddltime":"22/07/2019 17:56:31","createdtime":"12/05/2019 11:02:55"}
,
{"schema":"ADM_SGV_EQP","name":"HISTORICO_CHIP_TERMINAL_240303","comments":"","lastddltime":"12/05/2019 11:02:52","createdtime":"12/05/2019 11:02:52"}
,
{"schema":"ADM_SGV_EQP","name":"HISTORICO_NUM_SERIE","comments":"Entidade que guarda os hist�ricos dos n�meros de s�rie que esse terminal j� possuiu","lastddltime":"13/05/2019 11:42:27","createdtime":"12/05/2019 11:02:55"}
,
{"schema":"ADM_SGV_EQP","name":"IMAGEM_MERCHAN","comments":"Armazena informa��es das imagens dos merchan","lastddltime":"12/05/2019 12:54:15","createdtime":"12/05/2019 11:03:12"}
,
{"schema":"ADM_SGV_EQP","name":"ITEM_AGENDA_TELECARGA","comments":"Itens de relacionamento em que ser�o adicionados os terminais agendados","lastddltime":"13/05/2019 11:42:27","createdtime":"12/05/2019 11:02:54"}
,
{"schema":"ADM_SGV_EQP","name":"ITEM_APLICATIVO","comments":"Entidade respons�vel por armazenar os itens de aplicativo.","lastddltime":"13/05/2019 11:42:29","createdtime":"12/05/2019 11:02:54"}
,
{"schema":"ADM_SGV_EQP","name":"LOG_ERRO_TERMINAL","comments":"Log de erros do terminal.","lastddltime":"12/05/2019 12:54:14","createdtime":"12/05/2019 11:02:52"}
,
{"schema":"ADM_SGV_EQP","name":"LOGO_EMPRESA_POS","comments":"Entidade respons�vel por armazenar informa��es do cadastro da Logo POS da empresa.","lastddltime":"13/05/2019 11:42:33","createdtime":"12/05/2019 11:03:12"}
,
{"schema":"ADM_SGV_EQP","name":"MC_HIST_CHIP_TERMINAL","comments":"Armazena um modelo complementar da hist�rico chip_terminal","lastddltime":"18/06/2019 10:30:55","createdtime":"12/05/2019 11:02:53"}
,
{"schema":"ADM_SGV_EQP","name":"MC_TERMINAL_CONEXAO","comments":"","lastddltime":"13/05/2019 11:42:34","createdtime":"12/05/2019 11:02:52"}
,
{"schema":"ADM_SGV_EQP","name":"MERCHAN","comments":"Armazena informa��o de propaganda","lastddltime":"29/05/2019 10:05:47","createdtime":"12/05/2019 11:02:53"}
,
{"schema":"ADM_SGV_EQP","name":"MOBILE_PUBLICACAO","comments":"Descri��o da tabela.","lastddltime":"29/05/2019 09:59:41","createdtime":"29/05/2019 09:59:40"}
,
{"schema":"ADM_SGV_EQP","name":"MODELO","comments":"Modelos de POS por fabricante","lastddltime":"13/05/2019 11:42:22","createdtime":"12/05/2019 11:02:56"}
,
{"schema":"ADM_SGV_EQP","name":"MODELO_LOGO_EXTENSAO","comments":"Entidade respons�vel por armazenar informa��es da extens�o da LOGO para aquele modelo do terminal","lastddltime":"12/05/2019 12:54:14","createdtime":"12/05/2019 11:02:52"}
,
{"schema":"ADM_SGV_EQP","name":"MODELO_NEGOCIO","comments":"","lastddltime":"13/05/2019 11:42:34","createdtime":"12/05/2019 11:02:52"}
,
{"schema":"ADM_SGV_EQP","name":"MODELO_SIS_OPERACIONAL","comments":"Entidade que deve manter os sistemas operacionais que o modelo de terminal suporta","lastddltime":"13/05/2019 11:42:23","createdtime":"12/05/2019 11:02:56"}
,
{"schema":"ADM_SGV_EQP","name":"MODELO_TIPO_CONEXAO","comments":"Tipos de conex�o suportados pelo modelo de terminal","lastddltime":"13/05/2019 11:42:24","createdtime":"12/05/2019 11:02:55"}
,
{"schema":"ADM_SGV_EQP","name":"PARAM_CONEXAO_ARRANJO","comments":"Armazenar as configura��es de arranjos de servi�os do sistema de atendimento.","lastddltime":"13/05/2019 11:42:33","createdtime":"12/05/2019 11:02:52"}
,
{"schema":"ADM_SGV_EQP","name":"PARAMETRO_CONEXAO","comments":"Classe respons�vel por guardar dados de conex�o diversas","lastddltime":"10/10/2019 21:02:27","createdtime":"12/05/2019 11:02:55"}
,
{"schema":"ADM_SGV_EQP","name":"PARAMETRO_EMPRESA","comments":"Par�metros cadastrados para a empresa","lastddltime":"22/05/2019 21:02:59","createdtime":"12/05/2019 11:02:55"}
,
{"schema":"ADM_SGV_EQP","name":"PARAMETRO_EMPRESA_TERMINAL","comments":"Par�metros de conex�o da empresa que s�o utilizados pelo terminal","lastddltime":"29/10/2019 21:03:23","createdtime":"12/05/2019 11:02:55"}
,
{"schema":"ADM_SGV_EQP","name":"PARAMETRO_ESTAB_EMPRESA","comments":"Par�metros de configura��o do estabelecimento_empresa","lastddltime":"13/05/2019 11:42:29","createdtime":"12/05/2019 11:02:53"}
,
{"schema":"ADM_SGV_EQP","name":"POSVENDAZERO","comments":"DESCONTINUADA","lastddltime":"12/05/2019 12:54:15","createdtime":"12/05/2019 11:02:52"}
,
{"schema":"ADM_SGV_EQP","name":"PROCESSO_CREDENC_TERMINAL","comments":"Armazena informa��o do processo de credenciamento do terminal","lastddltime":"13/05/2019 11:42:28","createdtime":"12/05/2019 11:02:54"}
,
{"schema":"ADM_SGV_EQP","name":"PROPRIEDADES_PUBLICACAO","comments":"Armazena informa��es da propriedade da publica��o","lastddltime":"30/05/2019 21:01:56","createdtime":"12/05/2019 11:02:53"}
,
{"schema":"ADM_SGV_EQP","name":"PUBLICACAO","comments":"Armazena informa��o de publica��o do merchan","lastddltime":"29/05/2019 10:00:37","createdtime":"12/05/2019 11:02:53"}
,
{"schema":"ADM_SGV_EQP","name":"PUBLICACAO_ARQUIVO","comments":"Armazena informa��o de publica��o do arquivo","lastddltime":"13/05/2019 11:42:32","createdtime":"12/05/2019 11:02:52"}
,
{"schema":"ADM_SGV_EQP","name":"PUBLICACAO_ARQUIVO_ITEM","comments":"Armazena informa��es de publica��o dos itens do arquivo","lastddltime":"13/05/2019 11:42:32","createdtime":"12/05/2019 11:02:52"}
,
{"schema":"ADM_SGV_EQP","name":"PUBLICACAO_DATA_MERCHAN","comments":"Armazena informa��o da data de publica��o do merchan","lastddltime":"13/05/2019 11:42:32","createdtime":"12/05/2019 11:02:52"}
,
{"schema":"ADM_SGV_EQP","name":"PUBLICACAO_TERMINAL","comments":"Controle de Publica��es por Terminal","lastddltime":"31/05/2019 13:46:27","createdtime":"12/05/2019 11:02:52"}
,
{"schema":"ADM_SGV_EQP","name":"RADACCT","comments":"DESCONTINUADA","lastddltime":"13/05/2019 11:42:28","createdtime":"12/05/2019 11:02:54"}
,
{"schema":"ADM_SGV_EQP","name":"RADCHECK","comments":"Entidade para autentica��o no Radius","lastddltime":"12/05/2019 12:54:16","createdtime":"12/05/2019 11:02:54"}
,
{"schema":"ADM_SGV_EQP","name":"RADGROUPCHECK","comments":"DESCONTINUADA","lastddltime":"12/05/2019 12:54:16","createdtime":"12/05/2019 11:02:54"}
,
{"schema":"ADM_SGV_EQP","name":"RADGROUPREPLY","comments":"DESCONTINUADA","lastddltime":"13/05/2019 11:42:28","createdtime":"12/05/2019 11:02:54"}
,
{"schema":"ADM_SGV_EQP","name":"RADPOSTAUTH","comments":"Armazena informa��es de acesso a rede discada","lastddltime":"13/05/2019 11:42:27","createdtime":"12/05/2019 11:02:54"}
,
{"schema":"ADM_SGV_EQP","name":"RADREPLY","comments":"DESCONTINUADA","lastddltime":"13/05/2019 11:42:28","createdtime":"12/05/2019 11:02:54"}
,
{"schema":"ADM_SGV_EQP","name":"REALMGROUP","comments":"DESCONTINUADA","lastddltime":"12/05/2019 12:54:16","createdtime":"12/05/2019 11:02:54"}
,
{"schema":"ADM_SGV_EQP","name":"REALMS","comments":"DESCONTINUADA","lastddltime":"12/05/2019 12:54:16","createdtime":"12/05/2019 11:02:54"}
,
{"schema":"ADM_SGV_EQP","name":"SISTEMA_OPERACIONAL","comments":"Classe que armazena os sistemas operacionais que rodam nos terminais","lastddltime":"12/05/2019 12:54:18","createdtime":"12/05/2019 11:02:56"}
,
{"schema":"ADM_SGV_EQP","name":"TCI_HISTORICO_ERRO","comments":"Tabela para registrar o log de erros do processo TCI (Terminal/Chip Importa��o)","lastddltime":"13/05/2019 11:42:32","createdtime":"12/05/2019 11:02:52"}
,
{"schema":"ADM_SGV_EQP","name":"TCI_RECEPCAO_ARQUIVO","comments":"Armazena informa��o do terminal chip importa��o na recep��o de arquivo","lastddltime":"12/05/2019 12:54:15","createdtime":"12/05/2019 11:03:12"}
,
{"schema":"ADM_SGV_EQP","name":"TCI_REGISTRO_IMPORTACAO","comments":"Armazena informa��o dos registro de importa��o da Terminal Chip Importa��o","lastddltime":"13/05/2019 11:42:31","createdtime":"12/05/2019 11:02:52"}
,
{"schema":"ADM_SGV_EQP","name":"TCI_RESUMO_RECEPCAO","comments":"Armazena informa��es de resumo da recep��o do terminal chip importa��o","lastddltime":"13/05/2019 11:42:31","createdtime":"12/05/2019 11:02:52"}
,
{"schema":"ADM_SGV_EQP","name":"TERMINAL","comments":"Cadastro gen�rico do Terminal","lastddltime":"14/06/2019 21:03:34","createdtime":"12/05/2019 11:02:55"}
,
{"schema":"ADM_SGV_EQP","name":"TERMINAL_ADQUIRENTE","comments":"","lastddltime":"13/05/2019 11:42:33","createdtime":"12/05/2019 11:02:52"}
,
{"schema":"ADM_SGV_EQP","name":"TERMINAL_AGENDAMENTO","comments":"Armazena informa��es de agendamento dos terminais","lastddltime":"26/06/2019 21:01:02","createdtime":"12/05/2019 11:02:52"}
,
{"schema":"ADM_SGV_EQP","name":"TERMINAL_ATUALIZACAO","comments":"Armazena informa��es de atualiza��o do terminal","lastddltime":"30/05/2019 21:01:44","createdtime":"12/05/2019 11:02:53"}
,
{"schema":"ADM_SGV_EQP","name":"TERMINAL_ATUALIZACAO_HIST","comments":"","lastddltime":"12/05/2019 11:02:52","createdtime":"12/05/2019 11:02:52"}
,
{"schema":"ADM_SGV_EQP","name":"TERMINAL_ATUALIZACAO_HISTORICO","comments":"","lastddltime":"12/05/2019 11:02:51","createdtime":"12/05/2019 11:02:51"}
,
{"schema":"ADM_SGV_EQP","name":"TERMINAL_CONEXAO","comments":"Registrar as conex�es do POS","lastddltime":"12/05/2019 12:54:14","createdtime":"12/05/2019 11:02:52"}
,
{"schema":"ADM_SGV_EQP","name":"TERMINAL_NOTIFICACAO","comments":"Entidade respons�vel por armazenar TOKEN Gerado pelo Google para Aplicativo / Smartphone.","lastddltime":"13/05/2019 11:42:35","createdtime":"12/05/2019 11:02:23"}
,
{"schema":"ADM_SGV_EQP","name":"TERMINAL_TAXA_EXTRA","comments":"","lastddltime":"13/05/2019 11:42:34","createdtime":"12/05/2019 11:02:52"}
,
{"schema":"ADM_SGV_EQP","name":"TERMINAL_TOKEN","comments":"Entidade respons�vel por armazenar TOKEN Gerado pelo Google para Aplicativo / Smartphone.","lastddltime":"13/05/2019 11:42:34","createdtime":"12/05/2019 11:02:52"}
,
{"schema":"ADM_SGV_EQP","name":"TROCA_TERMINAL","comments":"Tabela que armazena informa��es da troca de terminal para primeiro aluguel.","lastddltime":"13/05/2019 11:42:36","createdtime":"12/05/2019 11:02:52"}
,
{"schema":"ADM_SGV_EQP","name":"VERSAO_APLICATIVO","comments":"Armazena informa��o da vers�o do aplicativo","lastddltime":"13/05/2019 11:42:29","createdtime":"12/05/2019 11:02:53"}
,
{"schema":"ADM_SGV_EQP","name":"VIGENCIA_DDD","comments":"Determina o inicio da vig�ncia do nono digito","lastddltime":"12/05/2019 12:54:15","createdtime":"12/05/2019 11:02:53"}
,
{"schema":"ADM_SGV_FTR","name":"CONTROLE_KIT_VIRTUAL","comments":"Entidade de controle da fila para montar lote_kit, Lote_kit_item e lote_kit_serial, do kit virtual.","lastddltime":"13/05/2019 11:21:31","createdtime":"12/05/2019 10:25:48"}
,
{"schema":"ADM_SGV_FTR","name":"FILA_PRC_TRANSACAO","comments":"","lastddltime":"18/10/2019 10:07:31","createdtime":"12/05/2019 10:25:36"}
,
{"schema":"ADM_SGV_FTR","name":"FILA_PRC_TRANSACAO_ERR","comments":"","lastddltime":"12/05/2019 10:26:37","createdtime":"12/05/2019 10:26:31"}
,
{"schema":"ADM_SGV_FTR","name":"FILA_PRC_TRANSACAO_LOG","comments":"","lastddltime":"12/05/2019 10:26:37","createdtime":"12/05/2019 10:26:31"}
,
{"schema":"ADM_SGV_FTR","name":"FILA_TRANSACAO","comments":"Entidade respons�vel por armazenar o consumo das transa��es provindas do desacoplamento do sisetma.","lastddltime":"12/05/2019 10:26:48","createdtime":"12/05/2019 10:26:02"}
,
{"schema":"ADM_SGV_FTR","name":"FILA_TRANSACAO_LOG","comments":"Entidade respons�vel por armazenar o log de erros ocorridos no consumo da fila.","lastddltime":"12/05/2019 10:27:01","createdtime":"12/05/2019 10:26:31"}
,
{"schema":"ADM_SGV_INT","name":"ACAO_TRANSFERENCIA_ESTAB_JS","comments":"Estrutura para armazenar a informa��o no formato JSON para envio de integra��es","lastddltime":"15/05/2019 10:59:35","createdtime":"12/05/2019 10:25:50"}
,
{"schema":"ADM_SGV_INT","name":"ACAO_TRANSFERENCIA_JS","comments":"Estrutura para armazenar a informa��o no formato JSON para envio de integra��es","lastddltime":"15/05/2019 10:59:35","createdtime":"12/05/2019 10:25:48"}
,
{"schema":"ADM_SGV_INT","name":"ACESSO_JS","comments":"ESTRUTURA PARA ARMAZENAR A INFORMA��O NO FORMATO JSON PARA ENVIO DE INTEGRA��ES","lastddltime":"15/05/2019 10:59:35","createdtime":"12/05/2019 10:26:05"}
,
{"schema":"ADM_SGV_INT","name":"AIRCLIC_EXEC","comments":"","lastddltime":"12/05/2019 10:27:04","createdtime":"12/05/2019 10:26:35"}
,
{"schema":"ADM_SGV_INT","name":"ALOCACAO_CHIP_EMPRESA_JS","comments":"ESTRUTURA PARA ARMAZENAR A INFORMA��O NO FORMATO JSON PARA ENVIO DE INTEGRA��ES","lastddltime":"15/05/2019 10:59:35","createdtime":"12/05/2019 10:26:04"}
,
{"schema":"ADM_SGV_INT","name":"ALOCACAO_TERMINAL_EMPRESA_JS","comments":"Estrutura para armazenar a informa��o no formato JSON para envio de integra��es","lastddltime":"15/05/2019 10:59:35","createdtime":"12/05/2019 10:25:50"}
,
{"schema":"ADM_SGV_INT","name":"ALOCACAO_TERMINAL_ESTAB_JS","comments":"Estrutura para armazenar a informa��o no formato JSON para envio de integra��es","lastddltime":"15/05/2019 10:59:36","createdtime":"12/05/2019 10:25:51"}
,
{"schema":"ADM_SGV_INT","name":"APLICATIVO_JS","comments":"Estrutura para armazenar a informa��o no formato JSON para envio de integra��es","lastddltime":"15/05/2019 10:59:36","createdtime":"12/05/2019 10:26:00"}
,
{"schema":"ADM_SGV_INT","name":"AREA_CONSULTOR_ITEM_JS","comments":"ESTRUTURA PARA ARMAZENAR A INFORMA��O NO FORMATO JSON PARA ENVIO DE INTEGRA��ES","lastddltime":"08/11/2019 09:45:29","createdtime":"30/05/2019 11:34:32"}
,
{"schema":"ADM_SGV_INT","name":"ARRECADACAO_PARCEIRO_JS","comments":"ESTRUTURA PARA ARMAZENAR A INFORMA��O NO FORMATO JSON PARA ENVIO DE INTEGRA��ES","lastddltime":"29/11/2019 11:40:38","createdtime":"29/11/2019 11:40:38"}
,
{"schema":"ADM_SGV_INT","name":"BAIRRO_JS","comments":"ESTRUTURA PARA ARMAZENAR A INFORMA��O NO FORMATO JSON PARA ENVIO DE INTEGRA��ES","lastddltime":"12/05/2019 10:26:50","createdtime":"12/05/2019 10:26:17"}
,
{"schema":"ADM_SGV_INT","name":"CANAL_SAIDA_CONCESSIONARIA_JS","comments":"ESTRUTURA PARA ARMAZENAR A INFORMA��O NO FORMATO JSON PARA ENVIO DE INTEGRA��ES","lastddltime":"14/10/2019 13:41:40","createdtime":"14/10/2019 13:41:40"}
,
{"schema":"ADM_SGV_INT","name":"CANAL_SAIDA_JS","comments":"ESTRUTURA PARA ARMAZENAR A INFORMA��O NO FORMATO JSON PARA ENVIO DE INTEGRA��ES","lastddltime":"12/05/2019 10:26:50","createdtime":"12/05/2019 10:26:22"}
,
{"schema":"ADM_SGV_INT","name":"CANAL_VENDA_ESTAB_JS","comments":"ESTRUTURA PARA ARMAZENAR A INFORMA��O NO FORMATO JSON PARA ENVIO DE INTEGRA��ES","lastddltime":"15/05/2019 10:59:36","createdtime":"12/05/2019 10:26:10"}
,
{"schema":"ADM_SGV_INT","name":"CHIP_JS","comments":"Estrutura para armazenar a informa��o no formato JSON para envio de integra��es","lastddltime":"15/05/2019 10:59:36","createdtime":"12/05/2019 10:25:52"}
,
{"schema":"ADM_SGV_INT","name":"CIDADE_JS","comments":"ESTRUTURA PARA ARMAZENAR A INFORMA��O NO FORMATO JSON PARA ENVIO DE INTEGRA��ES","lastddltime":"12/05/2019 10:26:50","createdtime":"12/05/2019 10:26:15"}
,
{"schema":"ADM_SGV_INT","name":"CODIGO_RESPOSTA_JS","comments":"ESTRUTURA PARA ARMAZENAR A INFORMA��O NO FORMATO JSON PARA ENVIO DE INTEGRA��ES","lastddltime":"15/05/2019 10:59:36","createdtime":"12/05/2019 10:26:05"}
,
{"schema":"ADM_SGV_INT","name":"CONCESSIONARIA_JS","comments":"ESTRUTURA PARA ARMAZENAR A INFORMA��O NO FORMATO JSON PARA ENVIO DE INTEGRA��ES","lastddltime":"15/05/2019 10:59:36","createdtime":"12/05/2019 10:26:06"}
,
{"schema":"ADM_SGV_INT","name":"CONFIGURACAO_EMPRESA_JS","comments":"Estrutura para armazenar a informa��o no formato JSON para envio de integra��es","lastddltime":"15/05/2019 10:59:36","createdtime":"12/05/2019 10:25:53"}
,
{"schema":"ADM_SGV_INT","name":"CONTATO_JS","comments":"ESTRUTURA PARA ARMAZENAR A INFORMA��O NO FORMATO JSON PARA ENVIO DE INTEGRA��ES","lastddltime":"12/05/2019 10:26:50","createdtime":"12/05/2019 10:26:22"}
,
{"schema":"ADM_SGV_INT","name":"CURVA_ESTABELECIMENTO_EMPRE_JS","comments":"ESTRUTURA PARA ARMAZENAR A INFORMA��O NO FORMATO JSON PARA ENVIO DE INTEGRA��ES","lastddltime":"12/05/2019 10:26:50","createdtime":"12/05/2019 10:26:20"}
,
{"schema":"ADM_SGV_INT","name":"EMPRESA_JS","comments":"Estrutura para armazenar a informa��o no formato JSON para envio de integra��es","lastddltime":"15/05/2019 10:59:36","createdtime":"12/05/2019 10:25:54"}
,
{"schema":"ADM_SGV_INT","name":"ESTABELECIMENTO_EMPRESA_JS","comments":"Estrutura para armazenar a informa��o no formato JSON para envio de integra��es","lastddltime":"15/05/2019 10:59:36","createdtime":"12/05/2019 10:25:55"}
,
{"schema":"ADM_SGV_INT","name":"ESTABELECIMENTO_JS","comments":"Estrutura para armazenar a informa��o no formato JSON para envio de integra��es","lastddltime":"15/05/2019 10:59:36","createdtime":"12/05/2019 10:25:54"}
,
{"schema":"ADM_SGV_INT","name":"ESTOQUE_AIRCLIC","comments":"","lastddltime":"13/05/2019 11:21:31","createdtime":"12/05/2019 10:26:35"}
,
{"schema":"ADM_SGV_INT","name":"FAIXA_VALOR_JS","comments":"ESTRUTURA PARA ARMAZENAR A INFORMA��O NO FORMATO JSON PARA ENVIO DE INTEGRA��ES","lastddltime":"15/05/2019 10:59:36","createdtime":"12/05/2019 10:26:06"}
,
{"schema":"ADM_SGV_INT","name":"FILIAL_CODIGO_AREA_JS","comments":"ESTRUTURA PARA ARMAZENAR A INFORMA��O NO FORMATO JSON PARA ENVIO DE INTEGRA��ES","lastddltime":"15/05/2019 10:59:36","createdtime":"12/05/2019 10:26:07"}
,
{"schema":"ADM_SGV_INT","name":"FILIAL_JS","comments":"ESTRUTURA PARA ARMAZENAR A INFORMA��O NO FORMATO JSON PARA ENVIO DE INTEGRA��ES","lastddltime":"15/05/2019 10:59:36","createdtime":"12/05/2019 10:26:06"}
,
{"schema":"ADM_SGV_INT","name":"FILIAL_TI_JS","comments":"ESTRUTURA PARA ARMAZENAR A INFORMA��O NO FORMATO JSON PARA ENVIO DE INTEGRA��ES","lastddltime":"15/05/2019 10:59:36","createdtime":"12/05/2019 10:26:07"}
,
{"schema":"ADM_SGV_INT","name":"FORNECEDOR_JS","comments":"Estrutura para armazenar a informa��o no formato JSON para envio de integra��es","lastddltime":"15/05/2019 10:59:36","createdtime":"12/05/2019 10:25:58"}
,
{"schema":"ADM_SGV_INT","name":"FORNECEDOR_NEGOCIO_JS","comments":"Estrutura para armazenar a informa��o no formato JSON para envio de integra��es","lastddltime":"15/05/2019 10:59:36","createdtime":"12/05/2019 10:25:58"}
,
{"schema":"ADM_SGV_INT","name":"FORNECEDOR_SGV_JS","comments":"ESTRUTURA PARA ARMAZENAR A INFORMA��O NO FORMATO JSON PARA ENVIO DE INTEGRA��ES","lastddltime":"15/05/2019 10:59:36","createdtime":"12/05/2019 10:26:10"}
,
{"schema":"ADM_SGV_INT","name":"GRUPO_PARCEIRO_JS","comments":"ESTRUTURA PARA ARMAZENAR A INFORMA��O NO FORMATO JSON PARA ENVIO DE INTEGRA��ES","lastddltime":"29/11/2019 11:40:02","createdtime":"29/11/2019 11:40:02"}
,
{"schema":"ADM_SGV_INT","name":"GRUPO_SERVICO_WKF","comments":"","lastddltime":"12/05/2019 10:27:04","createdtime":"12/05/2019 10:26:35"}
,
{"schema":"ADM_SGV_INT","name":"GRUPO_VENDA_AUTORIZADO_JS","comments":"ESTRUTURA PARA ARMAZENAR A INFORMA��O NO FORMATO JSON PARA ENVIO DE INTEGRA��ES","lastddltime":"15/05/2019 10:59:37","createdtime":"12/05/2019 10:26:12"}
,
{"schema":"ADM_SGV_INT","name":"GRUPO_VENDA_CRIPTOGRAFIA_JS","comments":"ESTRUTURA PARA ARMAZENAR A INFORMA��O NO FORMATO JSON PARA ENVIO DE INTEGRA��ES","lastddltime":"04/09/2019 09:54:46","createdtime":"04/09/2019 09:54:46"}
,
{"schema":"ADM_SGV_INT","name":"GRUPO_VENDA_DISTRIBUICAO_JS","comments":"ESTRUTURA PARA ARMAZENAR A INFORMA��O NO FORMATO JSON PARA ENVIO DE INTEGRA��ES","lastddltime":"14/10/2019 13:41:02","createdtime":"14/10/2019 13:41:02"}
,
{"schema":"ADM_SGV_INT","name":"GRUPO_VENDA_JS","comments":"ESTRUTURA PARA ARMAZENAR A INFORMA��O NO FORMATO JSON PARA ENVIO DE INTEGRA��ES","lastddltime":"05/11/2019 07:48:50","createdtime":"12/05/2019 10:26:12"}
,
{"schema":"ADM_SGV_INT","name":"HIST_IMPRESSAO_TERMO_ADESAO_JS","comments":"Estrutura para armazenar a informa��o no formato JSON para envio de integra��es","lastddltime":"15/05/2019 10:59:37","createdtime":"12/05/2019 10:26:02"}
,
{"schema":"ADM_SGV_INT","name":"INSTRUCAO_ATIVACAO_JS","comments":"ESTRUTURA PARA ARMAZENAR A INFORMA��O NO FORMATO JSON PARA ENVIO DE INTEGRA��ES","lastddltime":"12/05/2019 10:26:50","createdtime":"12/05/2019 10:26:19"}
,
{"schema":"ADM_SGV_INT","name":"INTEGRADOR_JS","comments":"ESTRUTURA PARA ARMAZENAR A INFORMA��O NO FORMATO JSON PARA ENVIO DE INTEGRA��ES","lastddltime":"15/05/2019 10:59:37","createdtime":"12/05/2019 10:26:11"}
,
{"schema":"ADM_SGV_INT","name":"ITEM_CONTATO_JS","comments":"ESTRUTURA PARA ARMAZENAR A INFORMA��O NO FORMATO JSON PARA ENVIO DE INTEGRA��ES","lastddltime":"12/05/2019 10:26:50","createdtime":"12/05/2019 10:26:21"}
,
{"schema":"ADM_SGV_INT","name":"LAYOUT_COMPROVANTE_JS","comments":"ESTRUTURA PARA ARMAZENAR A INFORMA��O NO FORMATO JSON PARA ENVIO DE INTEGRA��ES","lastddltime":"15/05/2019 10:59:37","createdtime":"12/05/2019 10:26:10"}
,
{"schema":"ADM_SGV_INT","name":"LIMITE_JS","comments":"ESTRUTURA PARA ARMAZENAR A INFORMA��O NO FORMATO JSON PARA ENVIO DE INTEGRA��ES","lastddltime":"15/05/2019 10:59:37","createdtime":"12/05/2019 10:26:13"}
,
{"schema":"ADM_SGV_INT","name":"LIMITE_SALDO_BAIXA_JS","comments":"ESTRUTURA PARA ARMAZENAR A INFORMA��O NO FORMATO JSON PARA ENVIO DE INTEGRA��ES","lastddltime":"15/05/2019 10:59:37","createdtime":"12/05/2019 10:26:14"}
,
{"schema":"ADM_SGV_INT","name":"LIMITE_SALDO_LIMITE_JS","comments":"ESTRUTURA PARA ARMAZENAR A INFORMA��O NO FORMATO JSON PARA ENVIO DE INTEGRA��ES","lastddltime":"15/05/2019 10:59:37","createdtime":"12/05/2019 10:26:13"}
,
{"schema":"ADM_SGV_INT","name":"LIMITE_SALDO_TRN_JS","comments":"ESTRUTURA PARA ARMAZENAR A INFORMA��O NO FORMATO JSON PARA ENVIO DE INTEGRA��ES","lastddltime":"15/05/2019 10:59:37","createdtime":"12/05/2019 10:26:14"}
,
{"schema":"ADM_SGV_INT","name":"MC_ENDERECO_JS","comments":"ESTRUTURA PARA ARMAZENAR A INFORMA��O NO FORMATO JSON PARA ENVIO DE INTEGRA��ES","lastddltime":"12/05/2019 10:26:50","createdtime":"12/05/2019 10:26:20"}
,
{"schema":"ADM_SGV_INT","name":"MERCHAN_JS","comments":"ESTRUTURA PARA ARMAZENAR A INFORMA��O NO FORMATO JSON PARA ENVIO DE INTEGRA��ES","lastddltime":"08/11/2019 09:45:28","createdtime":"30/05/2019 11:33:32"}
,
{"schema":"ADM_SGV_INT","name":"MODELO_JS","comments":"Estrutura para armazenar a informa��o no formato JSON para envio de integra��es","lastddltime":"15/05/2019 10:59:37","createdtime":"12/05/2019 10:25:59"}
,
{"schema":"ADM_SGV_INT","name":"NEGOCIACAO_COMPARTILHA_JS","comments":"Estrutura para armazenar a informa��o no formato JSON para envio de integra��es","lastddltime":"15/05/2019 10:59:37","createdtime":"12/05/2019 10:25:55"}
,
{"schema":"ADM_SGV_INT","name":"NEGOCIO_JS","comments":"Estrutura para armazenar a informa��o no formato JSON para envio de integra��es","lastddltime":"15/05/2019 10:59:37","createdtime":"12/05/2019 10:25:58"}
,
{"schema":"ADM_SGV_INT","name":"OFERTA_FORNECEDOR_DDD_JS","comments":"ESTRUTURA PARA ARMAZENAR A INFORMA��O NO FORMATO JSON PARA ENVIO DE INTEGRA��ES","lastddltime":"08/11/2019 09:10:09","createdtime":"08/11/2019 09:10:08"}
,
{"schema":"ADM_SGV_INT","name":"OFERTA_FORNECEDOR_FORNECEDO_JS","comments":"ESTRUTURA PARA ARMAZENAR A INFORMA��O NO FORMATO JSON PARA ENVIO DE INTEGRA��ES","lastddltime":"08/11/2019 09:10:12","createdtime":"08/11/2019 09:10:12"}
,
{"schema":"ADM_SGV_INT","name":"PARAMETRO_CONCESSIONARIA_JS","comments":"ESTRUTURA PARA ARMAZENAR A INFORMA��O NO FORMATO JSON PARA ENVIO DE INTEGRA��ES","lastddltime":"15/05/2019 10:59:37","createdtime":"12/05/2019 10:26:09"}
,
{"schema":"ADM_SGV_INT","name":"PARAMETRO_JS","comments":"ESTRUTURA PARA ARMAZENAR A INFORMA��O NO FORMATO JSON PARA ENVIO DE INTEGRA��ES","lastddltime":"15/05/2019 10:59:37","createdtime":"12/05/2019 10:26:08"}
,
{"schema":"ADM_SGV_INT","name":"PEDIDOS","comments":"","lastddltime":"13/05/2019 11:21:31","createdtime":"12/05/2019 10:26:35"}
,
{"schema":"ADM_SGV_INT","name":"PEDIDOS_BACKUP","comments":"","lastddltime":"12/05/2019 10:26:35","createdtime":"12/05/2019 10:26:35"}
,
{"schema":"ADM_SGV_INT","name":"PEDIDOS_ER","comments":"","lastddltime":"13/05/2019 11:21:31","createdtime":"12/05/2019 10:26:35"}
,
{"schema":"ADM_SGV_INT","name":"PEDIDOS_HS","comments":"","lastddltime":"12/05/2019 10:26:38","createdtime":"12/05/2019 10:26:35"}
,
{"schema":"ADM_SGV_INT","name":"PORTFOLIO_JS","comments":"Estrutura para armazenar a informa��o no formato JSON para envio de integra��es","lastddltime":"15/05/2019 10:59:37","createdtime":"12/05/2019 10:25:56"}
,
{"schema":"ADM_SGV_INT","name":"PRODUTO_JS","comments":"ESTRUTURA PARA ARMAZENAR A INFORMA��O NO FORMATO JSON PARA ENVIO DE INTEGRA��ES","lastddltime":"15/05/2019 10:59:37","createdtime":"12/05/2019 10:25:59"}
,
{"schema":"ADM_SGV_INT","name":"PRODUTO_PARAMETRO_JS","comments":"ESTRUTURA PARA ARMAZENAR A INFORMA��O NO FORMATO JSON PARA ENVIO DE INTEGRA��ES","lastddltime":"25/11/2019 16:14:04","createdtime":"25/11/2019 16:08:33"}
,
{"schema":"ADM_SGV_INT","name":"PRODUTO_PARCEIRO_JS","comments":"ESTRUTURA PARA ARMAZENAR A INFORMA��O NO FORMATO JSON PARA ENVIO DE INTEGRA��ES","lastddltime":"12/11/2019 15:01:47","createdtime":"22/08/2019 11:16:01"}
,
{"schema":"ADM_SGV_INT","name":"PRODUTO_VENDA_JS","comments":"Estrutura para armazenar a informa��o no formato JSON para envio de integra��es","lastddltime":"15/05/2019 10:59:37","createdtime":"12/05/2019 10:26:00"}
,
{"schema":"ADM_SGV_INT","name":"PROPRIEDADES_PUBLICACAO_JS","comments":"ESTRUTURA PARA ARMAZENAR A INFORMA��O NO FORMATO JSON PARA ENVIO DE INTEGRA��ES","lastddltime":"08/11/2019 09:45:28","createdtime":"30/05/2019 11:34:28"}
,
{"schema":"ADM_SGV_INT","name":"PUBLICACAO_DATA_MERCHAN_JS","comments":"ESTRUTURA PARA ARMAZENAR A INFORMA��O NO FORMATO JSON PARA ENVIO DE INTEGRA��ES","lastddltime":"08/11/2019 09:45:29","createdtime":"30/05/2019 11:34:30"}
,
{"schema":"ADM_SGV_INT","name":"PUBLICACAO_JS","comments":"ESTRUTURA PARA ARMAZENAR A INFORMA��O NO FORMATO JSON PARA ENVIO DE INTEGRA��ES","lastddltime":"08/11/2019 09:45:28","createdtime":"30/05/2019 11:34:00"}
,
{"schema":"ADM_SGV_INT","name":"REDE_JS","comments":"ESTRUTURA PARA ARMAZENAR A INFORMA��O NO FORMATO JSON PARA ENVIO DE INTEGRA��ES","lastddltime":"15/05/2019 10:59:38","createdtime":"12/05/2019 10:26:11"}
,
{"schema":"ADM_SGV_INT","name":"SALDOS_JS","comments":"ESTRUTURA PARA ARMAZENAR A INFORMA��O NO FORMATO JSON PARA ENVIO DE INTEGRA��ES","lastddltime":"15/05/2019 10:59:38","createdtime":"12/05/2019 10:26:14"}
,
{"schema":"ADM_SGV_INT","name":"SERVICO_WKF","comments":"Armazenar servico wkf","lastddltime":"12/05/2019 10:27:04","createdtime":"12/05/2019 10:26:34"}
,
{"schema":"ADM_SGV_INT","name":"SIG_BAIRROS_CT_JS","comments":"ESTRUTURA PARA ARMAZENAR A INFORMA��O NO FORMATO JSON PARA ENVIO DE INTEGRA��ES","lastddltime":"12/05/2019 10:26:50","createdtime":"12/05/2019 10:26:17"}
,
{"schema":"ADM_SGV_INT","name":"SIG_CIDADES_CT_JS","comments":"ESTRUTURA PARA ARMAZENAR A INFORMA��O NO FORMATO JSON PARA ENVIO DE INTEGRA��ES","lastddltime":"12/05/2019 10:26:50","createdtime":"12/05/2019 10:26:15"}
,
{"schema":"ADM_SGV_INT","name":"SIG_GERENTES_CT_JS","comments":"ESTRUTURA PARA ARMAZENAR A INFORMA��O NO FORMATO JSON PARA ENVIO DE INTEGRA��ES","lastddltime":"12/05/2019 10:26:50","createdtime":"12/05/2019 10:26:19"}
,
{"schema":"ADM_SGV_INT","name":"SIG_MOTIVOS_VISITAS_CT_JS","comments":"ESTRUTURA PARA ARMAZENAR A INFORMA��O NO FORMATO JSON PARA ENVIO DE INTEGRA��ES","lastddltime":"12/05/2019 10:26:50","createdtime":"12/05/2019 10:26:18"}
,
{"schema":"ADM_SGV_INT","name":"SIG_SUPERVISORES_CT_JS","comments":"ESTRUTURA PARA ARMAZENAR A INFORMA��O NO FORMATO JSON PARA ENVIO DE INTEGRA��ES","lastddltime":"12/05/2019 10:26:50","createdtime":"12/05/2019 10:26:18"}
,
{"schema":"ADM_SGV_INT","name":"SIG_TIPOSCLIENTES_CT_JS","comments":"ESTRUTURA PARA ARMAZENAR A INFORMA��O NO FORMATO JSON PARA ENVIO DE INTEGRA��ES","lastddltime":"12/05/2019 10:26:50","createdtime":"12/05/2019 10:26:16"}
,
{"schema":"ADM_SGV_INT","name":"SIG_TIPOSLOGRADOUROS_CT_JS","comments":"ESTRUTURA PARA ARMAZENAR A INFORMA��O NO FORMATO JSON PARA ENVIO DE INTEGRA��ES","lastddltime":"12/05/2019 10:26:50","createdtime":"12/05/2019 10:26:16"}
,
{"schema":"ADM_SGV_INT","name":"SIG_VENDEDORES_CT_JS","comments":"ESTRUTURA PARA ARMAZENAR A INFORMA��O NO FORMATO JSON PARA ENVIO DE INTEGRA��ES","lastddltime":"12/05/2019 10:26:50","createdtime":"12/05/2019 10:26:18"}
,
{"schema":"ADM_SGV_INT","name":"TERMINAL_JS","comments":"Estrutura para armazenar a informa��o no formato JSON para envio de integra��es","lastddltime":"15/05/2019 10:59:38","createdtime":"12/05/2019 10:25:57"}
,
{"schema":"ADM_SGV_INT","name":"TERMO_ADESAO_ESTAB_EMPRESA_JS","comments":"Estrutura para armazenar a informa��o no formato JSON para envio de integra��es","lastddltime":"15/05/2019 10:59:38","createdtime":"12/05/2019 10:26:01"}
,
{"schema":"ADM_SGV_INT","name":"TERMO_ADESAO_JS","comments":"Estrutura para armazenar a informa��o no formato JSON para envio de integra��es","lastddltime":"15/05/2019 10:59:38","createdtime":"12/05/2019 10:26:01"}
,
{"schema":"ADM_SGV_INT","name":"TIPO_LOGRADOURO_JS","comments":"ESTRUTURA PARA ARMAZENAR A INFORMA��O NO FORMATO JSON PARA ENVIO DE INTEGRA��ES","lastddltime":"12/05/2019 10:26:50","createdtime":"12/05/2019 10:26:16"}
,
{"schema":"ADM_SGV_INT","name":"TIPO_TRANSACAO_JS","comments":"ESTRUTURA PARA ARMAZENAR A INFORMA��O NO FORMATO JSON PARA ENVIO DE INTEGRA��ES","lastddltime":"15/05/2019 10:59:38","createdtime":"12/05/2019 10:26:12"}
,
{"schema":"ADM_SGV_INT","name":"USUARIO_JS","comments":"ESTRUTURA PARA ARMAZENAR A INFORMA��O NO FORMATO JSON PARA ENVIO DE INTEGRA��ES","lastddltime":"15/05/2019 10:59:38","createdtime":"12/05/2019 10:26:05"}
,
{"schema":"ADM_SGV_INT","name":"VALOR_FIXO_JS","comments":"ESTRUTURA PARA ARMAZENAR A INFORMA��O NO FORMATO JSON PARA ENVIO DE INTEGRA��ES","lastddltime":"15/05/2019 10:59:38","createdtime":"12/05/2019 10:26:09"}
,
{"schema":"ADM_SGV_INT","name":"VALOR_NEGOCIADO_JS","comments":"ESTRUTURA PARA ARMAZENAR A INFORMA��O NO FORMATO JSON PARA ENVIO DE INTEGRA��ES","lastddltime":"15/05/2019 10:59:38","createdtime":"12/05/2019 10:26:12"}
,
{"schema":"ADM_SGV_INT","name":"VALOR_POSSIVEL_JS","comments":"ESTRUTURA PARA ARMAZENAR A INFORMA��O NO FORMATO JSON PARA ENVIO DE INTEGRA��ES","lastddltime":"15/05/2019 10:59:38","createdtime":"12/05/2019 10:26:11"}
,
{"schema":"ADM_SGV_INT","name":"VERSAO_APLICATIVO_JS","comments":"ESTRUTURA PARA ARMAZENAR A INFORMA��O NO FORMATO JSON PARA ENVIO DE INTEGRA��ES","lastddltime":"08/11/2019 09:45:30","createdtime":"30/05/2019 11:34:34"}
,
{"schema":"ADM_SGV_INT","name":"VIGENCIA_DDD_JS","comments":"Estrutura para armazenar a informa��o no formato JSON para envio de integra��es","lastddltime":"15/05/2019 10:59:38","createdtime":"12/05/2019 10:25:57"}
,
{"schema":"ADM_SGV_JDE","name":"F0101Z2_HS","comments":"","lastddltime":"12/05/2019 11:17:00","createdtime":"12/05/2019 11:03:04"}
,
{"schema":"ADM_SGV_JDE","name":"F0111Z1_HS","comments":"","lastddltime":"12/05/2019 11:17:00","createdtime":"12/05/2019 11:03:03"}
,
{"schema":"ADM_SGV_JDE","name":"F01151Z1_HS","comments":"","lastddltime":"12/05/2019 12:54:26","createdtime":"12/05/2019 11:03:03"}
,
{"schema":"ADM_SGV_JDE","name":"F03012Z1_HS","comments":"","lastddltime":"12/05/2019 11:17:00","createdtime":"12/05/2019 11:03:04"}
,
{"schema":"ADM_SGV_JDE","name":"F0401Z1_HS","comments":"","lastddltime":"12/05/2019 11:17:00","createdtime":"12/05/2019 11:03:03"}
,
{"schema":"ADM_SGV_JDE","name":"F5547001_ER","comments":"","lastddltime":"13/05/2019 11:41:53","createdtime":"12/05/2019 11:03:03"}
,
{"schema":"ADM_SGV_JDE","name":"F5547001_HS","comments":"","lastddltime":"12/05/2019 11:17:00","createdtime":"12/05/2019 11:03:03"}
,
{"schema":"ADM_SGV_JDE","name":"F5547300_ER","comments":"","lastddltime":"12/05/2019 11:17:00","createdtime":"12/05/2019 11:03:02"}
,
{"schema":"ADM_SGV_JDE","name":"F5547300_HS","comments":"","lastddltime":"12/05/2019 12:54:26","createdtime":"12/05/2019 11:03:02"}
,
{"schema":"ADM_SGV_JDE","name":"F5547301_ER","comments":"","lastddltime":"12/05/2019 11:17:00","createdtime":"12/05/2019 11:03:02"}
,
{"schema":"ADM_SGV_JDE","name":"F5547301_HS","comments":"","lastddltime":"12/05/2019 12:54:26","createdtime":"12/05/2019 11:03:02"}
,
{"schema":"ADM_SGV_JDE","name":"JDE_AGENDAMENTO_ENVIO","comments":"","lastddltime":"12/05/2019 12:54:26","createdtime":"12/05/2019 11:03:03"}
,
{"schema":"ADM_SGV_JDE","name":"JDE_BAIXA","comments":"","lastddltime":"12/05/2019 12:15:22","createdtime":"12/05/2019 11:03:02"}
,
{"schema":"ADM_SGV_JDE","name":"JDE_BAIXA_LOG","comments":"","lastddltime":"25/07/2019 15:57:42","createdtime":"25/07/2019 15:57:41"}
,
{"schema":"ADM_SGV_JDE","name":"JDE_BAIXA_N2","comments":"","lastddltime":"12/05/2019 12:15:22","createdtime":"12/05/2019 11:03:02"}
,
{"schema":"ADM_SGV_JDE","name":"JDE_BLOQUEIO","comments":"","lastddltime":"12/05/2019 12:54:26","createdtime":"12/05/2019 11:03:03"}
,
{"schema":"ADM_SGV_JDE","name":"JDE_BLOQUEIO_EMPRESA","comments":"","lastddltime":"12/05/2019 12:54:26","createdtime":"12/05/2019 11:03:03"}
,
{"schema":"ADM_SGV_JDE","name":"JDE_CI_COBRANCA","comments":"","lastddltime":"12/05/2019 11:17:00","createdtime":"12/05/2019 11:03:04"}
,
{"schema":"ADM_SGV_JDE","name":"JDE_CLI_EXCLUIR","comments":"","lastddltime":"12/05/2019 11:17:00","createdtime":"12/05/2019 11:03:03"}
,
{"schema":"ADM_SGV_JDE","name":"JDE_COB_CANCELADAS","comments":"","lastddltime":"13/05/2019 11:41:52","createdtime":"12/05/2019 11:03:04"}
,
{"schema":"ADM_SGV_JDE","name":"JDE_COB_CANCELADAS_LOG","comments":"","lastddltime":"02/08/2019 11:29:30","createdtime":"18/07/2019 14:33:05"}
,
{"schema":"ADM_SGV_JDE","name":"JDE_COB_CANCELADAS_N2","comments":"","lastddltime":"13/05/2019 11:41:54","createdtime":"12/05/2019 11:03:02"}
,
{"schema":"ADM_SGV_JDE","name":"JDE_COB_PRE_PAGO","comments":"","lastddltime":"12/05/2019 12:15:21","createdtime":"12/05/2019 11:03:02"}
,
{"schema":"ADM_SGV_JDE","name":"JDE_COB_PRE_PAGO_LOG","comments":"","lastddltime":"02/08/2019 11:29:31","createdtime":"18/07/2019 14:33:06"}
,
{"schema":"ADM_SGV_JDE","name":"JDE_COB_PRE_PAGO_N2","comments":"","lastddltime":"26/06/2019 16:21:35","createdtime":"26/06/2019 16:21:35"}
,
{"schema":"ADM_SGV_JDE","name":"JDE_COBRANCAS","comments":"","lastddltime":"13/05/2019 11:41:52","createdtime":"12/05/2019 11:03:04"}
,
{"schema":"ADM_SGV_JDE","name":"JDE_COBRANCAS_LOG","comments":"","lastddltime":"02/08/2019 11:29:28","createdtime":"18/07/2019 14:33:03"}
,
{"schema":"ADM_SGV_JDE","name":"JDE_COBRANCAS_LOG_RV","comments":"","lastddltime":"25/07/2019 16:15:18","createdtime":"25/07/2019 16:13:53"}
,
{"schema":"ADM_SGV_JDE","name":"JDE_COBRANCAS_N2","comments":"","lastddltime":"13/05/2019 11:41:54","createdtime":"12/05/2019 11:03:02"}
,
{"schema":"ADM_SGV_JDE","name":"JDE_COBRANCAS_N2_RV","comments":"","lastddltime":"13/05/2019 11:41:53","createdtime":"12/05/2019 11:03:02"}
,
{"schema":"ADM_SGV_JDE","name":"JDE_COB_REGERADAS","comments":"","lastddltime":"13/05/2019 11:41:52","createdtime":"12/05/2019 11:03:04"}
,
{"schema":"ADM_SGV_JDE","name":"JDE_COB_REGERADAS_LOG","comments":"","lastddltime":"02/08/2019 11:29:29","createdtime":"18/07/2019 14:33:04"}
,
{"schema":"ADM_SGV_JDE","name":"JDE_COB_REGERADAS_N2","comments":"","lastddltime":"13/05/2019 11:41:54","createdtime":"12/05/2019 11:03:02"}
,
{"schema":"ADM_SGV_JDE","name":"JDE_COMPARTILHAMENTO","comments":"","lastddltime":"13/05/2019 11:41:51","createdtime":"12/05/2019 11:03:04"}
,
{"schema":"ADM_SGV_JDE","name":"JDE_COMPARTILHAMENTO_N2","comments":"","lastddltime":"13/05/2019 11:41:55","createdtime":"12/05/2019 11:03:01"}
,
{"schema":"ADM_SGV_JDE","name":"JDE_CONTROLE_ENVIO","comments":"","lastddltime":"12/05/2019 12:54:26","createdtime":"12/05/2019 11:03:03"}
,
{"schema":"ADM_SGV_JDE","name":"JDE_EXEC","comments":"","lastddltime":"12/05/2019 12:54:27","createdtime":"12/05/2019 11:03:04"}
,
{"schema":"ADM_SGV_JDE","name":"JDE_EXEC_DET","comments":"","lastddltime":"26/09/2019 11:09:36","createdtime":"12/05/2019 11:03:03"}
,
{"schema":"ADM_SGV_JDE","name":"JDE_EXTRACAO","comments":"","lastddltime":"13/05/2019 11:41:53","createdtime":"12/05/2019 11:03:03"}
,
{"schema":"ADM_SGV_JDE","name":"JDE_EXTRACAO_ER","comments":"","lastddltime":"12/05/2019 11:17:00","createdtime":"12/05/2019 11:03:03"}
,
{"schema":"ADM_SGV_JDE","name":"JDE_EXTRACAO_ER_LOG","comments":"","lastddltime":"02/08/2019 11:29:01","createdtime":"18/07/2019 12:03:28"}
,
{"schema":"ADM_SGV_JDE","name":"JDE_EXTRACAO_ER_N2","comments":"","lastddltime":"12/05/2019 11:17:00","createdtime":"12/05/2019 11:03:01"}
,
{"schema":"ADM_SGV_JDE","name":"JDE_EXTRACAO_LOG","comments":"","lastddltime":"02/08/2019 11:29:00","createdtime":"18/07/2019 12:03:27"}
,
{"schema":"ADM_SGV_JDE","name":"JDE_EXTRACAO_N2","comments":"","lastddltime":"13/05/2019 11:41:54","createdtime":"12/05/2019 11:03:02"}
,
{"schema":"ADM_SGV_JDE","name":"JDE_HISTORICO_INTEGRACAO","comments":"","lastddltime":"12/05/2019 12:15:20","createdtime":"12/05/2019 11:03:03"}
,
{"schema":"ADM_SGV_JDE","name":"JDE_IMPORTACOES_PIN","comments":"","lastddltime":"13/05/2019 11:41:53","createdtime":"12/05/2019 11:03:03"}
,
{"schema":"ADM_SGV_JDE","name":"JDE_LAYOUT","comments":"Armazena os layouts das integra��es realizadas entre o SGV e o JDE","lastddltime":"12/05/2019 12:54:26","createdtime":"12/05/2019 11:03:02"}
,
{"schema":"ADM_SGV_JDE","name":"JDE_REDES_EXCLUIR","comments":"","lastddltime":"12/05/2019 12:54:26","createdtime":"12/05/2019 11:03:03"}
,
{"schema":"ADM_SGV_JDE","name":"JDE_REEXEC","comments":"","lastddltime":"13/05/2019 11:41:53","createdtime":"12/05/2019 11:03:03"}
,
{"schema":"ADM_SGV_JDE","name":"JDE_REPASSES","comments":"","lastddltime":"05/08/2019 16:20:40","createdtime":"12/05/2019 11:03:04"}
,
{"schema":"ADM_SGV_JDE","name":"JDE_REPASSES_LOG","comments":"","lastddltime":"02/08/2019 11:30:26","createdtime":"25/07/2019 15:32:38"}
,
{"schema":"ADM_SGV_JDE","name":"JDE_REPASSES_N2","comments":"","lastddltime":"13/05/2019 11:41:54","createdtime":"12/05/2019 11:03:02"}
,
{"schema":"ADM_SGV_JDE","name":"JDE_SERASA","comments":"Tabela para armazenar hist�rico de consulta serasa por empresa.","lastddltime":"12/05/2019 12:54:26","createdtime":"12/05/2019 11:03:02"}
,
{"schema":"ADM_SGV_JDE","name":"JDE_SERVICOS","comments":"","lastddltime":"05/08/2019 16:25:52","createdtime":"12/05/2019 11:03:04"}
,
{"schema":"ADM_SGV_JDE","name":"JDE_TRANSACOES","comments":"","lastddltime":"05/08/2019 16:28:56","createdtime":"12/05/2019 11:03:04"}
,
{"schema":"ADM_SGV_JDE","name":"JDE_TRANSACOES_LOG","comments":"","lastddltime":"08/11/2019 11:16:22","createdtime":"19/07/2019 10:21:34"}
,
{"schema":"ADM_SGV_JDE","name":"JDE_TRANSACOES_LOG_RV","comments":"","lastddltime":"25/07/2019 16:15:17","createdtime":"25/07/2019 16:13:52"}
,
{"schema":"ADM_SGV_JDE","name":"JDE_TRANSACOES_N2","comments":"","lastddltime":"13/05/2019 11:41:54","createdtime":"12/05/2019 11:03:02"}
,
{"schema":"ADM_SGV_JDE","name":"JDE_TRANSACOES_N2_RV","comments":"","lastddltime":"13/05/2019 11:41:53","createdtime":"12/05/2019 11:03:02"}
,
{"schema":"ADM_SGV_JDE","name":"JDE_TRANSFERENCIAS","comments":"","lastddltime":"13/05/2019 11:41:51","createdtime":"12/05/2019 11:03:04"}
,
{"schema":"ADM_SGV_JDE","name":"JDE_TRANSFERENCIAS_LOG","comments":"","lastddltime":"02/08/2019 11:30:27","createdtime":"25/07/2019 15:32:39"}
,
{"schema":"ADM_SGV_JDE","name":"JDE_TRANSFERENCIAS_N2","comments":"","lastddltime":"13/05/2019 11:41:54","createdtime":"12/05/2019 11:03:02"}
,
{"schema":"ADM_SGV_JDE","name":"JDE_VISITAS_CONSULTOR","comments":"","lastddltime":"12/05/2019 12:54:26","createdtime":"12/05/2019 11:03:03"}
,
{"schema":"ADM_SGV_JDE","name":"LOG_JDE_EXEC","comments":"","lastddltime":"12/05/2019 12:15:20","createdtime":"12/05/2019 11:03:03"}
,
{"schema":"ADM_SGV_PIN","name":"AGENDA_LANCAMENTO","comments":"Armazena informa��es de agendamento de lan�amento","lastddltime":"13/05/2019 11:42:15","createdtime":"12/05/2019 11:02:44"}
,
{"schema":"ADM_SGV_PIN","name":"BLOQUEIO","comments":"Bloqueios efetuados aos estabelecimentos devido a falta de pagamento e/ou quebra de cl�usulas contratuais","lastddltime":"13/05/2019 11:42:14","createdtime":"12/05/2019 11:02:44"}
,
{"schema":"ADM_SGV_PIN","name":"BLOQUEIO_EMPRESA","comments":"Armazena informa��es de bloqueio da empresa","lastddltime":"13/05/2019 11:42:14","createdtime":"12/05/2019 11:02:44"}
,
{"schema":"ADM_SGV_PIN","name":"BLOQUEIO_PIN","comments":"Armazena informa��es de bloqueio do PIN","lastddltime":"13/05/2019 11:42:19","createdtime":"12/05/2019 11:02:43"}
,
{"schema":"ADM_SGV_PIN","name":"CANAL_SAIDA","comments":"","lastddltime":"04/10/2019 13:48:33","createdtime":"12/05/2019 11:02:42"}
,
{"schema":"ADM_SGV_PIN","name":"CANAL_SAIDA_COD_PROCESSAMENTO","comments":"","lastddltime":"18/10/2019 10:06:57","createdtime":"07/10/2019 09:04:24"}
,
{"schema":"ADM_SGV_PIN","name":"CANAL_SAIDA_CONCESSIONARIA","comments":"","lastddltime":"18/10/2019 10:05:48","createdtime":"07/10/2019 09:24:48"}
,
{"schema":"ADM_SGV_PIN","name":"COMPOSICAO_ARQUIVO_PIN","comments":"Armazena informa��es de composi��o de arquivo_pin","lastddltime":"13/05/2019 11:42:18","createdtime":"12/05/2019 11:02:43"}
,
{"schema":"ADM_SGV_PIN","name":"DESTINACAO_VALOR","comments":"S�o os itens que podem ser mensurados automaticamente sempre que a venda de um produto � efetuada no que diz respeito � despesa. Isto �, debito para o destino. Esta despesa � direcionada � um n�vel hier�rquico espec�fico de acordo com o mapeamento do neg�cio. Pode-se por exemplo, gerar uma taxa � loja pelo custo da transa��o. Os  valores apurados s�o calculados com base na regra de cada item de custo. A venda de um produto pode gerar ou n�o 1 ou mais itens de custo para diversos n�veis ao mesmo tempo. Deve ser obrigatoriamente uma transa��o de d�bito EX: TRANSA��O DE VENDA DE UM PRODUTO GERA: Taxa de transa��o -> Destino Loja Percentual sobre o valor da transa��o -> Destino Empresa OBS: Outros processos podem fazer lan�amentos de custo quando estes n�o forem diretamente relacionados � venda, mas � um conjunto de informa��es","lastddltime":"15/05/2019 10:59:38","createdtime":"12/05/2019 11:02:44"}
,
{"schema":"ADM_SGV_PIN","name":"DESTINACAO_VALOR_20_09","comments":"","lastddltime":"12/05/2019 11:02:42","createdtime":"12/05/2019 11:02:42"}
,
{"schema":"ADM_SGV_PIN","name":"ESTOQUE_PIN","comments":"PINs importados para a empresa. Dispon�veis para a compra aos estabelecimentos da empresa","lastddltime":"18/09/2019 21:02:28","createdtime":"12/05/2019 11:02:43"}
,
{"schema":"ADM_SGV_PIN","name":"ESTOQUE_PIN_REATIVACAO","comments":"Armazena informa��es do estoque_pin reativados","lastddltime":"13/05/2019 11:42:22","createdtime":"12/05/2019 11:02:42"}
,
{"schema":"ADM_SGV_PIN","name":"ESTORNO_PIN","comments":"PINs Estornados por algum motivo","lastddltime":"12/05/2019 12:34:34","createdtime":"12/05/2019 11:02:43"}
,
{"schema":"ADM_SGV_PIN","name":"FORNECEDOR_NEG_LAYOUT_PIN","comments":"Armazena informa��es de relacionamento do layout_pin aos fornecedores por negocio","lastddltime":"13/05/2019 11:42:17","createdtime":"12/05/2019 11:02:43"}
,
{"schema":"ADM_SGV_PIN","name":"GRUPO_VENDA","comments":"Armazena informa��es do Grupo de Venda","lastddltime":"18/10/2019 10:05:47","createdtime":"12/05/2019 11:02:43"}
,
{"schema":"ADM_SGV_PIN","name":"GRUPO_VENDA_AUTORIZADO","comments":"Armazena informa��es do grupo de venda autorizado","lastddltime":"15/05/2019 10:59:38","createdtime":"12/05/2019 11:02:43"}
,
{"schema":"ADM_SGV_PIN","name":"GRUPO_VENDA_BALANCEAMENTO","comments":"","lastddltime":"06/11/2019 15:52:40","createdtime":"06/11/2019 15:52:40"}
,
{"schema":"ADM_SGV_PIN","name":"GRUPO_VENDA_CRIPTOGRAFIA","comments":"","lastddltime":"11/11/2019 13:27:48","createdtime":"11/11/2019 13:27:48"}
,
{"schema":"ADM_SGV_PIN","name":"GRUPO_VENDA_DISTRIBUICAO","comments":"","lastddltime":"18/10/2019 10:06:29","createdtime":"07/10/2019 09:16:45"}
,
{"schema":"ADM_SGV_PIN","name":"HISTORICO_CONSULTA_PIN","comments":"Hist�rico de consulta de senha PIN","lastddltime":"12/05/2019 12:34:33","createdtime":"12/05/2019 11:02:42"}
,
{"schema":"ADM_SGV_PIN","name":"HISTORICO_ESTOQUE_PIN","comments":"Entidade respons�vel por armazenar o historico do estoque de PIN","lastddltime":"13/05/2019 11:42:20","createdtime":"12/05/2019 11:02:42"}
,
{"schema":"ADM_SGV_PIN","name":"ITEM_AGENDADO","comments":"Item agendado para o estabelecimento, este vai conter os lan�amentos que ser�o programados","lastddltime":"13/05/2019 11:42:15","createdtime":"12/05/2019 11:02:44"}
,
{"schema":"ADM_SGV_PIN","name":"ITEM_SOLICITADO","comments":"Itens solicitados em uma transfer�ncia","lastddltime":"13/05/2019 11:42:13","createdtime":"12/05/2019 11:02:44"}
,
{"schema":"ADM_SGV_PIN","name":"ITEM_TRANSFERIDO","comments":"Cadastro de Itens Transferidos","lastddltime":"13/05/2019 11:42:19","createdtime":"12/05/2019 11:02:42"}
,
{"schema":"ADM_SGV_PIN","name":"LAYOUT_PIN","comments":"Armazena informa��es de layout_pin","lastddltime":"12/05/2019 12:34:34","createdtime":"12/05/2019 11:02:43"}
,
{"schema":"ADM_SGV_PIN","name":"LAYOUT_PIN_COMPOSICAO","comments":"Armazena informa��es que comp�e o layout_pin","lastddltime":"13/05/2019 11:42:18","createdtime":"12/05/2019 11:02:43"}
,
{"schema":"ADM_SGV_PIN","name":"LOG_AGENDA","comments":"Armazena o log de execucao da agenda","lastddltime":"13/05/2019 11:42:20","createdtime":"12/05/2019 11:02:42"}
,
{"schema":"ADM_SGV_PIN","name":"LOG_GRUPO_VENDA","comments":"Armazena informa��es de log do produto_venda","lastddltime":"13/05/2019 11:42:16","createdtime":"12/05/2019 11:02:43"}
,
{"schema":"ADM_SGV_PIN","name":"LOTE_PIN","comments":"Armazena informa��es do lote_pin","lastddltime":"13/05/2019 11:42:17","createdtime":"12/05/2019 11:02:43"}
,
{"schema":"ADM_SGV_PIN","name":"LOTE_PIN_RESUMO","comments":"Armazena informa��es resumidas do lote_pin","lastddltime":"13/05/2019 11:42:18","createdtime":"12/05/2019 11:02:43"}
,
{"schema":"ADM_SGV_PIN","name":"PRODUTO_VENDA","comments":"� o relacionamento do cliente final com o produto, mais especificamente, � o acordo de negocio entre a Tendencia e os demais subn�veis inferiores no que se refere � permiss�o de venda e regras de neg�cio. Prev� a temporalidade das regras, isto �, s�o de acordo com o per�odo de vig�ncia dos contratos","lastddltime":"01/11/2019 11:44:10","createdtime":"12/05/2019 11:02:45"}
,
{"schema":"ADM_SGV_PIN","name":"PRODUTO_VENDA_BKP","comments":"","lastddltime":"12/05/2019 11:02:42","createdtime":"12/05/2019 11:02:42"}
,
{"schema":"ADM_SGV_PIN","name":"RECEPCAO_LOTE_PIN","comments":"Armazena informa��es da recep��o do lote_pin","lastddltime":"13/05/2019 11:42:21","createdtime":"12/05/2019 11:02:42"}
,
{"schema":"ADM_SGV_PIN","name":"RECEPCAO_LOTE_PIN_ARQUIVO","comments":"Armazena informa��es de recep��o do lote_pin_arquivo","lastddltime":"13/05/2019 11:42:22","createdtime":"12/05/2019 11:03:12"}
,
{"schema":"ADM_SGV_PIN","name":"RECEPCAO_LOTE_PIN_PROC","comments":"Armazena informa��es da recep��o dos lote_pin processados","lastddltime":"13/05/2019 11:42:22","createdtime":"12/05/2019 11:02:42"}
,
{"schema":"ADM_SGV_PIN","name":"RECEPCAO_LOTE_PIN_PROC_2705","comments":"","lastddltime":"12/05/2019 11:02:42","createdtime":"12/05/2019 11:02:42"}
,
{"schema":"ADM_SGV_PIN","name":"TIPO_BLOQUEIO","comments":"Entidade respons�vel por armazenar os tipos de bloqueios utilizados no sistema, para as diferentes entidades: 1-Empresa 2-Estabelecimento 3-PIN","lastddltime":"12/05/2019 12:54:09","createdtime":"12/05/2019 11:02:45"}
,
{"schema":"ADM_SGV_PIN","name":"TIPO_TRANSACAO","comments":"Identifica e categoriza as transa��es que podem trafegar no sistema Toda transa��o lan�ada no sistema deve possuir seu tipo definido nesta tabela. Os v�rios atributos de qualifica��o tem como objetivo poder reagrupar as diversas transa��es do sistema em novos subgrupos mais identific�veis com a realidade de cada elemento na organiza��o. Diversos tipos de transa��es diferentes (c�digos diferentes) podem possuir atributos iguais, por exemplo: IDTransacao Nome Transa��o CS01 Consulta saldo VV01 Venda no POS VV02 Venda no Back Office VV03 Venda na WEB A simples identifica��o visual j� permite que auditorias sejam imediatas, a contabilidade seja espec�fica e outros processos envolvidos possam tratar as diferen�as de forma adequada","lastddltime":"15/05/2019 10:59:38","createdtime":"12/05/2019 11:02:44"}
,
{"schema":"ADM_SGV_PIN","name":"TIPO_TRANSACAO_CURVA","comments":"","lastddltime":"13/05/2019 11:42:22","createdtime":"12/05/2019 11:02:41"}
,
{"schema":"ADM_SGV_PIN","name":"TRANSFERENCIA_PIN","comments":"Armazena o registro de transfer�ncia dos PINs entre as empresas","lastddltime":"12/05/2019 12:51:42","createdtime":"12/05/2019 11:02:44"}
,
{"schema":"ADM_SGV_PIN","name":"VALOR_NEGOCIADO","comments":"Valores de produtos dispon�veis para venda","lastddltime":"27/07/2019 05:00:40","createdtime":"12/05/2019 11:02:44"}
,
{"schema":"ADM_SGV_PRC","name":"DETALHE_PROCESSAMENTO","comments":"Entidade que cont�m informa��es dos detalhes dos processamentos que aconteceram","lastddltime":"13/05/2019 11:21:31","createdtime":"12/05/2019 10:26:30"}
,
{"schema":"ADM_SGV_PRC","name":"LOG_PROCESSAMENTO","comments":"Logs gerados pelos processamentos","lastddltime":"13/05/2019 11:21:31","createdtime":"12/05/2019 10:26:30"}
,
{"schema":"ADM_SGV_PRC","name":"PROCESSAMENTO","comments":"Entidade que cont�m informa��es dos processamentos que aconteceram","lastddltime":"12/05/2019 10:27:03","createdtime":"12/05/2019 10:26:31"}
,
{"schema":"ADM_SGV_SEX","name":"ADESAO_UPSELL","comments":"","lastddltime":"13/05/2019 10:55:21","createdtime":"11/05/2019 21:25:45"}
,
{"schema":"ADM_SGV_SEX","name":"CATEGORIA_VALOR_BONUS","comments":"Armazena informa��es da categoria do valor do B�nus por filial","lastddltime":"13/05/2019 10:55:20","createdtime":"11/05/2019 21:25:46"}
,
{"schema":"ADM_SGV_SEX","name":"CODIGO_RESPOSTA","comments":"Armazena informa��es do c�digo resposta das transa��es","lastddltime":"13/05/2019 10:55:20","createdtime":"11/05/2019 21:25:46"}
,
{"schema":"ADM_SGV_SEX","name":"CONCESSIONARIA","comments":"Cadastro das concessionarias dispon�veis para o sistema","lastddltime":"21/08/2019 21:01:54","createdtime":"11/05/2019 21:25:46"}
,
{"schema":"ADM_SGV_SEX","name":"DADOS_GERAIS","comments":"Armazena os dados gerais da filial","lastddltime":"13/05/2019 10:55:19","createdtime":"11/05/2019 21:25:46"}
,
{"schema":"ADM_SGV_SEX","name":"FAIXA_VALOR","comments":"Armazena informa��es de cadastro da faixa de valor usado na filial para recarga de celular","lastddltime":"13/05/2019 10:55:18","createdtime":"11/05/2019 21:25:46"}
,
{"schema":"ADM_SGV_SEX","name":"FILIAL","comments":"Armazena informa��es da filial","lastddltime":"26/09/2019 21:02:46","createdtime":"11/05/2019 21:25:46"}
,
{"schema":"ADM_SGV_SEX","name":"FILIAL_CODIGO_AREA","comments":"Cadastro dos relacionamentos de filiais com c�digos de �rea dispon�veis para o sistema","lastddltime":"13/05/2019 10:55:17","createdtime":"11/05/2019 21:25:46"}
,
{"schema":"ADM_SGV_SEX","name":"FILIAL_TI","comments":"Tabela criada para suportar a nova estrutura de par�metros","lastddltime":"13/05/2019 10:55:21","createdtime":"11/05/2019 21:25:46"}
,
{"schema":"ADM_SGV_SEX","name":"FORNECEDOR_SGV","comments":"Armazena informa��es do fornecedor no SGV","lastddltime":"13/05/2019 10:55:19","createdtime":"11/05/2019 21:25:46"}
,
{"schema":"ADM_SGV_SEX","name":"PARAMETRO","comments":"Armazena informa��es do cadastro de par�metros","lastddltime":"11/06/2019 21:03:05","createdtime":"11/05/2019 21:25:46"}
,
{"schema":"ADM_SGV_SEX","name":"PARAMETRO_CONCESSIONARIA","comments":"Armazena informa��es do par�metro das concession�rias","lastddltime":"13/05/2019 10:55:17","createdtime":"11/05/2019 21:25:46"}
,
{"schema":"ADM_SGV_SEX","name":"PARAMETRO_DADOS","comments":"Armazena informa��es do par�metro de dados","lastddltime":"13/05/2019 10:55:20","createdtime":"11/05/2019 21:25:46"}
,
{"schema":"ADM_SGV_SEX","name":"PRODUTO_PAGAMENTO","comments":"Armazenar produto ou documentos de pagamento de fatura na estrutura par�metro","lastddltime":"13/05/2019 10:55:20","createdtime":"11/05/2019 21:25:46"}
,
{"schema":"ADM_SGV_SEX","name":"VALOR_FIXO","comments":"Armazena informa��es de cadastro de valor_fixo usado nas filiais para recarga de celular","lastddltime":"08/10/2019 21:03:57","createdtime":"11/05/2019 21:25:46"}
,
{"schema":"ADM_SGV_SFA","name":"AGENDAMENTO_COLETA","comments":"Armazena informa��es do agendamento da coleta por n�mero de s�rie para empresa e operadora.","lastddltime":"14/06/2019 21:03:20","createdtime":"12/05/2019 11:03:06"}
,
{"schema":"ADM_SGV_SFA","name":"AGENDAMENTO_COLETA_PRODUTO","comments":"Armazena informa��es dos produtos a serem coletados por numero de s�rie.","lastddltime":"13/05/2019 11:41:37","createdtime":"12/05/2019 11:03:06"}
,
{"schema":"ADM_SGV_SFA","name":"AREA_CONS_OTIMIZA_ITEM_PROC","comments":"","lastddltime":"13/05/2019 11:41:24","createdtime":"12/05/2019 11:03:07"}
,
{"schema":"ADM_SGV_SFA","name":"AREA_CONS_OTIMIZA_PROC","comments":"","lastddltime":"13/05/2019 11:41:24","createdtime":"12/05/2019 11:03:07"}
,
{"schema":"ADM_SGV_SFA","name":"AREA_CONSULTOR_EXECUCAO_MASSA","comments":"","lastddltime":"12/05/2019 12:54:31","createdtime":"12/05/2019 11:03:07"}
,
{"schema":"ADM_SGV_SFA","name":"AREA_CONSULTOR_OTIMIZACAO","comments":"","lastddltime":"13/05/2019 11:41:22","createdtime":"12/05/2019 11:03:07"}
,
{"schema":"ADM_SGV_SFA","name":"AREA_CONSULTOR_OTIMIZACAO_ITEM","comments":"","lastddltime":"13/05/2019 11:41:23","createdtime":"12/05/2019 11:03:07"}
,
{"schema":"ADM_SGV_SFA","name":"AUD_ESTOQUE_FISICO","comments":"Armazenar o estoque do produto do usu�rio que foi auditado","lastddltime":"23/08/2019 21:01:10","createdtime":"12/05/2019 11:03:08"}
,
{"schema":"ADM_SGV_SFA","name":"AUD_ESTOQUE_FISICO_ITEM","comments":"Armazenar os itens do estoque f�sico auditado","lastddltime":"13/05/2019 11:41:22","createdtime":"12/05/2019 11:03:08"}
,
{"schema":"ADM_SGV_SFA","name":"AUD_ESTOQUE_FISICO_SERIAL","comments":"Entidade respons�vel por armazenar numero de serie e realizar a��es de auditoria.","lastddltime":"18/06/2019 21:01:58","createdtime":"12/05/2019 11:03:05"}
,
{"schema":"ADM_SGV_SFA","name":"BONIFICACAO","comments":"","lastddltime":"13/05/2019 11:41:36","createdtime":"12/05/2019 11:03:06"}
,
{"schema":"ADM_SGV_SFA","name":"BONIFICACAO_ITEM","comments":"","lastddltime":"13/05/2019 11:41:36","createdtime":"12/05/2019 11:03:06"}
,
{"schema":"ADM_SGV_SFA","name":"BONIFICACAO_ITEM_TRANSACAO","comments":"","lastddltime":"13/05/2019 11:41:38","createdtime":"12/05/2019 11:03:05"}
,
{"schema":"ADM_SGV_SFA","name":"CAMPANHA","comments":"Entidade para armazenar campanhas.","lastddltime":"10/12/2019 21:02:11","createdtime":"12/05/2019 11:03:05"}
,
{"schema":"ADM_SGV_SFA","name":"CAMPANHA_ITEM","comments":"Entidade para armazenar item das campanhas.","lastddltime":"13/05/2019 11:41:51","createdtime":"12/05/2019 11:03:04"}
,
{"schema":"ADM_SGV_SFA","name":"CAMPANHA_KIT","comments":"","lastddltime":"16/09/2019 13:18:18","createdtime":"16/09/2019 13:18:11"}
,
{"schema":"ADM_SGV_SFA","name":"CONFIRMACAO_CADASTRO","comments":"Armazenar as informa��es para confirma��o de cadastro.","lastddltime":"13/05/2019 11:41:20","createdtime":"12/05/2019 11:03:08"}
,
{"schema":"ADM_SGV_SFA","name":"CONFIRMACAO_CADASTRO_ESTAB","comments":"Entidade de relacionamento entre CONFIRMACAO_CADASTRO e ESTABELECIMENTO.","lastddltime":"13/05/2019 11:41:20","createdtime":"12/05/2019 11:03:08"}
,
{"schema":"ADM_SGV_SFA","name":"CONTROLE_AGENDAMENTO_TERMINAL","comments":"Armazena informa��es de controle do agendamento de terminal","lastddltime":"13/05/2019 11:41:17","createdtime":"12/05/2019 11:03:08"}
,
{"schema":"ADM_SGV_SFA","name":"CREDITO_OPERADORA_PDV","comments":"","lastddltime":"13/05/2019 11:41:25","createdtime":"12/05/2019 11:03:07"}
,
{"schema":"ADM_SGV_SFA","name":"CURVA","comments":"Armazenar as informa��es das curvas.","lastddltime":"20/11/2019 15:21:26","createdtime":"12/05/2019 11:03:08"}
,
{"schema":"ADM_SGV_SFA","name":"CURVA_EMPRESA","comments":"Entidade respons�vel por armazenar a curva cadastrada por fornecedor","lastddltime":"13/05/2019 11:41:30","createdtime":"12/05/2019 11:03:06"}
,
{"schema":"ADM_SGV_SFA","name":"CURVA_ESTABELECIMENTO_EMPRESA","comments":"Entidade respons�vel por armazenar o resultado da gera��o dos c�lculos de curva","lastddltime":"20/11/2019 15:21:26","createdtime":"12/05/2019 11:03:06"}
,
{"schema":"ADM_SGV_SFA","name":"CURVA_FAIXA","comments":"Armazenar as informa��es da curva faixa","lastddltime":"20/11/2019 15:21:26","createdtime":"12/05/2019 11:03:08"}
,
{"schema":"ADM_SGV_SFA","name":"CURVA_FORNECEDOR","comments":"Entidade respons�vel por armazenar a curva cadastrada por fornecedor","lastddltime":"13/05/2019 11:41:30","createdtime":"12/05/2019 11:03:06"}
,
{"schema":"ADM_SGV_SFA","name":"DOWNLOAD","comments":"Entidade para controle da sequ�ncia dos downloads.","lastddltime":"12/05/2019 12:54:27","createdtime":"12/05/2019 11:03:05"}
,
{"schema":"ADM_SGV_SFA","name":"DOWNLOAD_ITEM","comments":"Entidade para controle dos modulos para downloads.","lastddltime":"13/05/2019 11:41:42","createdtime":"12/05/2019 11:03:05"}
,
{"schema":"ADM_SGV_SFA","name":"EFI_RECEPCAO_ARQUIVO","comments":"Armazena informa��es de recep��o dos arquivos de Estoque F�sico Importa��o (Estoque f�sico importa��o) ","lastddltime":"12/05/2019 12:54:34","createdtime":"12/05/2019 11:03:13"}
,
{"schema":"ADM_SGV_SFA","name":"EFI_RECEPCAO_REGISTRO","comments":"Armazena informa��es de recep��o dos registros de Estoque F�sico Importa��o","lastddltime":"13/05/2019 11:41:10","createdtime":"12/05/2019 11:03:08"}
,
{"schema":"ADM_SGV_SFA","name":"EMPRESA_PARAMETRO","comments":"","lastddltime":"13/05/2019 11:41:33","createdtime":"12/05/2019 11:03:06"}
,
{"schema":"ADM_SGV_SFA","name":"ESTABELECIMENTO_INDICADOR","comments":"","lastddltime":"13/05/2019 11:41:31","createdtime":"12/05/2019 11:03:06"}
,
{"schema":"ADM_SGV_SFA","name":"ESTABELECIMENTO_INDICADOR_LDR","comments":"","lastddltime":"12/05/2019 11:17:01","createdtime":"12/05/2019 11:03:06"}
,
{"schema":"ADM_SGV_SFA","name":"ESTABELECIMENTO_RUPTURA","comments":"Armazena os estabelecimentos que est�o em ruptura. Represenata a vis�o atual do SIG Claro","lastddltime":"10/10/2019 15:45:33","createdtime":"10/10/2019 15:45:32"}
,
{"schema":"ADM_SGV_SFA","name":"ESTAB_LONGITUDE_LATITUDE","comments":"","lastddltime":"13/05/2019 11:41:18","createdtime":"12/05/2019 11:03:08"}
,
{"schema":"ADM_SGV_SFA","name":"ESTOQUE_FISICO","comments":"Armazena informa��es do estoque F�sico","lastddltime":"21/11/2019 09:41:26","createdtime":"12/05/2019 11:03:08"}
,
{"schema":"ADM_SGV_SFA","name":"FILA_ENVIO_EMAIL","comments":"Fila para notifica��o via email","lastddltime":"12/05/2019 12:54:28","createdtime":"12/05/2019 11:03:13"}
,
{"schema":"ADM_SGV_SFA","name":"FILA_MOVIMENTACAO_ESTOQUE","comments":"Entidade destinada a gegistrar as movimenta��es de estoque.","lastddltime":"13/09/2019 08:53:20","createdtime":"12/05/2019 11:02:26"}
,
{"schema":"ADM_SGV_SFA","name":"GOOGLE_MAPS","comments":"","lastddltime":"12/05/2019 12:54:32","createdtime":"12/05/2019 11:03:08"}
,
{"schema":"ADM_SGV_SFA","name":"HIST_ERRO_MOVIMENTO_FISICO","comments":"Armazena o hist�rico de erros de processamento durante a movimenta��o de estoque de produtos f�sicos. ","lastddltime":"12/05/2019 12:54:28","createdtime":"12/05/2019 11:03:06"}
,
{"schema":"ADM_SGV_SFA","name":"HISTORICO_COLETA","comments":"Armazena informa��es dos estabelecimento_empresa j� coletadas para determinado agendamento","lastddltime":"05/06/2019 21:00:36","createdtime":"12/05/2019 11:03:06"}
,
{"schema":"ADM_SGV_SFA","name":"HISTORICO_ERRO_RECEBIMENTO","comments":"Armazena informa��es do hist�rico de erro de recebimento","lastddltime":"12/05/2019 12:54:34","createdtime":"12/05/2019 11:03:08"}
,
{"schema":"ADM_SGV_SFA","name":"HISTORICO_ESTOQUE_FISICO","comments":"Armazena informa��es do hist�rico do estoque f�sico","lastddltime":"13/05/2019 11:41:17","createdtime":"12/05/2019 11:03:08"}
,
{"schema":"ADM_SGV_SFA","name":"INDICADOR","comments":"","lastddltime":"12/05/2019 12:54:28","createdtime":"12/05/2019 11:03:06"}
,
{"schema":"ADM_SGV_SFA","name":"INVASAO","comments":"Armazena registro de para evidenciar invas�o de �rea.","lastddltime":"13/05/2019 11:41:50","createdtime":"12/05/2019 11:03:05"}
,
{"schema":"ADM_SGV_SFA","name":"INVENTARIO","comments":"Armazena informa��es do invent�rio","lastddltime":"10/10/2019 09:30:23","createdtime":"12/05/2019 11:03:08"}
,
{"schema":"ADM_SGV_SFA","name":"INVENTARIO_ITEM","comments":"Armazena informa��es dos itens do inventario","lastddltime":"10/10/2019 09:30:23","createdtime":"12/05/2019 11:03:08"}
,
{"schema":"ADM_SGV_SFA","name":"INVENTARIO_SERIAL","comments":"Armazena informa��es dos seriais bipados na coleta de inventario.","lastddltime":"10/10/2019 09:30:23","createdtime":"12/05/2019 11:03:06"}
,
{"schema":"ADM_SGV_SFA","name":"KIT","comments":"Entidade respons�vel por armazenar o template do kit que ser� gerado.","lastddltime":"13/05/2019 11:41:43","createdtime":"12/05/2019 11:03:05"}
,
{"schema":"ADM_SGV_SFA","name":"KIT_ITEM","comments":"Entidade respons�vel por armazenar o template do KIT_ITEM que ser� gerado.","lastddltime":"13/05/2019 11:41:43","createdtime":"12/05/2019 11:03:05"}
,
{"schema":"ADM_SGV_SFA","name":"LOCAL_ESTOQUE","comments":"","lastddltime":"12/11/2019 21:03:15","createdtime":"12/05/2019 11:03:06"}
,
{"schema":"ADM_SGV_SFA","name":"LOCALIZACAO","comments":"Armazena informa��es de localiza��o","lastddltime":"13/05/2019 11:41:10","createdtime":"12/05/2019 11:03:08"}
,
{"schema":"ADM_SGV_SFA","name":"LOG_ERRO_NUMERO_SERIE","comments":"Tabela para registro dos logs de erros de processamento do N�mero de S�rie.","lastddltime":"12/05/2019 12:54:28","createdtime":"12/05/2019 11:03:06"}
,
{"schema":"ADM_SGV_SFA","name":"LOG_SMARTPHONE","comments":"Armazena informa��es de log do smartphone","lastddltime":"13/05/2019 11:41:18","createdtime":"12/05/2019 11:03:08"}
,
{"schema":"ADM_SGV_SFA","name":"LOTE_KIT","comments":"Entidade respons�vel por armazenar o kit que esta sendo gerado.","lastddltime":"15/05/2019 10:59:39","createdtime":"12/05/2019 11:03:05"}
,
{"schema":"ADM_SGV_SFA","name":"LOTE_KIT_ITEM","comments":"Entidade respons�vel por armazenar o template do LOTE_KIT_ITEM que ser� gerado.","lastddltime":"15/05/2019 10:59:39","createdtime":"12/05/2019 11:03:05"}
,
{"schema":"ADM_SGV_SFA","name":"LOTE_KIT_ITEM_SERIAL","comments":"Entidade respons�vel por armazenar o template do LOTE_KIT_ITEM_SERIAL que ser� gerado.","lastddltime":"13/05/2019 11:41:45","createdtime":"12/05/2019 11:03:05"}
,
{"schema":"ADM_SGV_SFA","name":"LOTE_OTIMIZACAO_AREA","comments":"Armazena os processamentos de otimiza��o das �reas por empresa","lastddltime":"13/05/2019 11:41:22","createdtime":"12/05/2019 11:03:08"}
,
{"schema":"ADM_SGV_SFA","name":"LOTE_OTIMIZACAO_AREA_ITEM","comments":"","lastddltime":"13/05/2019 11:41:36","createdtime":"12/05/2019 11:03:06"}
,
{"schema":"ADM_SGV_SFA","name":"LOTE_OTIMIZACAO_AREA_JSON","comments":"Armazena os processamentos de otimiza��o das �reas por empresa","lastddltime":"13/05/2019 11:41:23","createdtime":"12/05/2019 11:03:07"}
,
{"schema":"ADM_SGV_SFA","name":"MAPEAMENTO_CODIGO_PARCEIRO","comments":"","lastddltime":"13/05/2019 11:41:24","createdtime":"12/05/2019 11:03:07"}
,
{"schema":"ADM_SGV_SFA","name":"MATRIZ_DISTANCIA_PREVIA","comments":"","lastddltime":"12/05/2019 12:54:31","createdtime":"12/05/2019 11:03:06"}
,
{"schema":"ADM_SGV_SFA","name":"MC_LOCALIZACAO_RASTREIO","comments":"Descri��o da tabela.","lastddltime":"13/05/2019 11:41:48","createdtime":"12/05/2019 11:03:05"}
,
{"schema":"ADM_SGV_SFA","name":"MC_QUESTIONARIO_RESPOSTA","comments":"","lastddltime":"13/05/2019 11:41:28","createdtime":"12/05/2019 11:03:07"}
,
{"schema":"ADM_SGV_SFA","name":"MC_ROTA_PRODUTO_VENDA","comments":"Armazena informa��es da rota do produto_venda","lastddltime":"10/12/2019 14:38:09","createdtime":"12/05/2019 11:03:08"}
,
{"schema":"ADM_SGV_SFA","name":"MOTIVO","comments":"Armazena informa��es do motivo","lastddltime":"12/05/2019 12:54:34","createdtime":"12/05/2019 11:03:08"}
,
{"schema":"ADM_SGV_SFA","name":"MOTIVO_FORNECEDOR","comments":"Registrar os motivos de ruptura para fornecedor/empresa.","lastddltime":"13/05/2019 11:41:41","createdtime":"12/05/2019 11:03:05"}
,
{"schema":"ADM_SGV_SFA","name":"MOVIMENTO_FISICO","comments":"Armazena informa��es da movimenta��o do estoque F�sico","lastddltime":"26/09/2019 21:00:53","createdtime":"12/05/2019 11:03:08"}
,
{"schema":"ADM_SGV_SFA","name":"MOVIMENTO_FISICO_DIGITACAO","comments":"","lastddltime":"23/09/2019 21:01:12","createdtime":"12/05/2019 11:03:06"}
,
{"schema":"ADM_SGV_SFA","name":"MOVIMENTO_FISICO_ITEM","comments":"Armazena informa��es da movimenta��o dos itens do estoque f�sico","lastddltime":"15/05/2019 10:59:39","createdtime":"12/05/2019 11:03:08"}
,
{"schema":"ADM_SGV_SFA","name":"MOVIMENTO_FISICO_PRODUTO","comments":"","lastddltime":"13/05/2019 11:41:35","createdtime":"12/05/2019 11:03:06"}
,
{"schema":"ADM_SGV_SFA","name":"NOTA_FISCAL","comments":"","lastddltime":"16/05/2019 12:09:26","createdtime":"12/05/2019 11:03:06"}
,
{"schema":"ADM_SGV_SFA","name":"NOTA_FISCAL_INTEGRACAO_LDR","comments":"","lastddltime":"12/05/2019 11:17:01","createdtime":"12/05/2019 11:03:04"}
,
{"schema":"ADM_SGV_SFA","name":"NOTA_FISCAL_ITEM","comments":"","lastddltime":"13/05/2019 11:41:34","createdtime":"12/05/2019 11:03:06"}
,
{"schema":"ADM_SGV_SFA","name":"NOTA_FISCAL_PRODUTO","comments":"","lastddltime":"28/05/2019 21:00:53","createdtime":"12/05/2019 11:03:06"}
,
{"schema":"ADM_SGV_SFA","name":"OPI_RECEPCAO_ARQUIVO","comments":"","lastddltime":"13/05/2019 11:41:26","createdtime":"12/05/2019 11:03:13"}
,
{"schema":"ADM_SGV_SFA","name":"OPI_RECEPCAO_REGISTRO","comments":"","lastddltime":"13/05/2019 11:41:26","createdtime":"12/05/2019 11:03:07"}
,
{"schema":"ADM_SGV_SFA","name":"PEDIDO","comments":"Pedidos de venda ou compra.","lastddltime":"21/11/2019 14:30:47","createdtime":"12/05/2019 11:03:05"}
,
{"schema":"ADM_SGV_SFA","name":"PEDIDO_DIGITACAO","comments":"Descri��o da tabela.","lastddltime":"13/05/2019 11:41:47","createdtime":"12/05/2019 11:03:05"}
,
{"schema":"ADM_SGV_SFA","name":"PEDIDO_ITEM","comments":"Descri��o da tabela.","lastddltime":"13/05/2019 11:41:47","createdtime":"12/05/2019 11:03:05"}
,
{"schema":"ADM_SGV_SFA","name":"PEDIDO_STATUS","comments":"Tabela com situa��es e status validos para pedidos.","lastddltime":"05/12/2019 13:57:55","createdtime":"12/05/2019 11:03:05"}
,
{"schema":"ADM_SGV_SFA","name":"PEDIDO_VENDA_COBRANCA","comments":"","lastddltime":"21/11/2019 09:08:25","createdtime":"21/11/2019 09:08:24"}
,
{"schema":"ADM_SGV_SFA","name":"PERMISSAO_MOBILE","comments":"Entidade para manter as premiss�es que ser� enviada ao mobile.","lastddltime":"22/11/2019 08:54:16","createdtime":"12/05/2019 11:03:05"}
,
{"schema":"ADM_SGV_SFA","name":"PESQUISA_FORNECEDOR_EMPRESA","comments":"","lastddltime":"13/05/2019 11:41:19","createdtime":"12/05/2019 11:03:08"}
,
{"schema":"ADM_SGV_SFA","name":"PESQUISA_FORN_EMP_TIPO","comments":"Registrar quais pesquisas devera ser realizada para cada fornecedor.","lastddltime":"13/05/2019 11:41:39","createdtime":"12/05/2019 11:03:05"}
,
{"schema":"ADM_SGV_SFA","name":"PESQUISA_MERCHAN","comments":"Registrar as capturas de merchan para os estabelecimentos.","lastddltime":"13/05/2019 11:41:40","createdtime":"12/05/2019 11:03:05"}
,
{"schema":"ADM_SGV_SFA","name":"PESQUISA_QUALIDADE","comments":"Armazena informa��es de pesquisa de qualidade","lastddltime":"15/05/2019 10:59:39","createdtime":"12/05/2019 11:03:08"}
,
{"schema":"ADM_SGV_SFA","name":"PESQUISA_TIPO","comments":"Registrar os tipos de pesquisa para fornecedor/empresa.","lastddltime":"12/05/2019 12:54:28","createdtime":"12/05/2019 11:03:05"}
,
{"schema":"ADM_SGV_SFA","name":"POSICAO_ESTOQUE_MOBILE","comments":"","lastddltime":"13/05/2019 11:41:21","createdtime":"12/05/2019 11:03:08"}
,
{"schema":"ADM_SGV_SFA","name":"PROCESSAMENTO_MC_ROTA","comments":"Entidade respons�vel por armazenar o status do processamento de feeding da MC_ROTA_PRODUTO_VENDA","lastddltime":"12/05/2019 12:54:31","createdtime":"12/05/2019 11:03:06"}
,
{"schema":"ADM_SGV_SFA","name":"PRODUTO_CONSIGNADO","comments":"Entidade respons�vel por armazenar os itens consignados.","lastddltime":"24/10/2019 21:02:30","createdtime":"12/05/2019 11:03:06"}
,
{"schema":"ADM_SGV_SFA","name":"PRODUTO_VENDA_CONSIGNADO","comments":"Entidade respons�vel por armazenar o produto e a quantidade consignado.","lastddltime":"24/06/2019 21:00:38","createdtime":"12/05/2019 11:03:06"}
,
{"schema":"ADM_SGV_SFA","name":"QUESTIONARIO","comments":"Armazenar as informa��es do question�rio","lastddltime":"13/05/2019 11:41:27","createdtime":"12/05/2019 11:03:07"}
,
{"schema":"ADM_SGV_SFA","name":"QUESTIONARIO_PERGUNTA","comments":"","lastddltime":"13/05/2019 11:41:27","createdtime":"12/05/2019 11:03:07"}
,
{"schema":"ADM_SGV_SFA","name":"QUESTIONARIO_PERGUNTA_ITEM","comments":"","lastddltime":"13/05/2019 11:41:27","createdtime":"12/05/2019 11:03:07"}
,
{"schema":"ADM_SGV_SFA","name":"QUESTIONARIO_RESPOSTA","comments":"","lastddltime":"13/05/2019 11:41:28","createdtime":"12/05/2019 11:03:07"}
,
{"schema":"ADM_SGV_SFA","name":"RECEBIMENTO","comments":"Armazena informa��o do pedidos realizados e recebidos pelo JDE","lastddltime":"12/05/2019 12:54:34","createdtime":"12/05/2019 11:03:08"}
,
{"schema":"ADM_SGV_SFA","name":"RECEBIMENTO_LDR","comments":"","lastddltime":"12/05/2019 11:17:02","createdtime":"12/05/2019 11:03:08"}
,
{"schema":"ADM_SGV_SFA","name":"RECEPCAO_CARGA","comments":"Armazena informa��es de recep��o das cargas","lastddltime":"13/05/2019 11:41:12","createdtime":"12/05/2019 11:03:08"}
,
{"schema":"ADM_SGV_SFA","name":"RECEPCAO_CARGA_ITEM","comments":"Armazena informa��o dos itens da carga do estoque f�sico","lastddltime":"13/05/2019 11:41:12","createdtime":"12/05/2019 11:03:08"}
,
{"schema":"ADM_SGV_SFA","name":"RECEPCAO_ESTOQUE_FISICO","comments":"Armazena informa��o de recep��o do estoque f�sico","lastddltime":"13/05/2019 11:41:09","createdtime":"12/05/2019 11:03:08"}
,
{"schema":"ADM_SGV_SFA","name":"ROTA_ITEM_PREVIA","comments":"","lastddltime":"13/05/2019 11:41:29","createdtime":"12/05/2019 11:03:06"}
,
{"schema":"ADM_SGV_SFA","name":"ROTA_ORIGEM_DESTINO_CONFIG","comments":"","lastddltime":"13/05/2019 11:41:29","createdtime":"12/05/2019 11:03:06"}
,
{"schema":"ADM_SGV_SFA","name":"ROTA_PREVIA","comments":"","lastddltime":"13/05/2019 11:41:29","createdtime":"12/05/2019 11:03:06"}
,
{"schema":"ADM_SGV_SFA","name":"RUPTURA","comments":"Registrar as rupturas identificadas para os estabelecimentos.","lastddltime":"13/05/2019 11:41:42","createdtime":"12/05/2019 11:03:05"}
,
{"schema":"ADM_SGV_SFA","name":"SFA_CONTROLE","comments":"Armazena informa��es de controle do SFA","lastddltime":"12/05/2019 12:54:34","createdtime":"12/05/2019 11:03:08"}
,
{"schema":"ADM_SGV_SFA","name":"TIPO_PEDIDO","comments":"Descri��o da tabela.","lastddltime":"20/11/2019 17:06:11","createdtime":"12/05/2019 11:03:05"}
,
{"schema":"ADM_SGV_SFA","name":"TMP_ESTOQUE_FISICO","comments":"Entidade temporaria para auxilio na AUD_ESTOQUE_FISICO.","lastddltime":"12/05/2019 11:17:01","createdtime":"12/05/2019 11:03:13"}
,
{"schema":"ADM_SGV_SFA","name":"TRANSFERENCIA","comments":"Entidade para fazer a liga��o entre o Movimento_Fisico de Sa�da e de Entrada da transfer�ncia.","lastddltime":"20/09/2019 21:00:51","createdtime":"12/05/2019 11:03:05"}
,
{"schema":"ADM_SGV_SFA","name":"TROCA_NUMERO_SERIE","comments":"","lastddltime":"13/05/2019 11:41:33","createdtime":"12/05/2019 11:03:06"}
,
{"schema":"ADM_SGV_SFA","name":"UNIDADE_MEDIDA","comments":"Descri��o da tabela.","lastddltime":"12/05/2019 12:54:27","createdtime":"12/05/2019 11:03:05"}
,
{"schema":"ADM_SGV_SFA","name":"USUARIO_LOCALIZACAO","comments":"Entidade para fazer rastreio usu�rio.","lastddltime":"20/05/2019 08:01:28","createdtime":"20/05/2019 08:01:27"}
,
{"schema":"ADM_SGV_SFA","name":"USUARIO_PARAMETRO","comments":"","lastddltime":"13/05/2019 11:41:33","createdtime":"12/05/2019 11:03:06"}
,
{"schema":"ADM_SGV_SFA","name":"VENDA","comments":"Armazena informa��es das Vendas","lastddltime":"27/11/2019 21:02:27","createdtime":"12/05/2019 11:03:08"}
,
{"schema":"ADM_SGV_SFA","name":"VENDA_ITEM","comments":"Armazena informa��es dos itens de venda","lastddltime":"06/12/2019 21:02:18","createdtime":"12/05/2019 11:03:08"}
,
{"schema":"ADM_SGV_SFA","name":"VENDA_ITEM_DEVOLUCAO","comments":"Entidade para realizar devolu��es do mudulo SFA.","lastddltime":"15/05/2019 10:59:39","createdtime":"12/05/2019 11:03:05"}
,
{"schema":"ADM_SGV_SFA","name":"VENDA_SERIAL","comments":"Entidade para armazenar os n�meros de s�ries dos itens vendidos.","lastddltime":"24/07/2019 11:04:40","createdtime":"12/05/2019 11:03:05"}
,
{"schema":"ADM_SGV_TMP","name":"IMP_BOLETOS_CONTAS","comments":"","lastddltime":"12/05/2019 10:27:03","createdtime":"12/05/2019 10:26:32"}
,
{"schema":"ADM_SGV_TMP","name":"IMP_CLIENTES","comments":"","lastddltime":"13/05/2019 11:21:34","createdtime":"12/05/2019 10:26:32"}
,
{"schema":"ADM_SGV_TMP","name":"IMP_CLIENTES_BOLETOS_CONTAS","comments":"","lastddltime":"13/05/2019 11:21:35","createdtime":"12/05/2019 10:26:32"}
,
{"schema":"ADM_SGV_TMP","name":"IMP_CLIENTES_CANAL_VENDA","comments":"","lastddltime":"13/05/2019 11:21:36","createdtime":"12/05/2019 10:26:32"}
,
{"schema":"ADM_SGV_TMP","name":"IMP_CLIENTES_COBRANCA_AGENDA","comments":"","lastddltime":"12/05/2019 10:27:03","createdtime":"12/05/2019 10:26:32"}
,
{"schema":"ADM_SGV_TMP","name":"IMP_CLIENTES_COBRANCA_SEMANAL","comments":"","lastddltime":"13/05/2019 11:21:35","createdtime":"12/05/2019 10:26:32"}
,
{"schema":"ADM_SGV_TMP","name":"IMP_CLIENTES_FISICO_SGV","comments":"","lastddltime":"25/09/2019 10:26:00","createdtime":"12/05/2019 10:26:31"}
,
{"schema":"ADM_SGV_TMP","name":"IMP_CLIENTES_REDETREL","comments":"","lastddltime":"06/06/2019 13:43:13","createdtime":"05/06/2019 17:06:26"}
,
{"schema":"ADM_SGV_TMP","name":"IMP_CLIENTES_SGV","comments":"","lastddltime":"12/05/2019 10:27:03","createdtime":"12/05/2019 10:26:32"}
,
{"schema":"ADM_SGV_TMP","name":"IMP_CLIENTES_VALIDACAO","comments":"","lastddltime":"13/05/2019 11:21:35","createdtime":"12/05/2019 10:26:32"}
,
{"schema":"ADM_SGV_TMP","name":"IMP_DP_ATIVIDADE","comments":"","lastddltime":"12/05/2019 10:27:03","createdtime":"12/05/2019 10:26:32"}
,
{"schema":"ADM_SGV_TMP","name":"IMP_DP_BOLETOS_CONTAS","comments":"","lastddltime":"12/05/2019 10:27:03","createdtime":"12/05/2019 10:26:32"}
,
{"schema":"ADM_SGV_TMP","name":"IMP_DP_CIDADES","comments":"","lastddltime":"12/05/2019 10:27:03","createdtime":"12/05/2019 10:26:32"}
,
{"schema":"ADM_SGV_TMP","name":"IMP_DP_CLIENTES","comments":"","lastddltime":"12/05/2019 10:27:03","createdtime":"12/05/2019 10:26:32"}
,
{"schema":"ADM_SGV_TMP","name":"IMP_DP_CONSULTOR","comments":"","lastddltime":"12/05/2019 10:27:03","createdtime":"12/05/2019 10:26:32"}
,
{"schema":"ADM_SGV_TMP","name":"IMP_DP_EMPRESA","comments":"","lastddltime":"12/05/2019 10:27:03","createdtime":"12/05/2019 10:26:32"}
,
{"schema":"ADM_SGV_TMP","name":"IMP_DP_PRODUTOS","comments":"","lastddltime":"12/05/2019 10:27:03","createdtime":"12/05/2019 10:26:32"}
,
{"schema":"ADM_SGV_TMP","name":"IMP_EMPRESA","comments":"","lastddltime":"12/05/2019 10:27:03","createdtime":"12/05/2019 10:26:33"}
,
{"schema":"ADM_SGV_TMP","name":"IMP_EXECUCAO","comments":"","lastddltime":"12/05/2019 10:27:03","createdtime":"12/05/2019 10:26:32"}
,
{"schema":"ADM_SGV_TMP","name":"IMP_OPERADORES","comments":"","lastddltime":"13/05/2019 11:21:35","createdtime":"12/05/2019 10:26:32"}
,
{"schema":"ADM_SGV_TMP","name":"IMPORTACAO_REDETREL","comments":"","lastddltime":"17/07/2019 14:30:05","createdtime":"11/06/2019 16:51:25"}
,
{"schema":"ADM_SGV_TMP","name":"IMP_POS","comments":"","lastddltime":"13/05/2019 11:21:36","createdtime":"12/05/2019 10:26:32"}
,
{"schema":"ADM_SGV_TMP","name":"IMP_PRODUTOS_CLIENTES_VENDAS","comments":"","lastddltime":"13/05/2019 11:21:36","createdtime":"12/05/2019 10:26:32"}
,
{"schema":"ADM_SGV_TMP","name":"IMP_RECARGAONLINE_INTEGRADO","comments":"","lastddltime":"13/05/2019 11:21:35","createdtime":"12/05/2019 10:26:32"}
,
{"schema":"ADM_SGV_TMP","name":"IMP_REDETREL","comments":"","lastddltime":"11/06/2019 08:36:04","createdtime":"10/06/2019 17:34:56"}
,
{"schema":"ADM_SGV_TMP","name":"NUMERO_PANTANAL_CAP","comments":"","lastddltime":"22/05/2019 09:49:19","createdtime":"22/05/2019 09:02:41"}
,
{"schema":"ADM_SGV_TMP","name":"NUMERO_PANTANAL_CAP_FULL","comments":"","lastddltime":"22/05/2019 08:50:07","createdtime":"22/05/2019 08:50:07"}
,
{"schema":"ADM_SGV_TMP","name":"PERIODICIDADE_REDETREL","comments":"","lastddltime":"18/06/2019 15:45:44","createdtime":"17/06/2019 14:06:29"}
,
{"schema":"ADM_SGV_TMP","name":"PRAZO_REDETREL","comments":"","lastddltime":"18/06/2019 15:45:47","createdtime":"17/06/2019 14:07:09"}
,
{"schema":"ADM_SGV_TMP","name":"RAMO_ATIVIDADE_REDETREL","comments":"","lastddltime":"05/06/2019 18:30:45","createdtime":"05/06/2019 18:30:41"}
,
{"schema":"ADM_SGV_TMP","name":"REDES_REDETREL","comments":"","lastddltime":"17/06/2019 10:57:45","createdtime":"17/06/2019 10:57:45"}
,
{"schema":"ADM_SGV_TMP","name":"REDE_TREL_FINAL","comments":"","lastddltime":"11/06/2019 16:36:39","createdtime":"11/06/2019 16:36:39"}
,
{"schema":"ADM_SGV_TMP","name":"TESTE_SALDO","comments":"","lastddltime":"20/09/2019 16:28:39","createdtime":"20/09/2019 16:28:15"}
,
{"schema":"ADM_SGV_TMP","name":"TRANSACAO_CICLO_COBRANCA_F01","comments":"","lastddltime":"12/05/2019 10:26:48","createdtime":"12/05/2019 10:26:33"}
,
{"schema":"ADM_SGV_TRN","name":"CHIP_REGISTRO_VENDA","comments":"Registro de Vendas de CHIPs","lastddltime":"13/05/2019 11:21:27","createdtime":"12/05/2019 10:26:25"}
,
{"schema":"ADM_SGV_TRN","name":"CHIP_VENDA","comments":"Armazena informa��es dos CHIPs que est�o � venda","lastddltime":"12/05/2019 10:27:01","createdtime":"12/05/2019 10:26:25"}
,
{"schema":"ADM_SGV_TRN","name":"HISTORICO_ERRO_REC_EMBARCADA","comments":"Entidade respons�vel por armazenar informa��es de erro da recarga embarcada","lastddltime":"13/05/2019 11:21:28","createdtime":"12/05/2019 10:26:24"}
,
{"schema":"ADM_SGV_TRN","name":"HISTORICO_PROC_PROTOCOLOS","comments":"Tabela de hist�rico de execu��o da Procedure PCD_MS_INJETOR_PROC_PROTOCOLOS.","lastddltime":"12/05/2019 10:27:01","createdtime":"12/05/2019 10:26:36"}
,
{"schema":"ADM_SGV_TRN","name":"INDICE_VENDAS_APLIC","comments":"","lastddltime":"12/05/2019 10:27:01","createdtime":"12/05/2019 10:26:23"}
,
{"schema":"ADM_SGV_TRN","name":"LIMITE_SALDO_TRN","comments":"Esta tabela ficar� respons�vel por salvar as altera��es de saldo provindas da tabela TRASNACAO.","lastddltime":"12/05/2019 10:27:01","createdtime":"12/05/2019 10:25:15"}
,
{"schema":"ADM_SGV_TRN","name":"LOG_DESFAZIMENTO","comments":"Armazena informa��es de desfazimento (Desfazer)","lastddltime":"12/05/2019 10:26:40","createdtime":"12/05/2019 10:26:26"}
,
{"schema":"ADM_SGV_TRN","name":"LOG_INJETOR_ALGAR","comments":"","lastddltime":"08/11/2019 11:17:00","createdtime":"08/11/2019 11:17:00"}
,
{"schema":"ADM_SGV_TRN","name":"LOG_TRANSACAO","comments":"Armazena o log das transa��es","lastddltime":"12/05/2019 10:26:46","createdtime":"12/05/2019 10:26:26"}
,
{"schema":"ADM_SGV_TRN","name":"LOTE_ENVIO_TRANSACAO","comments":"Tabela de envio transa��o parceiros.","lastddltime":"12/05/2019 10:27:01","createdtime":"12/05/2019 10:26:24"}
,
{"schema":"ADM_SGV_TRN","name":"LOTE_ENVIO_TRANSACAO_ITEM","comments":"Descri��o da tabela.","lastddltime":"13/05/2019 11:21:29","createdtime":"12/05/2019 10:26:23"}
,
{"schema":"ADM_SGV_TRN","name":"LOTE_ENVIO_TRANS_HIST","comments":"Tabela de hist�rico do processamento de lotes envio transacao.","lastddltime":"13/05/2019 11:21:29","createdtime":"12/05/2019 10:26:23"}
,
{"schema":"ADM_SGV_TRN","name":"LOTE_INJETOR","comments":"Descri��o da tabela.","lastddltime":"12/05/2019 10:26:47","createdtime":"12/05/2019 10:26:24"}
,
{"schema":"ADM_SGV_TRN","name":"MC_MAX_DATA_CHIP","comments":"Armazena informa��o da ultima transa��o do CHIP","lastddltime":"12/05/2019 10:27:01","createdtime":"12/05/2019 10:26:24"}
,
{"schema":"ADM_SGV_TRN","name":"MC_MAX_DATA_CHIP_VDA","comments":"Armazena informa��o da ultima venda ","lastddltime":"12/05/2019 10:27:01","createdtime":"12/05/2019 10:26:24"}
,
{"schema":"ADM_SGV_TRN","name":"MC_MAX_DATA_ESTAB","comments":"Armazena informa��o da ultima transa��o do estabelecimento","lastddltime":"12/05/2019 10:27:02","createdtime":"12/05/2019 10:26:26"}
,
{"schema":"ADM_SGV_TRN","name":"MC_MAX_DATA_ESTAB_VDA","comments":"Armazena informa��es da ultima venda do estabelecimento","lastddltime":"12/05/2019 10:27:02","createdtime":"12/05/2019 10:26:25"}
,
{"schema":"ADM_SGV_TRN","name":"MC_MAX_DATA_ESTAB_VDA_FRN","comments":"","lastddltime":"15/05/2019 10:59:39","createdtime":"12/05/2019 10:26:24"}
,
{"schema":"ADM_SGV_TRN","name":"MC_MAX_DATA_TERM","comments":"Armazena informa��es da ultima transa��o do terminal","lastddltime":"12/05/2019 10:27:02","createdtime":"12/05/2019 10:26:25"}
,
{"schema":"ADM_SGV_TRN","name":"MC_MAX_DATA_TERM_VDA","comments":"Armazena informa��es da ultima venda do terminal","lastddltime":"12/05/2019 10:27:02","createdtime":"12/05/2019 10:26:25"}
,
{"schema":"ADM_SGV_TRN","name":"MC_ULTIMA_LOCALIZACAO_CHIP","comments":"Armazena informa��o da ultima transa��o do CHIP","lastddltime":"12/05/2019 10:26:47","createdtime":"12/05/2019 10:26:24"}
,
{"schema":"ADM_SGV_TRN","name":"MENSAGEM_ERRO_VALORACAO","comments":"Armazena informa��es de erro de valora��o","lastddltime":"12/05/2019 10:26:40","createdtime":"12/05/2019 10:26:26"}
,
{"schema":"ADM_SGV_TRN","name":"NUMERO_SERIE_CONTROLE","comments":"tabela de controle numero de serie","lastddltime":"12/05/2019 10:27:01","createdtime":"12/05/2019 10:26:23"}
,
{"schema":"ADM_SGV_TRN","name":"PROPOSTA_PARCEIRO","comments":"","lastddltime":"12/05/2019 10:27:01","createdtime":"12/05/2019 10:26:24"}
,
{"schema":"ADM_SGV_TRN","name":"PROTOCOLO_INJETOR","comments":"Descri��o da tabela.","lastddltime":"12/05/2019 10:26:47","createdtime":"12/05/2019 10:26:24"}
,
{"schema":"ADM_SGV_TRN","name":"RECARGA_EMBARCADA","comments":"","lastddltime":"13/05/2019 11:21:28","createdtime":"12/05/2019 10:26:24"}
,
{"schema":"ADM_SGV_TRN","name":"RECARGA_EMBARCADA_ARQ","comments":"","lastddltime":"13/05/2019 11:21:28","createdtime":"12/05/2019 10:26:36"}
,
{"schema":"ADM_SGV_TRN","name":"RECARGA_EMBARCADA_ITEM","comments":"","lastddltime":"13/05/2019 11:21:28","createdtime":"12/05/2019 10:26:24"}
,
{"schema":"ADM_SGV_TRN","name":"RELATORIO_VENDAS_SUMARIZADO","comments":"Armazena informa��es do relatorio de venda sumarizada","lastddltime":"12/05/2019 10:27:01","createdtime":"12/05/2019 10:26:24"}
,
{"schema":"ADM_SGV_TRN","name":"SCV_MC_MAX_DATA_CHIP","comments":"Armazena informa��es da ultima transa��o do SCV","lastddltime":"12/05/2019 10:26:46","createdtime":"12/05/2019 10:26:24"}
,
{"schema":"ADM_SGV_TRN","name":"SCV_MC_MAX_DATA_TERM","comments":"Armazena informa��o da ultima data do terminal no SCV","lastddltime":"12/05/2019 10:26:46","createdtime":"12/05/2019 10:26:24"}
,
{"schema":"ADM_SGV_TRN","name":"TRANSACAO","comments":"Armazena informa��es das Transa��es","lastddltime":"18/10/2019 10:07:31","createdtime":"12/05/2019 10:40:29"}
,
{"schema":"ADM_SGV_TRN","name":"TRANSACAO_DADOS_COMPLEMENTARES","comments":"","lastddltime":"12/05/2019 10:27:01","createdtime":"12/05/2019 10:26:24"}
,
{"schema":"ADM_SGV_TRN","name":"TRANSACAO_SUPORTE","comments":"Descri��o da tabela.","lastddltime":"12/05/2019 10:26:52","createdtime":"12/05/2019 10:25:43"}
,
{"schema":"ADM_SGV_TRN","name":"TRANSACAO_UUID","comments":"","lastddltime":"22/10/2019 09:17:53","createdtime":"12/05/2019 10:25:38"}
,
{"schema":"ADM_SGV_TRN","name":"TRANSACAO_VALORADA","comments":"Entidade respons�vel por armazenar informa��es das Transa��es que foram valoradas.","lastddltime":"18/10/2019 10:07:32","createdtime":"12/05/2019 10:24:29"}
,
{"schema":"ADM_SGV_TRN","name":"TRANSACAO_VALORADA_DESTINOS","comments":"Armazena informa��o de destino da transa��o valorada","lastddltime":"12/05/2019 10:27:01","createdtime":"12/05/2019 10:26:25"}
,
{"schema":"ADM_SGV_TRN","name":"TRANSACAO_VALORADA_SUMARIZADA","comments":"Entidade respons�vel por armazenar as transa��es valoradas sumarizadas.","lastddltime":"15/05/2019 10:16:28","createdtime":"12/05/2019 10:52:54"}
,
{"schema":"ADM_SGV_TRN","name":"VENDA_APLIC","comments":"","lastddltime":"12/05/2019 10:27:01","createdtime":"12/05/2019 10:26:23"}
,
{"schema":"ADM_SGV_TRN","name":"VENDA_CAP","comments":"","lastddltime":"05/08/2019 09:17:03","createdtime":"12/05/2019 10:26:23"}
,
{"schema":"ADM_SGV_TRN","name":"WS_COMPROVANTE_PIN","comments":"Respons�vel por armazenar os comprovantes que pussuam PINs para serem recuperados somente na confirma��o da venda.","lastddltime":"03/10/2019 10:22:34","createdtime":"13/09/2019 14:10:07"}
,
{"schema":"ADM_SGV_WS","name":"WS","comments":"Tabela para o cadastro de WebServices","lastddltime":"11/05/2019 23:32:28","createdtime":"11/05/2019 21:25:51"}
,
{"schema":"ADM_SGV_WS","name":"WS_CONTROLE_ACESSO","comments":"Tabela para o cadastro de Tags do envelope de entrada","lastddltime":"21/05/2019 10:21:43","createdtime":"11/05/2019 21:25:51"}
,
{"schema":"ADM_SGV_WS","name":"WS_DE_PARA","comments":"Descri��o da tabela.","lastddltime":"11/05/2019 22:25:38","createdtime":"11/05/2019 21:25:51"}
,
{"schema":"ADM_SGV_WS","name":"WS_OPERACAO","comments":"Tabela para o cadastro de opera��es(Actions/A��es) de um WebService","lastddltime":"13/05/2019 10:55:15","createdtime":"11/05/2019 21:25:51"}
,
{"schema":"ADM_SGV_WS","name":"WS_OPERACAO_ENTRADA","comments":"Tabela para o cadastro de Tags do envelope de entrada","lastddltime":"21/05/2019 10:21:44","createdtime":"11/05/2019 21:25:51"}
,
{"schema":"ADM_SGV_WS","name":"WS_REPASSE","comments":"Tabela que cont�m as permiss�es de repasse entre as empresas.;","lastddltime":"13/05/2019 10:55:16","createdtime":"11/05/2019 21:25:51"}
,
{"schema":"ADM_SGV_WS","name":"WS_USUARIO","comments":"Tabela com a identifica��o do Usu�rio no Active Directory;","lastddltime":"11/05/2019 23:31:37","createdtime":"11/05/2019 21:25:51"}
,
{"schema":"ADM_SGV_WS","name":"WS_USUARIO_EMPRESA","comments":"Tabela de liga��o entre o usu�rio do webservice e as empresas;","lastddltime":"13/05/2019 10:55:15","createdtime":"11/05/2019 21:25:51"}
,
{"schema":"ADM_SIG_PRC","name":"INT_PDV_CONTROLE_ENVIO","comments":"","lastddltime":"12/05/2019 12:54:21","createdtime":"12/05/2019 11:02:59"}
,
{"schema":"ADM_SIG_PRC","name":"PARAMETRO_AGENDADOR_TAREFA","comments":"","lastddltime":"15/05/2019 10:59:39","createdtime":"12/05/2019 11:03:12"}
,
{"schema":"ADM_SIG_PRC","name":"SIG_ALMOXARIFADO_DISP","comments":"","lastddltime":"15/05/2019 10:59:39","createdtime":"12/05/2019 11:02:59"}
,
{"schema":"ADM_SIG_PRC","name":"SIG_ALMOXARIFADOS","comments":"","lastddltime":"15/05/2019 10:59:39","createdtime":"12/05/2019 11:01:15"}
,
{"schema":"ADM_SIG_PRC","name":"SIG_ALMOXARIFADOS_CT","comments":"","lastddltime":"15/05/2019 10:59:39","createdtime":"12/05/2019 11:03:01"}
,
{"schema":"ADM_SIG_PRC","name":"SIG_BAIRRO_DISP","comments":"","lastddltime":"15/05/2019 10:59:40","createdtime":"12/05/2019 11:03:00"}
,
{"schema":"ADM_SIG_PRC","name":"SIG_BAIRROS","comments":"Cadastro de bairros","lastddltime":"15/05/2019 10:59:40","createdtime":"12/05/2019 11:01:13"}
,
{"schema":"ADM_SIG_PRC","name":"SIG_BAIRROS_CT","comments":"","lastddltime":"15/05/2019 10:59:40","createdtime":"12/05/2019 11:03:01"}
,
{"schema":"ADM_SIG_PRC","name":"SIG_CARGA_ICCID","comments":"Integra��o para inser��o de novos ICCIDs","lastddltime":"24/10/2019 09:12:55","createdtime":"12/05/2019 11:02:21"}
,
{"schema":"ADM_SIG_PRC","name":"SIG_CARGA_ICCID_CT","comments":"Tabela de controle de envio de Carga ICCID.","lastddltime":"15/05/2019 10:59:40","createdtime":"12/05/2019 11:02:58"}
,
{"schema":"ADM_SIG_PRC","name":"SIG_CIDADE_DISP","comments":"","lastddltime":"15/05/2019 10:59:40","createdtime":"12/05/2019 11:03:00"}
,
{"schema":"ADM_SIG_PRC","name":"SIG_CIDADES","comments":"Cadastro de Cidades","lastddltime":"15/05/2019 10:59:40","createdtime":"12/05/2019 11:01:12"}
,
{"schema":"ADM_SIG_PRC","name":"SIG_CIDADES_CT","comments":"","lastddltime":"15/05/2019 10:59:40","createdtime":"12/05/2019 11:03:01"}
,
{"schema":"ADM_SIG_PRC","name":"SIGCLARO_ALMOXARIFADO","comments":"","lastddltime":"19/09/2019 14:40:06","createdtime":"12/05/2019 11:01:18"}
,
{"schema":"ADM_SIG_PRC","name":"SIGCLARO_ALMOXARIFADO_CT","comments":"","lastddltime":"15/05/2019 10:59:40","createdtime":"12/05/2019 11:02:59"}
,
{"schema":"ADM_SIG_PRC","name":"SIGCLARO_ALMOXARIFADO_DISP","comments":"","lastddltime":"15/05/2019 10:59:40","createdtime":"12/05/2019 11:02:58"}
,
{"schema":"ADM_SIG_PRC","name":"SIGCLARO_BAIRRO","comments":"","lastddltime":"15/05/2019 10:59:40","createdtime":"12/05/2019 11:01:18"}
,
{"schema":"ADM_SIG_PRC","name":"SIGCLARO_BAIRRO_CT","comments":"","lastddltime":"15/05/2019 10:59:40","createdtime":"12/05/2019 11:02:59"}
,
{"schema":"ADM_SIG_PRC","name":"SIGCLARO_BAIRRO_DISP","comments":"","lastddltime":"15/05/2019 10:59:40","createdtime":"12/05/2019 11:02:59"}
,
{"schema":"ADM_SIG_PRC","name":"SIGCLARO_BLACKLIST","comments":"Lista de produtos que n�o enviamos no SIG CLARO.","lastddltime":"15/05/2019 10:59:40","createdtime":"12/05/2019 11:02:57"}
,
{"schema":"ADM_SIG_PRC","name":"SIGCLARO_CIDADE_DISP","comments":"","lastddltime":"15/05/2019 10:59:40","createdtime":"12/05/2019 11:02:59"}
,
{"schema":"ADM_SIG_PRC","name":"SIGCLARO_CLIENTE","comments":"","lastddltime":"15/05/2019 10:59:41","createdtime":"12/05/2019 11:01:18"}
,
{"schema":"ADM_SIG_PRC","name":"SIGCLARO_CLIENTE_CT","comments":"","lastddltime":"15/05/2019 10:59:41","createdtime":"12/05/2019 11:02:59"}
,
{"schema":"ADM_SIG_PRC","name":"SIGCLARO_CLIENTE_DISP","comments":"","lastddltime":"15/05/2019 10:59:41","createdtime":"12/05/2019 11:02:59"}
,
{"schema":"ADM_SIG_PRC","name":"SIGCLARO_CLIENTE_SETOR","comments":"Descri��o da tabela.","lastddltime":"15/05/2019 10:59:41","createdtime":"12/05/2019 11:02:57"}
,
{"schema":"ADM_SIG_PRC","name":"SIGCLARO_CLIENTE_SETOR_CT","comments":"Tabela de controle dos setores do cliente.","lastddltime":"15/05/2019 10:59:41","createdtime":"12/05/2019 11:02:57"}
,
{"schema":"ADM_SIG_PRC","name":"SIGCLARO_CLIENTE_SETOR_DISP","comments":"Disponibiliza��o de cliente setor.","lastddltime":"15/05/2019 10:59:41","createdtime":"12/05/2019 11:02:57"}
,
{"schema":"ADM_SIG_PRC","name":"SIGCLARO_CLIENTE_SINAL_CT","comments":"","lastddltime":"15/05/2019 10:59:41","createdtime":"12/05/2019 11:02:59"}
,
{"schema":"ADM_SIG_PRC","name":"SIGCLARO_CLIENTE_SINALIZACAO","comments":"","lastddltime":"15/05/2019 10:59:41","createdtime":"12/05/2019 11:01:19"}
,
{"schema":"ADM_SIG_PRC","name":"SIGCLARO_CLIENTE_SINALIZA_DISP","comments":"","lastddltime":"15/05/2019 10:59:41","createdtime":"12/05/2019 11:02:58"}
,
{"schema":"ADM_SIG_PRC","name":"SIGCLARO_CLIENTE_VISITA","comments":"","lastddltime":"15/05/2019 10:59:41","createdtime":"12/05/2019 11:01:18"}
,
{"schema":"ADM_SIG_PRC","name":"SIGCLARO_CLIENTE_VISITA_CT","comments":"","lastddltime":"15/05/2019 10:59:41","createdtime":"12/05/2019 11:02:59"}
,
{"schema":"ADM_SIG_PRC","name":"SIGCLARO_CLIENTE_VISITA_DISP","comments":"","lastddltime":"15/05/2019 10:59:41","createdtime":"12/05/2019 11:02:59"}
,
{"schema":"ADM_SIG_PRC","name":"SIGCLARO_CODIGO_ONLINE","comments":"Tabela armazena os codigos a serem enviados.","lastddltime":"17/05/2019 15:05:12","createdtime":"12/05/2019 11:02:58"}
,
{"schema":"ADM_SIG_PRC","name":"SIGCLARO_DP_DISTRIBUIDOR","comments":"Descri��o da tabela.","lastddltime":"28/08/2019 09:36:37","createdtime":"10/07/2019 15:36:38"}
,
{"schema":"ADM_SIG_PRC","name":"SIGCLARO_DP_EMPRESA","comments":"","lastddltime":"09/08/2019 10:54:08","createdtime":"12/05/2019 11:02:59"}
,
{"schema":"ADM_SIG_PRC","name":"SIGCLARO_DP_EST_EMP","comments":"Tabela de De/Para para o estabelecimento empresa.","lastddltime":"28/08/2019 09:36:47","createdtime":"12/05/2019 11:02:58"}
,
{"schema":"ADM_SIG_PRC","name":"SIGCLARO_DP_MOTIVO_DEVOLUCAO","comments":"Descri��o da tabela.","lastddltime":"30/08/2019 08:31:50","createdtime":"12/05/2019 11:02:57"}
,
{"schema":"ADM_SIG_PRC","name":"SIGCLARO_DP_PRODUTO","comments":"","lastddltime":"09/08/2019 10:51:44","createdtime":"12/05/2019 11:02:59"}
,
{"schema":"ADM_SIG_PRC","name":"SIGCLARO_DP_REDE","comments":"Entidade de De/Para para rede.","lastddltime":"15/08/2019 21:03:35","createdtime":"12/05/2019 11:02:57"}
,
{"schema":"ADM_SIG_PRC","name":"SIGCLARO_DP_SEGMENTOS","comments":"","lastddltime":"09/08/2019 11:00:21","createdtime":"12/05/2019 11:02:59"}
,
{"schema":"ADM_SIG_PRC","name":"SIGCLARO_DP_TIPO_LOGR","comments":"","lastddltime":"09/08/2019 11:07:59","createdtime":"12/05/2019 11:02:59"}
,
{"schema":"ADM_SIG_PRC","name":"SIGCLARO_EMPRESA_DISP","comments":"","lastddltime":"15/05/2019 10:59:42","createdtime":"12/05/2019 11:02:59"}
,
{"schema":"ADM_SIG_PRC","name":"SIGCLARO_EMPRESA_SEQ","comments":"","lastddltime":"15/05/2019 10:59:42","createdtime":"12/05/2019 11:02:58"}
,
{"schema":"ADM_SIG_PRC","name":"SIGCLARO_EST_EMP","comments":"","lastddltime":"15/05/2019 10:59:42","createdtime":"12/05/2019 11:02:58"}
,
{"schema":"ADM_SIG_PRC","name":"SIGCLARO_GEOCOORDOBJETOPONT_CT","comments":"Tabela de controle da geo coordenada.","lastddltime":"15/05/2019 10:59:42","createdtime":"12/05/2019 11:02:58"}
,
{"schema":"ADM_SIG_PRC","name":"SIGCLARO_GEOCOORDOBJETOPONTO","comments":"Tabela que envia  a geo coordenada.","lastddltime":"15/05/2019 10:59:42","createdtime":"12/05/2019 11:02:21"}
,
{"schema":"ADM_SIG_PRC","name":"SIGCLARO_GERENTE","comments":"","lastddltime":"09/08/2019 21:00:41","createdtime":"12/05/2019 11:01:18"}
,
{"schema":"ADM_SIG_PRC","name":"SIGCLARO_GERENTE_CT","comments":"","lastddltime":"15/05/2019 10:59:42","createdtime":"12/05/2019 11:02:59"}
,
{"schema":"ADM_SIG_PRC","name":"SIGCLARO_GERENTE_DISP","comments":"","lastddltime":"15/05/2019 10:59:42","createdtime":"12/05/2019 11:02:59"}
,
{"schema":"ADM_SIG_PRC","name":"SIGCLARO_GRUPO_PRODUTO_DISP","comments":"","lastddltime":"15/05/2019 10:59:42","createdtime":"12/05/2019 11:02:59"}
,
{"schema":"ADM_SIG_PRC","name":"SIGCLARO_ICCID","comments":"Tabela que envia o iccid do CHIP .","lastddltime":"06/12/2019 10:14:28","createdtime":"12/05/2019 11:02:21"}
,
{"schema":"ADM_SIG_PRC","name":"SIGCLARO_ICCID_CT","comments":"Tabela de controle de envio de ICCID.","lastddltime":"15/05/2019 10:59:42","createdtime":"12/05/2019 11:02:58"}
,
{"schema":"ADM_SIG_PRC","name":"SIGCLARO_ICCID_DISP","comments":"","lastddltime":"15/05/2019 10:59:42","createdtime":"12/05/2019 11:02:58"}
,
{"schema":"ADM_SIG_PRC","name":"SIGCLARO_ICCID_JDE","comments":"Tabela para carregar ICCID de Pedidos feitos pelo JDE.","lastddltime":"15/05/2019 10:59:42","createdtime":"12/05/2019 11:02:58"}
,
{"schema":"ADM_SIG_PRC","name":"SIGCLARO_INTEGRADOR_DISP","comments":"","lastddltime":"15/05/2019 10:59:42","createdtime":"12/05/2019 11:02:59"}
,
{"schema":"ADM_SIG_PRC","name":"SIGCLARO_MOTIVO_DEVOLUCAO_DISP","comments":"Tabela para a disponibiliza��o de Motivo devolu��o.","lastddltime":"15/05/2019 10:59:43","createdtime":"12/05/2019 11:02:57"}
,
{"schema":"ADM_SIG_PRC","name":"SIGCLARO_MOTIVO_VISITA","comments":"","lastddltime":"15/05/2019 10:59:43","createdtime":"12/05/2019 11:01:18"}
,
{"schema":"ADM_SIG_PRC","name":"SIGCLARO_MOTIVO_VISITA_CT","comments":"","lastddltime":"15/05/2019 10:59:43","createdtime":"12/05/2019 11:02:59"}
,
{"schema":"ADM_SIG_PRC","name":"SIGCLARO_MOTIVO_VISITA_DISP","comments":"","lastddltime":"15/05/2019 10:59:43","createdtime":"12/05/2019 11:02:59"}
,
{"schema":"ADM_SIG_PRC","name":"SIGCLARO_PEDIDO","comments":"","lastddltime":"15/05/2019 10:59:43","createdtime":"12/05/2019 11:01:19"}
,
{"schema":"ADM_SIG_PRC","name":"SIGCLARO_PEDIDO_DISP","comments":"","lastddltime":"15/05/2019 10:59:43","createdtime":"12/05/2019 11:02:59"}
,
{"schema":"ADM_SIG_PRC","name":"SIGCLARO_PORTFOLIO","comments":"Descri��o da tabela.","lastddltime":"15/05/2019 10:59:43","createdtime":"12/05/2019 11:02:57"}
,
{"schema":"ADM_SIG_PRC","name":"SIGCLARO_PRODUTO_DISP","comments":"","lastddltime":"15/05/2019 10:59:43","createdtime":"12/05/2019 11:02:58"}
,
{"schema":"ADM_SIG_PRC","name":"SIGCLARO_REDE_PDV_DISP","comments":"C�digo do Sequencial da Rede do Pdv.","lastddltime":"15/08/2019 21:03:36","createdtime":"12/05/2019 11:02:58"}
,
{"schema":"ADM_SIG_PRC","name":"SIGCLARO_RELEITURA_ICCID","comments":"Tabela que envia o iccid do CHIP .","lastddltime":"15/05/2019 10:59:44","createdtime":"12/05/2019 11:02:26"}
,
{"schema":"ADM_SIG_PRC","name":"SIGCLARO_RELEITURA_ICCID_CT","comments":"Tabela de controle de envio de ICCID.","lastddltime":"15/05/2019 10:59:44","createdtime":"12/05/2019 11:02:57"}
,
{"schema":"ADM_SIG_PRC","name":"SIGCLARO_RUPTURA","comments":"Respons�vel por armazenar as informa��a de Rupturas de Clientes.","lastddltime":"12/07/2019 21:02:15","createdtime":"08/07/2019 10:18:13"}
,
{"schema":"ADM_SIG_PRC","name":"SIGCLARO_SEGMENTO_DISP","comments":"","lastddltime":"21/08/2019 21:01:53","createdtime":"12/05/2019 11:02:58"}
,
{"schema":"ADM_SIG_PRC","name":"SIGCLARO_SETOR","comments":"","lastddltime":"15/05/2019 10:59:44","createdtime":"12/05/2019 11:01:19"}
,
{"schema":"ADM_SIG_PRC","name":"SIGCLARO_SETOR_CT","comments":"","lastddltime":"15/05/2019 10:59:44","createdtime":"12/05/2019 11:02:59"}
,
{"schema":"ADM_SIG_PRC","name":"SIGCLARO_SETOR_DISP","comments":"","lastddltime":"15/05/2019 10:59:44","createdtime":"12/05/2019 11:02:58"}
,
{"schema":"ADM_SIG_PRC","name":"SIGCLARO_SUPERVISOR","comments":"","lastddltime":"15/05/2019 10:59:44","createdtime":"12/05/2019 11:01:19"}
,
{"schema":"ADM_SIG_PRC","name":"SIGCLARO_SUPERVISOR_CT","comments":"","lastddltime":"15/05/2019 10:59:44","createdtime":"12/05/2019 11:02:59"}
,
{"schema":"ADM_SIG_PRC","name":"SIGCLARO_SUPERVISOR_DISP","comments":"","lastddltime":"15/05/2019 10:59:44","createdtime":"12/05/2019 11:02:58"}
,
{"schema":"ADM_SIG_PRC","name":"SIGCLARO_TIPO_CLIENTE","comments":"","lastddltime":"15/05/2019 10:59:44","createdtime":"12/05/2019 11:01:19"}
,
{"schema":"ADM_SIG_PRC","name":"SIGCLARO_TIPO_CLIENTE_CT","comments":"","lastddltime":"15/05/2019 10:59:44","createdtime":"12/05/2019 11:02:59"}
,
{"schema":"ADM_SIG_PRC","name":"SIGCLARO_TIPO_CLIENTE_DISP","comments":"","lastddltime":"15/05/2019 10:59:44","createdtime":"12/05/2019 11:02:58"}
,
{"schema":"ADM_SIG_PRC","name":"SIGCLARO_TIPO_LOGRADOURO_DISP","comments":"","lastddltime":"15/05/2019 10:59:44","createdtime":"12/05/2019 11:02:58"}
,
{"schema":"ADM_SIG_PRC","name":"SIGCLARO_TIPO_SINALIZACAO_DISP","comments":"","lastddltime":"15/05/2019 10:59:44","createdtime":"12/05/2019 11:02:59"}
,
{"schema":"ADM_SIG_PRC","name":"SIGCLARO_USUARIOS","comments":"","lastddltime":"15/05/2019 10:59:44","createdtime":"12/05/2019 11:02:58"}
,
{"schema":"ADM_SIG_PRC","name":"SIGCLARO_VENDEDOR","comments":"","lastddltime":"15/05/2019 10:59:44","createdtime":"12/05/2019 11:01:19"}
,
{"schema":"ADM_SIG_PRC","name":"SIGCLARO_VENDEDOR_CT","comments":"","lastddltime":"15/05/2019 10:59:44","createdtime":"12/05/2019 11:02:59"}
,
{"schema":"ADM_SIG_PRC","name":"SIGCLARO_VENDEDOR_DISP","comments":"","lastddltime":"15/05/2019 10:59:45","createdtime":"12/05/2019 11:02:58"}
,
{"schema":"ADM_SIG_PRC","name":"SIGCLARO_VISITA","comments":"","lastddltime":"15/05/2019 10:59:45","createdtime":"12/05/2019 11:01:19"}
,
{"schema":"ADM_SIG_PRC","name":"SIGCLARO_VISITA_DISP","comments":"","lastddltime":"15/05/2019 10:59:45","createdtime":"12/05/2019 11:02:58"}
,
{"schema":"ADM_SIG_PRC","name":"SIG_CLIENTE_DISP","comments":"","lastddltime":"15/05/2019 10:59:45","createdtime":"12/05/2019 11:03:00"}
,
{"schema":"ADM_SIG_PRC","name":"SIG_CLIENTES","comments":"","lastddltime":"15/05/2019 10:59:45","createdtime":"12/05/2019 11:01:14"}
,
{"schema":"ADM_SIG_PRC","name":"SIG_CLIENTES_CT","comments":"","lastddltime":"15/05/2019 10:59:45","createdtime":"12/05/2019 11:03:01"}
,
{"schema":"ADM_SIG_PRC","name":"SIG_CLIENTE_SETOR","comments":"Descri��o da tabela.","lastddltime":"15/05/2019 10:59:45","createdtime":"12/05/2019 11:02:57"}
,
{"schema":"ADM_SIG_PRC","name":"SIG_CLIENTE_SETOR_DISP","comments":"Disponibiliza��o de cliente setor.","lastddltime":"15/05/2019 10:59:45","createdtime":"12/05/2019 11:02:57"}
,
{"schema":"ADM_SIG_PRC","name":"SIG_CLIENTES_SETOR_CT","comments":"","lastddltime":"15/05/2019 10:59:45","createdtime":"12/05/2019 11:02:58"}
,
{"schema":"ADM_SIG_PRC","name":"SIG_DETALHE_MSG","comments":"Mensagens que ter�o um novo status.","lastddltime":"15/05/2019 10:59:45","createdtime":"12/05/2019 11:02:57"}
,
{"schema":"ADM_SIG_PRC","name":"SIG_EMPRESA_DISP","comments":"","lastddltime":"15/05/2019 10:59:45","createdtime":"12/05/2019 11:03:00"}
,
{"schema":"ADM_SIG_PRC","name":"SIG_ESTOQUE_DEVOLUCAO","comments":"Locais de estoque para a Devolu��o.","lastddltime":"15/05/2019 10:59:45","createdtime":"12/05/2019 11:02:58"}
,
{"schema":"ADM_SIG_PRC","name":"SIG_EXEC","comments":"","lastddltime":"19/09/2019 14:40:06","createdtime":"12/05/2019 11:01:09"}
,
{"schema":"ADM_SIG_PRC","name":"SIG_EXEC_LOG","comments":"Contem Os logs de execu��o","lastddltime":"15/05/2019 10:59:45","createdtime":"12/05/2019 11:03:01"}
,
{"schema":"ADM_SIG_PRC","name":"SIG_FUNCIONALIDADE","comments":"","lastddltime":"15/05/2019 10:59:45","createdtime":"12/05/2019 11:02:59"}
,
{"schema":"ADM_SIG_PRC","name":"SIG_GEOCOORDOBJETOPONTO","comments":"Tabela que envia a Geo coordenada.","lastddltime":"24/10/2019 09:12:39","createdtime":"12/05/2019 11:02:22"}
,
{"schema":"ADM_SIG_PRC","name":"SIG_GEOCOORDOBJETOPONTO_CT","comments":"Tabela de controle da geo coordenada.","lastddltime":"15/05/2019 10:59:46","createdtime":"12/05/2019 11:02:58"}
,
{"schema":"ADM_SIG_PRC","name":"SIG_GERENTE_DISP","comments":"","lastddltime":"15/05/2019 10:59:46","createdtime":"12/05/2019 11:02:59"}
,
{"schema":"ADM_SIG_PRC","name":"SIG_GERENTES","comments":"","lastddltime":"15/05/2019 10:59:46","createdtime":"12/05/2019 11:01:14"}
,
{"schema":"ADM_SIG_PRC","name":"SIG_GERENTES_CT","comments":"","lastddltime":"15/05/2019 10:59:46","createdtime":"12/05/2019 11:03:01"}
,
{"schema":"ADM_SIG_PRC","name":"SIG_GERENTES_TEMP","comments":"Tabela para aux�lio no controle de status dos gerentes.","lastddltime":"15/05/2019 10:59:46","createdtime":"12/05/2019 11:02:28"}
,
{"schema":"ADM_SIG_PRC","name":"SIG_GRUPOS","comments":"Identifica os grupos de Informa��o","lastddltime":"15/05/2019 10:59:46","createdtime":"12/05/2019 11:03:01"}
,
{"schema":"ADM_SIG_PRC","name":"SIG_ICCID_DISP","comments":"Descri��o da tabela.","lastddltime":"15/05/2019 10:59:46","createdtime":"12/05/2019 11:02:58"}
,
{"schema":"ADM_SIG_PRC","name":"SIG_ID_GERAL","comments":"","lastddltime":"15/05/2019 10:59:46","createdtime":"12/05/2019 11:03:01"}
,
{"schema":"ADM_SIG_PRC","name":"SIG_MOTIVOS_VISITAS","comments":"","lastddltime":"15/05/2019 10:59:46","createdtime":"12/05/2019 11:01:15"}
,
{"schema":"ADM_SIG_PRC","name":"SIG_MOTIVOS_VISITAS_CT","comments":"","lastddltime":"15/05/2019 10:59:46","createdtime":"12/05/2019 11:03:01"}
,
{"schema":"ADM_SIG_PRC","name":"SIG_MOTIVO_VISITA_DISP","comments":"","lastddltime":"15/05/2019 10:59:46","createdtime":"12/05/2019 11:02:59"}
,
{"schema":"ADM_SIG_PRC","name":"SIG_MOVIMENTACAO_ICCID","comments":"Tabela que registra as movimentações para rastreio.","lastddltime":"24/10/2019 09:12:35","createdtime":"12/05/2019 11:02:22"}
,
{"schema":"ADM_SIG_PRC","name":"SIG_MOVIMENTACAO_ICCID_CT","comments":"Tabela que realiza o controle de dbAcao da movimentação ICCID.","lastddltime":"15/05/2019 10:59:46","createdtime":"12/05/2019 11:02:58"}
,
{"schema":"ADM_SIG_PRC","name":"SIG_OBJ_CARREGADOS","comments":"Conterm a informa��o de todos os objetos que foram carregados na execu��o","lastddltime":"15/05/2019 10:59:47","createdtime":"12/05/2019 11:01:12"}
,
{"schema":"ADM_SIG_PRC","name":"SIG_OBJETO_DISP","comments":"","lastddltime":"15/05/2019 10:59:47","createdtime":"12/05/2019 11:03:00"}
,
{"schema":"ADM_SIG_PRC","name":"SIG_OBJETOS","comments":"Identifica os objetos que ser�o transferidos","lastddltime":"15/05/2019 10:59:47","createdtime":"12/05/2019 11:03:01"}
,
{"schema":"ADM_SIG_PRC","name":"SIG_OBJ_LOG","comments":"Comtem os Logs dos objetos","lastddltime":"15/05/2019 10:59:47","createdtime":"12/05/2019 11:03:01"}
,
{"schema":"ADM_SIG_PRC","name":"SIGOI_ALMOXARIFADO_DISP","comments":"","lastddltime":"15/05/2019 10:59:47","createdtime":"12/05/2019 11:03:00"}
,
{"schema":"ADM_SIG_PRC","name":"SIGOI_ALMOXARIFADOS","comments":"","lastddltime":"15/05/2019 10:59:47","createdtime":"12/05/2019 11:01:18"}
,
{"schema":"ADM_SIG_PRC","name":"SIGOI_ALMOXARIFADOS_CT","comments":"","lastddltime":"15/05/2019 10:59:47","createdtime":"12/05/2019 11:03:00"}
,
{"schema":"ADM_SIG_PRC","name":"SIGOI_BAIRRO_DISP","comments":"","lastddltime":"15/05/2019 10:59:47","createdtime":"12/05/2019 11:03:00"}
,
{"schema":"ADM_SIG_PRC","name":"SIGOI_BAIRROS","comments":"Cadastro de bairros","lastddltime":"15/05/2019 10:59:47","createdtime":"12/05/2019 11:01:16"}
,
{"schema":"ADM_SIG_PRC","name":"SIGOI_BAIRROS_CT","comments":"","lastddltime":"15/05/2019 10:59:47","createdtime":"12/05/2019 11:03:00"}
,
{"schema":"ADM_SIG_PRC","name":"SIGOI_BLACKLIST","comments":"Blacklist de produtos que n�o devem ser enviados no Sig Oi.","lastddltime":"15/05/2019 10:59:47","createdtime":"12/05/2019 11:02:57"}
,
{"schema":"ADM_SIG_PRC","name":"SIGOI_CANAL_RECARGA_DISP","comments":"Servi�o de Integra��o para obten��o do Canal de Recarga","lastddltime":"12/05/2019 12:54:20","createdtime":"12/05/2019 11:02:58"}
,
{"schema":"ADM_SIG_PRC","name":"SIGOI_CIDADE_DISP","comments":"","lastddltime":"15/05/2019 10:59:47","createdtime":"12/05/2019 11:03:00"}
,
{"schema":"ADM_SIG_PRC","name":"SIGOI_CIDADES","comments":"Cadastro de Cidades","lastddltime":"15/05/2019 10:59:47","createdtime":"12/05/2019 11:01:16"}
,
{"schema":"ADM_SIG_PRC","name":"SIGOI_CIDADES_CT","comments":"","lastddltime":"15/05/2019 10:59:47","createdtime":"12/05/2019 11:03:00"}
,
{"schema":"ADM_SIG_PRC","name":"SIGOI_CLIENTE_DISP","comments":"","lastddltime":"15/05/2019 10:59:47","createdtime":"12/05/2019 11:03:00"}
,
{"schema":"ADM_SIG_PRC","name":"SIGOI_CLIENTES","comments":"","lastddltime":"28/11/2019 11:32:23","createdtime":"12/05/2019 11:01:17"}
,
{"schema":"ADM_SIG_PRC","name":"SIGOI_CLIENTES_CT","comments":"","lastddltime":"28/11/2019 11:31:06","createdtime":"12/05/2019 11:03:00"}
,
{"schema":"ADM_SIG_PRC","name":"SIGOI_CLIENTES_SETORES","comments":"","lastddltime":"15/05/2019 10:59:48","createdtime":"12/05/2019 11:02:58"}
,
{"schema":"ADM_SIG_PRC","name":"SIGOI_CLIENTES_SETORES_CT","comments":"","lastddltime":"15/05/2019 10:59:48","createdtime":"12/05/2019 11:02:58"}
,
{"schema":"ADM_SIG_PRC","name":"SIGOI_CLIENTES_SETORES_DISP","comments":"Tabela de disponibiliza��o dos clientes setores","lastddltime":"15/05/2019 10:59:48","createdtime":"12/05/2019 11:02:57"}
,
{"schema":"ADM_SIG_PRC","name":"SIGOI_CLIENTES_TUP","comments":"Servi�o de Integra��o para obten��o do Cadastro de Clientes Tups","lastddltime":"15/05/2019 10:59:48","createdtime":"12/05/2019 11:02:08"}
,
{"schema":"ADM_SIG_PRC","name":"SIGOI_CLIENTES_TUP_CT","comments":"","lastddltime":"12/05/2019 12:54:20","createdtime":"12/05/2019 11:02:58"}
,
{"schema":"ADM_SIG_PRC","name":"SIGOI_CLIENTES_TUP_DISP","comments":"Servi�o de Integra��o para obten��o do Cadastro de Clientes Tups","lastddltime":"12/05/2019 12:54:20","createdtime":"12/05/2019 11:02:58"}
,
{"schema":"ADM_SIG_PRC","name":"SIGOI_CODIGO_ONLINE","comments":"","lastddltime":"15/05/2019 10:59:48","createdtime":"12/05/2019 11:02:58"}
,
{"schema":"ADM_SIG_PRC","name":"SIGOI_DP_CANAL_RECARGA","comments":"Tabela armazena de/para do Canal Recarga.","lastddltime":"19/08/2019 11:29:06","createdtime":"12/05/2019 11:02:58"}
,
{"schema":"ADM_SIG_PRC","name":"SIGOI_DP_CANAL_VENDA","comments":"Tabela que define canal venda a ser utilizado no de para de produtos.","lastddltime":"30/08/2019 12:02:11","createdtime":"12/05/2019 11:02:58"}
,
{"schema":"ADM_SIG_PRC","name":"SIGOI_DP_EMPRESA","comments":"","lastddltime":"23/08/2019 11:19:16","createdtime":"12/05/2019 11:03:00"}
,
{"schema":"ADM_SIG_PRC","name":"SIGOI_DP_PERIODICIDADE","comments":"Tabela armazena de/para de periodicidade do Sig Oi.","lastddltime":"19/08/2019 12:47:27","createdtime":"12/05/2019 11:02:58"}
,
{"schema":"ADM_SIG_PRC","name":"SIGOI_DP_PRODUTO","comments":"","lastddltime":"21/08/2019 21:01:52","createdtime":"12/05/2019 11:03:00"}
,
{"schema":"ADM_SIG_PRC","name":"SIGOI_DP_REDE_PDV","comments":"","lastddltime":"21/08/2019 09:49:14","createdtime":"12/05/2019 11:02:57"}
,
{"schema":"ADM_SIG_PRC","name":"SIGOI_DP_SEGMENTOS","comments":"","lastddltime":"19/08/2019 12:56:00","createdtime":"12/05/2019 11:03:00"}
,
{"schema":"ADM_SIG_PRC","name":"SIGOI_EMPRESA_DISP","comments":"","lastddltime":"15/05/2019 10:59:48","createdtime":"12/05/2019 11:03:00"}
,
{"schema":"ADM_SIG_PRC","name":"SIGOI_EMPRESA_SEQ","comments":"","lastddltime":"15/05/2019 10:59:48","createdtime":"12/05/2019 11:03:00"}
,
{"schema":"ADM_SIG_PRC","name":"SIGOI_EST_EMP","comments":"","lastddltime":"15/05/2019 10:59:48","createdtime":"12/05/2019 11:03:00"}
,
{"schema":"ADM_SIG_PRC","name":"SIGOI_ESTOQUES","comments":"","lastddltime":"15/05/2019 10:59:49","createdtime":"12/05/2019 11:02:21"}
,
{"schema":"ADM_SIG_PRC","name":"SIGOI_ESTOQUES_CT","comments":"","lastddltime":"15/05/2019 10:59:49","createdtime":"12/05/2019 11:02:58"}
,
{"schema":"ADM_SIG_PRC","name":"SIGOI_GEOAGRUPACIDADE_DISP","comments":"","lastddltime":"12/05/2019 12:54:20","createdtime":"12/05/2019 11:02:58"}
,
{"schema":"ADM_SIG_PRC","name":"SIGOI_GEOCOORDOBJETOPONTO","comments":"","lastddltime":"28/11/2019 11:31:07","createdtime":"28/11/2019 07:45:30"}
,
{"schema":"ADM_SIG_PRC","name":"SIGOI_GEOCOORDOBJETOPONTO_CT","comments":"","lastddltime":"28/11/2019 15:20:49","createdtime":"28/11/2019 07:48:24"}
,
{"schema":"ADM_SIG_PRC","name":"SIGOI_GEOCOORDOBJETOPONTO_DISP","comments":"","lastddltime":"12/05/2019 12:54:20","createdtime":"12/05/2019 11:02:58"}
,
{"schema":"ADM_SIG_PRC","name":"SIGOI_GEOCOORDOBJETOTIPO_DISP","comments":"","lastddltime":"12/05/2019 12:54:20","createdtime":"12/05/2019 11:02:58"}
,
{"schema":"ADM_SIG_PRC","name":"SIGOI_GERENTE_DISP","comments":"","lastddltime":"15/05/2019 10:59:49","createdtime":"12/05/2019 11:03:00"}
,
{"schema":"ADM_SIG_PRC","name":"SIGOI_GERENTES","comments":"","lastddltime":"15/05/2019 10:59:49","createdtime":"12/05/2019 11:01:17"}
,
{"schema":"ADM_SIG_PRC","name":"SIGOI_GERENTES_CT","comments":"","lastddltime":"15/05/2019 10:59:49","createdtime":"12/05/2019 11:03:00"}
,
{"schema":"ADM_SIG_PRC","name":"SIGOI_ICCID_JDE","comments":"Tabela para carregar ICCID de Pedidos feitos pelo JDE.","lastddltime":"11/12/2019 11:36:08","createdtime":"11/12/2019 11:36:07"}
,
{"schema":"ADM_SIG_PRC","name":"SIGOI_LIMITE_CREDITO","comments":"Informa os limites de cr�dito do cliente.","lastddltime":"15/05/2019 10:59:49","createdtime":"12/05/2019 11:02:21"}
,
{"schema":"ADM_SIG_PRC","name":"SIGOI_LIMITE_CREDITO_CT","comments":"Tabela de controle de envio do limite cr�dito.","lastddltime":"15/05/2019 10:59:49","createdtime":"12/05/2019 11:02:58"}
,
{"schema":"ADM_SIG_PRC","name":"SIGOI_MOTIVOS_VISITAS","comments":"","lastddltime":"15/05/2019 10:59:49","createdtime":"12/05/2019 11:01:17"}
,
{"schema":"ADM_SIG_PRC","name":"SIGOI_MOTIVOS_VISITAS_CT","comments":"","lastddltime":"15/05/2019 10:59:49","createdtime":"12/05/2019 11:03:00"}
,
{"schema":"ADM_SIG_PRC","name":"SIGOI_PEDIDO_DISP","comments":"","lastddltime":"15/05/2019 10:59:49","createdtime":"12/05/2019 11:03:00"}
,
{"schema":"ADM_SIG_PRC","name":"SIGOI_PEDIDOS","comments":"","lastddltime":"05/12/2019 16:28:23","createdtime":"12/05/2019 11:01:18"}
,
{"schema":"ADM_SIG_PRC","name":"SIGOI_PEDIDOS_CT","comments":"","lastddltime":"15/05/2019 10:59:49","createdtime":"12/05/2019 11:03:00"}
,
{"schema":"ADM_SIG_PRC","name":"SIGOI_PRODUTO_DISP","comments":"","lastddltime":"01/07/2019 21:02:02","createdtime":"12/05/2019 11:03:00"}
,
{"schema":"ADM_SIG_PRC","name":"SIGOI_RASTREABILIDADEIDS","comments":"","lastddltime":"15/05/2019 10:59:49","createdtime":"12/05/2019 11:02:21"}
,
{"schema":"ADM_SIG_PRC","name":"SIGOI_REDE_PDV_DISP","comments":"","lastddltime":"15/05/2019 10:59:50","createdtime":"12/05/2019 11:02:58"}
,
{"schema":"ADM_SIG_PRC","name":"SIGOI_SEGMENTO_DISP","comments":"","lastddltime":"15/05/2019 10:59:50","createdtime":"12/05/2019 11:03:00"}
,
{"schema":"ADM_SIG_PRC","name":"SIGOI_SEGMENTOS","comments":"","lastddltime":"15/05/2019 10:59:50","createdtime":"12/05/2019 11:01:17"}
,
{"schema":"ADM_SIG_PRC","name":"SIGOI_SEGMENTOS_CT","comments":"","lastddltime":"15/05/2019 10:59:50","createdtime":"12/05/2019 11:03:00"}
,
{"schema":"ADM_SIG_PRC","name":"SIGOI_SETOR_DISP","comments":"","lastddltime":"15/05/2019 10:59:50","createdtime":"12/05/2019 11:03:00"}
,
{"schema":"ADM_SIG_PRC","name":"SIGOI_SETORES","comments":"","lastddltime":"15/05/2019 10:59:50","createdtime":"12/05/2019 11:01:17"}
,
{"schema":"ADM_SIG_PRC","name":"SIGOI_SETORES_CT","comments":"","lastddltime":"15/05/2019 10:59:50","createdtime":"12/05/2019 11:03:00"}
,
{"schema":"ADM_SIG_PRC","name":"SIGOI_SUPERVISOR_DISP","comments":"","lastddltime":"15/05/2019 10:59:50","createdtime":"12/05/2019 11:03:00"}
,
{"schema":"ADM_SIG_PRC","name":"SIGOI_SUPERVISORES","comments":"","lastddltime":"15/05/2019 10:59:50","createdtime":"12/05/2019 11:01:17"}
,
{"schema":"ADM_SIG_PRC","name":"SIGOI_SUPERVISORES_CT","comments":"","lastddltime":"15/05/2019 10:59:50","createdtime":"12/05/2019 11:03:00"}
,
{"schema":"ADM_SIG_PRC","name":"SIGOI_TIPOCLIENTE_DISP","comments":"","lastddltime":"15/05/2019 10:59:50","createdtime":"12/05/2019 11:03:00"}
,
{"schema":"ADM_SIG_PRC","name":"SIGOI_TIPOLOGRADOURO_DISP","comments":"","lastddltime":"15/05/2019 10:59:50","createdtime":"12/05/2019 11:03:00"}
,
{"schema":"ADM_SIG_PRC","name":"SIGOI_TIPOSCLIENTES","comments":"Cadastro de Tipos de Clientes","lastddltime":"15/05/2019 10:59:50","createdtime":"12/05/2019 11:01:16"}
,
{"schema":"ADM_SIG_PRC","name":"SIGOI_TIPOSCLIENTES_CT","comments":"","lastddltime":"15/05/2019 10:59:50","createdtime":"12/05/2019 11:03:00"}
,
{"schema":"ADM_SIG_PRC","name":"SIGOI_TIPOSLOGRADOUROS","comments":"","lastddltime":"15/05/2019 10:59:50","createdtime":"12/05/2019 11:01:16"}
,
{"schema":"ADM_SIG_PRC","name":"SIGOI_TIPOSLOGRADOUROS_CT","comments":"","lastddltime":"15/05/2019 10:59:50","createdtime":"12/05/2019 11:03:00"}
,
{"schema":"ADM_SIG_PRC","name":"SIGOI_USUARIOS","comments":"","lastddltime":"15/05/2019 10:59:50","createdtime":"12/05/2019 11:03:00"}
,
{"schema":"ADM_SIG_PRC","name":"SIGOI_VENDEDOR_DISP","comments":"","lastddltime":"15/05/2019 10:59:51","createdtime":"12/05/2019 11:03:00"}
,
{"schema":"ADM_SIG_PRC","name":"SIGOI_VENDEDORES","comments":"","lastddltime":"15/05/2019 10:59:51","createdtime":"12/05/2019 11:01:17"}
,
{"schema":"ADM_SIG_PRC","name":"SIGOI_VENDEDORES_CT","comments":"","lastddltime":"15/05/2019 10:59:51","createdtime":"12/05/2019 11:03:00"}
,
{"schema":"ADM_SIG_PRC","name":"SIGOI_VISITAS","comments":"","lastddltime":"15/05/2019 10:59:51","createdtime":"12/05/2019 11:01:17"}
,
{"schema":"ADM_SIG_PRC","name":"SIGOI_VISITAS_CT","comments":"","lastddltime":"15/05/2019 10:59:51","createdtime":"12/05/2019 11:03:00"}
,
{"schema":"ADM_SIG_PRC","name":"SIG_PARAMETRO_SERVICO","comments":"","lastddltime":"22/07/2019 08:52:43","createdtime":"12/05/2019 11:03:01"}
,
{"schema":"ADM_SIG_PRC","name":"SIG_PEDIDO_DISP","comments":"","lastddltime":"15/05/2019 10:59:51","createdtime":"12/05/2019 11:02:59"}
,
{"schema":"ADM_SIG_PRC","name":"SIG_PEDIDOS","comments":"","lastddltime":"15/05/2019 10:59:51","createdtime":"12/05/2019 11:01:16"}
,
{"schema":"ADM_SIG_PRC","name":"SIG_PEDIDOS_CT","comments":"","lastddltime":"15/05/2019 10:59:51","createdtime":"12/05/2019 11:03:01"}
,
{"schema":"ADM_SIG_PRC","name":"SIG_PERMISSAO","comments":"","lastddltime":"15/05/2019 10:59:51","createdtime":"12/05/2019 11:02:59"}
,
{"schema":"ADM_SIG_PRC","name":"SIG_PROCESSO_OBJETO_DISP","comments":"","lastddltime":"15/05/2019 10:59:51","createdtime":"12/05/2019 11:03:00"}
,
{"schema":"ADM_SIG_PRC","name":"SIG_PROCESSOS","comments":"Cadastro dos processos SIG","lastddltime":"10/07/2019 21:01:05","createdtime":"12/05/2019 11:03:01"}
,
{"schema":"ADM_SIG_PRC","name":"SIG_PROCESSOS_02033013_BKP","comments":"","lastddltime":"12/05/2019 11:17:05","createdtime":"12/05/2019 11:03:01"}
,
{"schema":"ADM_SIG_PRC","name":"SIG_PROC_LOG","comments":"Comtem os Logs do processo","lastddltime":"15/05/2019 10:59:51","createdtime":"12/05/2019 11:03:01"}
,
{"schema":"ADM_SIG_PRC","name":"SIG_PRODUTO_DISP","comments":"","lastddltime":"15/05/2019 10:59:51","createdtime":"12/05/2019 11:03:00"}
,
{"schema":"ADM_SIG_PRC","name":"SIG_REDE_DISP","comments":"Armazena as redes da operadora","lastddltime":"12/05/2019 12:54:20","createdtime":"12/05/2019 11:02:58"}
,
{"schema":"ADM_SIG_PRC","name":"SIG_SEGMENTO_CLARO","comments":"","lastddltime":"15/05/2019 10:59:51","createdtime":"12/05/2019 11:02:58"}
,
{"schema":"ADM_SIG_PRC","name":"SIG_SEGMENTO_DISP","comments":"","lastddltime":"15/05/2019 10:59:51","createdtime":"12/05/2019 11:03:00"}
,
{"schema":"ADM_SIG_PRC","name":"SIG_SEGMENTO_OPERADORA_DISP","comments":"Armazena o segmento da operadora","lastddltime":"15/05/2019 10:59:51","createdtime":"12/05/2019 11:02:58"}
,
{"schema":"ADM_SIG_PRC","name":"SIG_SEGMENTOS","comments":"","lastddltime":"15/05/2019 10:59:52","createdtime":"12/05/2019 11:01:14"}
,
{"schema":"ADM_SIG_PRC","name":"SIG_SEGMENTOS_CT","comments":"","lastddltime":"15/05/2019 10:59:52","createdtime":"12/05/2019 11:03:01"}
,
{"schema":"ADM_SIG_PRC","name":"SIG_SETOR_DISP","comments":"","lastddltime":"15/05/2019 10:59:52","createdtime":"12/05/2019 11:02:59"}
,
{"schema":"ADM_SIG_PRC","name":"SIG_SETORES","comments":"","lastddltime":"15/05/2019 10:59:52","createdtime":"12/05/2019 11:01:16"}
,
{"schema":"ADM_SIG_PRC","name":"SIG_SETORES_CT","comments":"","lastddltime":"15/05/2019 10:59:52","createdtime":"12/05/2019 11:03:01"}
,
{"schema":"ADM_SIG_PRC","name":"SIG_SUPERVISOR_DISP","comments":"","lastddltime":"15/05/2019 10:59:52","createdtime":"12/05/2019 11:02:59"}
,
{"schema":"ADM_SIG_PRC","name":"SIG_SUPERVISORES","comments":"","lastddltime":"15/05/2019 10:59:52","createdtime":"12/05/2019 11:01:15"}
,
{"schema":"ADM_SIG_PRC","name":"SIG_SUPERVISORES_CT","comments":"","lastddltime":"15/05/2019 10:59:52","createdtime":"12/05/2019 11:03:01"}
,
{"schema":"ADM_SIG_PRC","name":"SIG_SUPERVISORES_TEMP","comments":"Tabela para aux�lio no controle de status dos supervisores.","lastddltime":"15/05/2019 10:59:52","createdtime":"12/05/2019 11:02:28"}
,
{"schema":"ADM_SIG_PRC","name":"SIGTIM_BLACKLIST","comments":"Lista de produtos que n�o enviamos no SIG TIM.","lastddltime":"15/05/2019 10:59:52","createdtime":"12/05/2019 11:02:57"}
,
{"schema":"ADM_SIG_PRC","name":"SIGTIM_CODIGO_ONLINE","comments":"","lastddltime":"15/05/2019 10:59:52","createdtime":"12/05/2019 11:02:59"}
,
{"schema":"ADM_SIG_PRC","name":"SIGTIM_DP_CLIENTE_REDE","comments":"Tabela armazena de/para de redes do Sig TIM.","lastddltime":"09/08/2019 08:07:07","createdtime":"12/05/2019 11:02:58"}
,
{"schema":"ADM_SIG_PRC","name":"SIGTIM_DP_EMPRESA","comments":"","lastddltime":"09/08/2019 08:07:05","createdtime":"12/05/2019 11:03:01"}
,
{"schema":"ADM_SIG_PRC","name":"SIGTIM_DP_MOTIVO_DEVOLUCAO","comments":"De/Para dos Motivos de Devolu��o entre a Tend�ncia e a TIM.","lastddltime":"09/08/2019 08:07:09","createdtime":"12/05/2019 11:02:58"}
,
{"schema":"ADM_SIG_PRC","name":"SIGTIM_DP_PRODUTO","comments":"","lastddltime":"01/08/2019 12:40:32","createdtime":"12/05/2019 11:03:01"}
,
{"schema":"ADM_SIG_PRC","name":"SIGTIM_DP_SEGMENTOS","comments":"","lastddltime":"09/08/2019 08:07:11","createdtime":"12/05/2019 11:02:59"}
,
{"schema":"ADM_SIG_PRC","name":"SIGTIM_EMPRESA_SEQ","comments":"","lastddltime":"15/05/2019 10:59:53","createdtime":"12/05/2019 11:03:01"}
,
{"schema":"ADM_SIG_PRC","name":"SIGTIM_EST_EMP","comments":"","lastddltime":"15/05/2019 10:59:53","createdtime":"12/05/2019 11:03:01"}
,
{"schema":"ADM_SIG_PRC","name":"SIGTIM_USUARIOS","comments":"","lastddltime":"15/05/2019 10:59:53","createdtime":"12/05/2019 11:03:01"}
,
{"schema":"ADM_SIG_PRC","name":"SIG_TIPOCLIENTE_DISP","comments":"","lastddltime":"15/05/2019 10:59:53","createdtime":"12/05/2019 11:03:00"}
,
{"schema":"ADM_SIG_PRC","name":"SIG_TIPOLOGRADOURO_DISP","comments":"","lastddltime":"15/05/2019 10:59:53","createdtime":"12/05/2019 11:03:00"}
,
{"schema":"ADM_SIG_PRC","name":"SIG_TIPOSCLIENTES","comments":"Cadastro de Tipos de Clientes","lastddltime":"15/05/2019 10:59:53","createdtime":"12/05/2019 11:01:14"}
,
{"schema":"ADM_SIG_PRC","name":"SIG_TIPOSCLIENTES_CT","comments":"","lastddltime":"15/05/2019 10:59:53","createdtime":"12/05/2019 11:03:01"}
,
{"schema":"ADM_SIG_PRC","name":"SIG_TIPOSLOGRADOUROS","comments":"","lastddltime":"15/05/2019 10:59:53","createdtime":"12/05/2019 11:01:14"}
,
{"schema":"ADM_SIG_PRC","name":"SIG_TIPOSLOGRADOUROS_CT","comments":"","lastddltime":"15/05/2019 10:59:53","createdtime":"12/05/2019 11:03:01"}
,
{"schema":"ADM_SIG_PRC","name":"SIG_TIPO_TRANSACAO","comments":"Tipos de transa��es enviadas pelos Sigs.","lastddltime":"15/05/2019 10:59:53","createdtime":"12/05/2019 11:02:57"}
,
{"schema":"ADM_SIG_PRC","name":"SIG_UF_DDD","comments":"Tabela que relaciona quais codigos de area s�o usados nos estados.","lastddltime":"15/05/2019 10:59:53","createdtime":"12/05/2019 11:02:58"}
,
{"schema":"ADM_SIG_PRC","name":"SIG_USUARIO_ACESSO","comments":"","lastddltime":"15/05/2019 10:59:53","createdtime":"12/05/2019 11:02:59"}
,
{"schema":"ADM_SIG_PRC","name":"SIG_USUARIO_ACESSO_PERMISSAO","comments":"","lastddltime":"15/05/2019 10:59:53","createdtime":"12/05/2019 11:02:59"}
,
{"schema":"ADM_SIG_PRC","name":"SIG_USUARIO_PROCESSO","comments":"Tabela responsável por informar quais processos o usuário tem acesso. ","lastddltime":"15/05/2019 10:59:54","createdtime":"12/05/2019 11:02:58"}
,
{"schema":"ADM_SIG_PRC","name":"SIG_VENDEDOR_DISP","comments":"","lastddltime":"15/05/2019 10:59:54","createdtime":"12/05/2019 11:02:59"}
,
{"schema":"ADM_SIG_PRC","name":"SIG_VENDEDORES","comments":"","lastddltime":"15/05/2019 10:59:54","createdtime":"12/05/2019 11:01:15"}
,
{"schema":"ADM_SIG_PRC","name":"SIG_VENDEDORES_CT","comments":"","lastddltime":"15/05/2019 10:59:54","createdtime":"12/05/2019 11:03:01"}
,
{"schema":"ADM_SIG_PRC","name":"SIG_VENDEDORES_TEMP","comments":"Tabela para aux�lio no controle de status dos vendedores.","lastddltime":"19/09/2019 14:40:06","createdtime":"12/05/2019 11:02:28"}
,
{"schema":"ADM_SIG_PRC","name":"SIG_VISITA_DISP","comments":"","lastddltime":"15/05/2019 10:59:54","createdtime":"12/05/2019 11:02:59"}
,
{"schema":"ADM_SIG_PRC","name":"SIG_VISITAS","comments":"","lastddltime":"15/05/2019 10:59:54","createdtime":"12/05/2019 11:01:15"}
,
{"schema":"ADM_SIG_PRC","name":"SIG_VISITAS_CT","comments":"","lastddltime":"15/05/2019 10:59:54","createdtime":"12/05/2019 11:03:01"}
,
{"schema":"ADM_SIT","name":"LOTE_SEGURO","comments":"","lastddltime":"12/05/2019 12:54:19","createdtime":"12/05/2019 11:02:57"}
,
{"schema":"ADM_SIT","name":"LOTE_SEGURO_BKP16072012","comments":"","lastddltime":"12/05/2019 11:02:57","createdtime":"12/05/2019 11:02:57"}
,
{"schema":"ADM_SIT","name":"SIT_ARQUIVO","comments":"","lastddltime":"13/05/2019 11:41:55","createdtime":"12/05/2019 11:02:57"}
,
{"schema":"ADM_SIT","name":"SIT_ARQUIVO_BKP","comments":"","lastddltime":"12/05/2019 11:02:56","createdtime":"12/05/2019 11:02:56"}
,
{"schema":"ADM_SIT","name":"SIT_ARQUIVO_SIT_CLIENTE","comments":"","lastddltime":"13/05/2019 11:41:56","createdtime":"12/05/2019 11:02:56"}
,
{"schema":"ADM_SIT","name":"SIT_ARQUIVO_SIT_CLIENTE_BKP","comments":"","lastddltime":"12/05/2019 11:02:56","createdtime":"12/05/2019 11:02:56"}
,
{"schema":"ADM_SIT","name":"SIT_CLIENTE","comments":"","lastddltime":"12/05/2019 12:54:19","createdtime":"12/05/2019 11:02:57"}
,
{"schema":"ADM_SIT","name":"SIT_CLIENTE_BKP","comments":"","lastddltime":"12/05/2019 11:02:56","createdtime":"12/05/2019 11:02:56"}
,
{"schema":"ADM_SIT","name":"SIT_CONTROLE_ENVIO","comments":"","lastddltime":"12/05/2019 12:54:18","createdtime":"12/05/2019 11:02:57"}
,
{"schema":"ADM_SIT","name":"SIT_DADOS_CLIENTE","comments":"","lastddltime":"13/05/2019 11:41:55","createdtime":"12/05/2019 11:02:57"}
,
{"schema":"ADM_SIT","name":"SIT_LAYOUT","comments":"","lastddltime":"12/05/2019 12:54:19","createdtime":"12/05/2019 11:02:57"}
,
{"schema":"ADM_SIT","name":"SIT_LAYOUT_BKP","comments":"","lastddltime":"12/05/2019 11:02:56","createdtime":"12/05/2019 11:02:56"}
,
{"schema":"ADM_SIT","name":"SIT_LOG","comments":"","lastddltime":"13/05/2019 11:41:55","createdtime":"12/05/2019 11:02:57"}
,
{"schema":"ADM_SIT","name":"SIT_LOG_PROCESSAMENTO","comments":"","lastddltime":"13/05/2019 11:41:56","createdtime":"12/05/2019 11:02:56"}
,
{"schema":"ADM_SIT","name":"SIT_PARAMETROS","comments":"","lastddltime":"12/05/2019 12:54:18","createdtime":"12/05/2019 11:02:57"}
,
{"schema":"ADM_SIT","name":"SIT_PROCESSAMENTO","comments":"","lastddltime":"12/05/2019 12:54:18","createdtime":"12/05/2019 11:02:56"}
,
{"schema":"ADM_SIT","name":"SIT_PROPRIEDADE","comments":"","lastddltime":"13/05/2019 11:41:56","createdtime":"12/05/2019 11:02:56"}
,
{"schema":"ADM_SIT","name":"TRANSACAO_LOTE_SEGURO","comments":"","lastddltime":"12/05/2019 12:54:19","createdtime":"12/05/2019 11:02:57"}
,
{"schema":"SIG_VIVO","name":"ATIVACAO","comments":"Descri��o da tabela.","lastddltime":"04/12/2019 08:59:53","createdtime":"23/09/2019 14:29:18"}
,
{"schema":"SIG_VIVO","name":"CLIENTE","comments":"Armazena os clientes que ser�o enviados para a Vivo.","lastddltime":"26/09/2019 21:02:50","createdtime":"12/05/2019 11:03:11"}
,
{"schema":"SIG_VIVO","name":"CLIENTE_CT","comments":"Tabela de controle de Clientes usada para definir o DBACAO.","lastddltime":"16/05/2019 12:17:04","createdtime":"12/05/2019 11:03:11"}
,
{"schema":"SIG_VIVO","name":"CLIENTE_DISP","comments":"Armazena os clientes que ser�o enviados para a Vivo.","lastddltime":"17/09/2019 08:16:14","createdtime":"12/05/2019 11:03:10"}
,
{"schema":"SIG_VIVO","name":"CLIENTE_PRODUTO","comments":"Tabela de pordutos vendidos pelo cliente.","lastddltime":"12/11/2019 14:43:16","createdtime":"12/05/2019 11:03:11"}
,
{"schema":"SIG_VIVO","name":"CLIENTE_PRODUTO_DISP","comments":"Tabela de pordutos vendidos pelo cliente.","lastddltime":"26/11/2019 08:47:38","createdtime":"12/05/2019 11:03:10"}
,
{"schema":"SIG_VIVO","name":"CODIGO_DISTRIBUIDOR_DISP","comments":"Lista Fixa de C�digo do Distribuidor.","lastddltime":"15/05/2019 11:00:05","createdtime":"12/05/2019 11:03:11"}
,
{"schema":"SIG_VIVO","name":"DEVOLUCAO_CLIENTE","comments":"Tabela de envio de devolu��o cliente.","lastddltime":"15/07/2019 14:36:31","createdtime":"12/05/2019 11:03:10"}
,
{"schema":"SIG_VIVO","name":"DEVOLUCAO_CLIENTE_CT","comments":"Tabela de controle de entrada cliente.","lastddltime":"15/05/2019 11:00:05","createdtime":"12/05/2019 11:03:10"}
,
{"schema":"SIG_VIVO","name":"DEVOLUCAO_DISTRIBUIDOR","comments":"DEVOLUCAO_DISTRIBUIDOR","lastddltime":"16/05/2019 12:03:23","createdtime":"12/05/2019 11:03:11"}
,
{"schema":"SIG_VIVO","name":"DEVOLUCAO_DISTRIBUIDOR_CT","comments":"DEVOLUCAO_DISTRIBUIDOR_CT","lastddltime":"15/05/2019 11:00:05","createdtime":"12/05/2019 11:03:10"}
,
{"schema":"SIG_VIVO","name":"DEVOLUCAO_VENDEDOR","comments":"DEVOLUCAO_VENDEDOR","lastddltime":"16/05/2019 12:03:22","createdtime":"12/05/2019 11:03:11"}
,
{"schema":"SIG_VIVO","name":"DEVOLUCAO_VENDEDOR_CT","comments":"Descri��o da tabela.","lastddltime":"15/05/2019 11:00:05","createdtime":"12/05/2019 11:03:11"}
,
{"schema":"SIG_VIVO","name":"ENTRADA_CLIENTE","comments":"Tabela de envio de entrada cliente.","lastddltime":"15/07/2019 14:36:31","createdtime":"12/05/2019 11:03:11"}
,
{"schema":"SIG_VIVO","name":"ENTRADA_CLIENTE_CT","comments":"Tabela de controle de entrada cliente.","lastddltime":"15/05/2019 11:00:05","createdtime":"12/05/2019 11:03:11"}
,
{"schema":"SIG_VIVO","name":"ENTRADA_DISTRIBUIDOR","comments":"Registro do recebimento dos produtos no endere�o do distribuidor.","lastddltime":"09/12/2019 14:57:01","createdtime":"12/05/2019 11:03:11"}
,
{"schema":"SIG_VIVO","name":"ENTRADA_DISTRIBUIDOR_CT","comments":"Tabela de controle de entrada distribuidor usada para definir o DBACAO.","lastddltime":"15/05/2019 11:00:05","createdtime":"12/05/2019 11:03:11"}
,
{"schema":"SIG_VIVO","name":"ENTRADA_VENDEDOR","comments":"Descri��o da tabela.","lastddltime":"16/05/2019 12:03:22","createdtime":"12/05/2019 11:03:11"}
,
{"schema":"SIG_VIVO","name":"ENTRADA_VENDEDOR_CT","comments":"Descri��o da tabela.","lastddltime":"15/05/2019 11:00:05","createdtime":"12/05/2019 11:03:11"}
,
{"schema":"SIG_VIVO","name":"ENVIO_CT","comments":"Tabela responsável por armazenar o último status de pistolagem.","lastddltime":"03/06/2019 21:01:40","createdtime":"12/05/2019 11:03:10"}
,
{"schema":"SIG_VIVO","name":"ESCRITORIO_OPERACAO","comments":"Descri��o da tabela.","lastddltime":"16/09/2019 09:24:13","createdtime":"12/09/2019 08:16:06"}
,
{"schema":"SIG_VIVO","name":"ESCRITORIO_OPERACAO_DISP","comments":"Descri��o da tabela.","lastddltime":"18/09/2019 09:55:19","createdtime":"17/09/2019 11:12:59"}
,
{"schema":"SIG_VIVO","name":"ESTOQUE_PDV","comments":"Estoque de produtos nos PDVs do distribuidor.","lastddltime":"04/12/2019 08:32:35","createdtime":"17/09/2019 11:02:33"}
,
{"schema":"SIG_VIVO","name":"FREQUENCIA_VISITA_VEND_DISP","comments":"Entidade de Diponibiliza��o da frequencia de visita do vendedor.","lastddltime":"20/05/2019 14:24:39","createdtime":"20/05/2019 14:24:39"}
,
{"schema":"SIG_VIVO","name":"INVENTARIO","comments":"Descri��o da tabela.","lastddltime":"10/10/2019 09:30:22","createdtime":"04/10/2019 09:56:10"}
,
{"schema":"SIG_VIVO","name":"MEIO_RECARGA_DISP","comments":"A tabela MEIO_RECARGA_DISP contem os meios de recarga de acordo com a Vivo.","lastddltime":"15/05/2019 11:00:06","createdtime":"12/05/2019 11:03:11"}
,
{"schema":"SIG_VIVO","name":"NF_EMITIDA","comments":"Tabela de notas fiscais emitidas pela Operadora.","lastddltime":"15/05/2019 11:00:06","createdtime":"12/05/2019 11:03:10"}
,
{"schema":"SIG_VIVO","name":"NF_EMITIDA_PRODUTO","comments":"Tabela de produtos da nota fiscal emitida.","lastddltime":"15/05/2019 11:00:06","createdtime":"12/05/2019 11:03:10"}
,
{"schema":"SIG_VIVO","name":"NOME_INTEGRADOR_DISP","comments":"A tabela NOME_INTEGRADOR_DISP contem os nomes dos integradores de acordo com a Vivo.","lastddltime":"15/05/2019 11:00:06","createdtime":"12/05/2019 11:03:11"}
,
{"schema":"SIG_VIVO","name":"POSITIVACAO","comments":"Descri��o da tabela.","lastddltime":"15/05/2019 11:00:06","createdtime":"12/05/2019 11:03:10"}
,
{"schema":"SIG_VIVO","name":"PRODUTO_DISP","comments":"A tabela PRODUTO_DISP contem os produtos de acordo com as informa��es da Vivo.","lastddltime":"15/05/2019 11:00:06","createdtime":"12/05/2019 11:03:11"}
,
{"schema":"SIG_VIVO","name":"RAMO_ATIVIDADE_DISP","comments":"Descri��o da tabela.","lastddltime":"15/05/2019 11:00:06","createdtime":"12/05/2019 11:03:11"}
,
{"schema":"SIG_VIVO","name":"RAZAO_DEVOLUCAO_DISP","comments":"Lista Fixa de Raz�o Devolu��o","lastddltime":"15/05/2019 11:00:06","createdtime":"12/05/2019 11:03:11"}
,
{"schema":"SIG_VIVO","name":"RAZAO_ENTREGA_DISP","comments":"A tabela RAZAO_ENTREGA_DISP contem as raz�es de acordo com as informa��es da Vivo.","lastddltime":"15/05/2019 11:00:06","createdtime":"12/05/2019 11:03:11"}
,
{"schema":"SIG_VIVO","name":"REDE_ASSOCIATIVA_DISP","comments":"Descri��o da tabela.","lastddltime":"26/06/2019 08:37:14","createdtime":"30/05/2019 17:08:26"}
,
{"schema":"SIG_VIVO","name":"SIG_MOVIMENTACAO_ESTOQUE","comments":"Tabela que representa a a tabela ADM_SGV_SFA.FILA_MOVIMENTACAO_ESTOQUE.","lastddltime":"02/11/2019 05:01:50","createdtime":"12/05/2019 11:02:26"}
,
{"schema":"SIG_VIVO","name":"SIG_TIPO_TRANSACAO_MOVIMENTO","comments":"Tabela que armazena os tipos de transa��es de acordo com cada movimento.","lastddltime":"15/05/2019 11:00:06","createdtime":"12/05/2019 11:03:10"}
,
{"schema":"SIG_VIVO","name":"STATUS_PDV_DISP","comments":"A tabela STATUS_PDV_DISP contem os tipos de status do pdv de acordo com a Vivo.","lastddltime":"15/05/2019 11:00:06","createdtime":"12/05/2019 11:03:11"}
,
{"schema":"SIG_VIVO","name":"STATUS_PISTOLAGEM_NF","comments":"Descri��o da tabela.","lastddltime":"15/05/2019 11:00:06","createdtime":"12/05/2019 11:03:10"}
,
{"schema":"SIG_VIVO","name":"TIPO_LOCAL_PDV_DISP","comments":"Lista Fixa de Tipo Local PDV","lastddltime":"30/05/2019 17:08:26","createdtime":"12/05/2019 11:03:11"}
,
{"schema":"SIG_VIVO","name":"TIPO_PDV_DISP","comments":"Lista fixa de Tipo de PDV","lastddltime":"15/05/2019 11:00:06","createdtime":"12/05/2019 11:03:11"}
,
{"schema":"SIG_VIVO","name":"TIPO_POSITIVACAO_DISP","comments":"A tabela TIPO_POSITIVACAO contem os tipos de positiva��o com as informa��es da Vivo.","lastddltime":"15/05/2019 11:00:06","createdtime":"12/05/2019 11:03:11"}
,
{"schema":"SIG_VIVO","name":"TRANSACAO_NA","comments":"Tabela de transa��es n�o associadas a um PDV.","lastddltime":"15/05/2019 11:00:07","createdtime":"12/05/2019 11:03:10"}
,
{"schema":"SIG_VIVO","name":"VENDAS_ZERO_CLIENTE","comments":"Tabela de clientes com venda zero.","lastddltime":"15/05/2019 11:00:07","createdtime":"12/05/2019 11:03:10"}
,
{"schema":"SIG_VIVO","name":"VENDAS_ZERO_PRODUTO","comments":"Tabela de produtos associados ao PDV com venda zero.","lastddltime":"15/05/2019 11:00:07","createdtime":"12/05/2019 11:03:10"}
,
{"schema":"SIG_VIVO","name":"VISITA_VENDEDOR","comments":"Tabela que registra visita vendedor.","lastddltime":"10/10/2019 09:30:21","createdtime":"04/10/2019 09:51:19"}
,
{"schema":"SIG_VIVO","name":"VIVO_CANAL_RECARGA_DISP","comments":"","lastddltime":"15/05/2019 11:00:07","createdtime":"12/05/2019 11:03:12"}
,
{"schema":"SIG_VIVO","name":"VIVO_CANAL_VENDA_DISP","comments":"","lastddltime":"15/05/2019 11:00:07","createdtime":"12/05/2019 11:03:12"}
,
{"schema":"SIG_VIVO","name":"VIVO_CLASS_PDV_DISP","comments":"","lastddltime":"15/05/2019 11:00:07","createdtime":"12/05/2019 11:03:12"}
,
{"schema":"SIG_VIVO","name":"VIVO_CLIENTE","comments":"","lastddltime":"15/05/2019 11:00:07","createdtime":"12/05/2019 11:03:12"}
,
{"schema":"SIG_VIVO","name":"VIVO_CLIENTE_CT","comments":"","lastddltime":"15/05/2019 11:00:07","createdtime":"12/05/2019 11:03:12"}
,
{"schema":"SIG_VIVO","name":"VIVO_CONTROLE_ARQUIVO","comments":"","lastddltime":"15/05/2019 11:00:07","createdtime":"12/05/2019 11:03:12"}
,
{"schema":"SIG_VIVO","name":"VIVO_DEVOLUCAO_CLIENTE","comments":"Registro da devolu��o de um produto. Remove a quantidade do estoque do Ponto de Venda.","lastddltime":"15/05/2019 11:00:07","createdtime":"12/05/2019 11:03:11"}
,
{"schema":"SIG_VIVO","name":"VIVO_DEVOLUCAO_CLIENTE_CT","comments":"","lastddltime":"15/05/2019 11:00:07","createdtime":"12/05/2019 11:03:11"}
,
{"schema":"SIG_VIVO","name":"VIVO_DEVOLUCAO_DISTRIBUIDOR","comments":"Registro da devolu��o de um produto. Remove a quantidade do estoque do Distribuidor.","lastddltime":"15/05/2019 11:00:07","createdtime":"12/05/2019 11:03:11"}
,
{"schema":"SIG_VIVO","name":"VIVO_DEVOLUCAO_VENDEDOR","comments":"Registro da devolu��o de um produto. Remove a quantidade do estoque onde os produtos estiverem (No momento, somente devolu��o de vendedor).","lastddltime":"15/05/2019 11:00:07","createdtime":"12/05/2019 11:03:12"}
,
{"schema":"SIG_VIVO","name":"VIVO_DEVOLUCAO_VENDEDOR_CT","comments":"","lastddltime":"15/05/2019 11:00:07","createdtime":"12/05/2019 11:03:11"}
,
{"schema":"SIG_VIVO","name":"VIVO_ENTRADA_CLIENTE","comments":"Registro da entrega do produto ao ponto de venda. Adiciona a quantidade ao estoque do ponto de venda e remove do estoque o vendedor. ","lastddltime":"15/05/2019 11:00:07","createdtime":"12/05/2019 11:03:12"}
,
{"schema":"SIG_VIVO","name":"VIVO_ENTRADA_CLIENTE_CT","comments":"","lastddltime":"15/05/2019 11:00:07","createdtime":"12/05/2019 11:03:12"}
,
{"schema":"SIG_VIVO","name":"VIVO_ENTRADA_DISTRIBUIDOR","comments":"Registro do recebimento dos produtos no endere�o do distribuidor. ","lastddltime":"15/05/2019 11:00:07","createdtime":"12/05/2019 11:03:12"}
,
{"schema":"SIG_VIVO","name":"VIVO_ENTRADA_DISTRIBUIDOR_CT","comments":"","lastddltime":"15/05/2019 11:00:07","createdtime":"12/05/2019 11:03:12"}
,
{"schema":"SIG_VIVO","name":"VIVO_ENTRADA_VENDEDOR","comments":"Registro da entrega do produto ao vendedor. Adiciona a quantidade informada ao estoque do vendedor e remove do estoque do distribuidor. ","lastddltime":"15/05/2019 11:00:08","createdtime":"12/05/2019 11:03:12"}
,
{"schema":"SIG_VIVO","name":"VIVO_ENTRADA_VENDEDOR_CT","comments":"","lastddltime":"15/05/2019 11:00:08","createdtime":"12/05/2019 11:03:12"}
,
{"schema":"SIG_VIVO","name":"VIVO_EXEC_ESTAB_EMP","comments":"","lastddltime":"15/05/2019 11:00:08","createdtime":"12/05/2019 11:03:12"}
,
{"schema":"SIG_VIVO","name":"VIVO_INTEGRADOR_DISP","comments":"","lastddltime":"15/05/2019 11:00:08","createdtime":"12/05/2019 11:03:12"}
,
{"schema":"SIG_VIVO","name":"VIVO_RAMO_ATIVIDADE_DISP","comments":"","lastddltime":"15/05/2019 11:00:08","createdtime":"12/05/2019 11:03:12"}
,
{"schema":"SIG_VIVO","name":"VIVO_REDE_VAREJO_DISP","comments":"","lastddltime":"15/05/2019 11:00:08","createdtime":"12/05/2019 11:03:12"}
];
