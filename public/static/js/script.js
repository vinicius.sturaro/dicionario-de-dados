$(document).ready(function() {

    /*
        define as propriedades default do plugin dataTable
    */
    datatable_options = {
        lengthMenu: [[10, 15, 20, 50, 100,-1], [10, 15,20, 50, 100, "Todos"]],
        pageLength: 15,
        language: {
            "lengthMenu": "Mostrar _MENU_ registros",
            "zeroRecords": "<h5 class='text-danger text-center'>Não há registros</h5>",
            "info": "<div class='text-right'><h5><small>Foram encontrados _MAX_ registro(s)</small></h5></div>",
            "infoEmpty": "<div class='text-right'><h5><small>Foram encontrados _MAX_ registro(s)</small></h5></div>",
            "search": "Busca geral:",
            "infoFiltered": "<div class='text-right'><h5><small>Filtrando _TOTAL_ registro(s)</small></h5></div>",
            "paginate": {
                "previous": "<i class='fa fa-chevron-left'></i>",
                "next": "<i class='fa fa-chevron-right'></i>"
            }
        }
    };

    $('#last-update').html(last_update );

    /*
    Na inicialização da página, é carregado um arquivo javascript com a definição das tabelas do banco.
    para cada tabela , é gerada uma nova linha na tabela #tables_id
    */
    items = '';
    $.each( tables, function( key, val ) {
        console.log("schema: "+val.schema +" nome: "+val.name);

        var table_name = val.schema + '__' + val.name;
        var project = val.schema.substr(4,3);

        items+='<tr>';
        items+='<td><a  id="HASH_' + table_name + '"  href="#' + table_name + '" data-id="'+ table_name +'" class="load-table">' + val.name + '</a></td>';
        items+='<td>' + val.schema + '</td>';
        items+='<td>' + val.createdtime + '</td>';
        items+='<td>' + val.lastddltime + '</td>';
        items+='<td>' + val.comments + '</td>';
		items+='<td> Jorge </td>';		
        /*
        items+='<td><a target="_blank" href="http://istrack.redetendencia.com.br:8080/newIssue?project='+project+'&clearDraft=true&description=Alterar+a+tabela+'+ val.schema.toLowerCase() + '.' + val.name +'&summary=BD+-+Alterar+tabela+'+val.name+'&c=State+À+fazer&c=Setor+F%C3%A1brica&c=Subsystem+BD&c=Fix+versions+Backlog" title="Criar tarefa a partir da tabela"><img alt="" src="static/img/favicon-utrack.ico" width="20" /></a></td>';
		*/
        items+='</tr>';
    });

    //limpa o corpo da tabela, para alimentá-lo com o conteudo da variável items
    $('#tables_id tbody *').remove();
    $('#tables_id tbody').html(items);

    $('#tables_id thead th').not('#tables_id thead th:last-child').not('#tables_id thead th:nth-child(5)').each( function () {
        var title = $('#tables_id thead th').eq( $(this).index() ).text();
        $(this).html( '<input type="text" class="form-control input-sm" title="'+title+'" placeholder="'+title+'..."/>' );
    } );

    // transforma a tabela #tables_id em um dataTable
    var tableAll = $('#tables_id').DataTable(datatable_options);

    // aplicando os campos de busca (menos na última coluna e na coluna dos "Comentários")
    tableAll.columns().eq(0).each(function (colIdx) {
        //Necessário pois se não retornar falso, quando clickar no input a coluna será ordenada
        $( 'input', tableAll.column(colIdx).header()).on('click', function(e){
            return false; //return false to avoid propagation events
        });
        $( 'input', tableAll.column(colIdx).header()).on( 'keyup change', function () {
            tableAll
                .column(colIdx)
                .search(this.value ? '^'+this.value+'' : '', true, false )
                .draw();
        } );
    } );

    $('#back').delay(1000).fadeOut('slow');




    function render_table(table_name){

        // nesse método, é informado o nome da tabela.
        // Após capturar a variavel correspondente à tabela informada,
        //os atributos da tabela(campos e indices) são carregados
        //dinamicamente na aba correspondente


        var table = window[table_name];

        var content = '';
        content+= '<br/>'
        content+='<div class="row"><div class="col-lg-8 col-md-8"><table id="tb_' + table_name + '" class="display table table-striped table-condensed table-bordered  table-striped floatThead" cellspacing="0" width="100%">';
        content+='<thead><tr>';
        content+='<th width="50">Ordem</th>';
        content+='<th>Nome</th>';
        content+='<th>Tipo</th>';
        content+='<th>Nulo</th>';
        content+='<th>Valor padrão</th>';
        content+='<th>Comentário</th>';
        content+='</tr><thead>';

        //alimentando campos ta tabela
        content+='<tbody>';
        $.each(table.fields, function( key, val ) {
            content+='<tr>';
            content+='<td>' + val.order        +'</td>';

            if(val.foreignkeytable){
                content+='<td><a href="#" data-id="'+ val.foreignkeytable +'" class="load-table">' + val.name + '</a>';
            } else {
                content+='<td>'+ val.name;
            }

            if(val.ispkey == 1){
                content+=' <i class="fa fa-key text-warning"></i>';
            }

            content+='</td>';
            content+='<td>' + val.datatype    +'</td>';
            content+='<td>' + val.nullable    +'</td>';
            content+='<td>' + (val.datadefault || " ") +'</td>';
            content+='<td>' + (val.comments    || " ")    +'</td>';
            content+='</tr>';

            console.log(val);
        });
        content+='</tbody>';
        content+='</table>';
        content+='</div>';

        content+='<div class="col-lg-4 col-md-4">';
        content+='      <div class="panel panel-primary">';
        content+='          <div class="panel-heading"> Índices';
        content+='          </div>';
        content+='          <div class="panel-body">';

        // alimentando indices
        if (!table.indexes || table.indexes.length == 0){
             content+= '<h5 class="text-danger text-center">Não há registros</h5>';
         }else{
            $.each(table.indexes, function( key, val ) {
                content+='<h6>' + val.name + '<small> ' + val.columns + ' </small> </h6>';
            });
         }

        content+='          </div>';
        content+='      </div>';
        content+='</div>';

        // limpando o conteudo da aba correspondente à tabela
        $('#' + table_name).html(content);

        // transforma a tabela em um dataTable
        var datatable = $('#tb_' + table_name).dataTable(datatable_options);
    }


    //--------------------------------
    // -> Eventos
    //--------------------------------

    function load_tab(table_name){
          if (!$( '#' + table_name).length ) {
            var metadata = table_name.split('__');
            var guia = '<li role="presentation"><a href="#' + table_name + '" aria-controls="' + table_name + '" role="tab" data-toggle="tab" id="tab_' + table_name + '"><h6>' + metadata[1] + ' <small>' + metadata[0] + '</small><button type="button" data-id="' + table_name + '" class="close close-left close-tab" aria-label="Fechar"><span aria-hidden="true" class="text-danger">&times;</span></button></h6></a></li>';
            var div = '<div role="tabpanel" class="tab-pane fade" id="'+ table_name + '"> conteudo para ' + table_name + '</div>';
            $('#tabs').append(guia);
            $('#tab-content').append(div);
            render_table(table_name);
        }

        $("#tab_" + table_name).tab('show');
    }

    $(document).on('click','.load-table',function(e){
        // ao clicar em um elemento que possua a classe .load-table,
        //  o codigo utiliza o atributo data-id do elemento para criar dinamicamente uma nova aba,
        //  para que o conteudo(lista de campos da tabela) seja renderizado;
        e.preventDefault();
        var tabela = $(this).attr('data-id');
        load_tab(tabela);

    });

    $('#tabs a').click(function (e) {
        e.preventDefault();
        $(this).tab('show');
    });

    $(document).on('click','.close-tab',function(e){
        e.preventDefault();
        var tabela = $(this).attr('data-id');
        $('#tab_' + tabela).parent('li').remove();
        $('#' + tabela).remove();

        console.log('#' + tabela);
        $('#tabs a:last').tab('show');
    });


    var hash_table = window.location.hash.substr(1);
    if ( hash_table != ''){
        load_tab(hash_table);
    }



});
