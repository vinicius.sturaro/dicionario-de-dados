SET SERVEROUTPUT ON
 DECLARE
    v_columns VARCHAR2(4000) := '';
    v_col_aux VARCHAR2(4000) := '';
    v_arquivo      utl_file.file_type;
    v_data_default VARCHAR2(4000);
    ispkey         NUMBER := 0;
    fktable        VARCHAR2(4000);
BEGIN
  v_arquivo := utl_file.fopen('SGV_SIT', 'database.js', 'W', 32767);
  FOR y IN (SELECT owner
                  ,table_name
                  ,rownum AS linha
              FROM (SELECT t.owner
                          ,t.table_name
                      FROM dba_tables t
                    WHERE T.OWNER IN ('ADM_SGV_COB'
                                    ,'ADM_SGV_SFA'
                                    ,'DW_SGV'
                                    ,'ADM_SGV_WS'
                                    ,'ADM_SGV_PIN'
                                    ,'ADM_SGV_SEX'
                                    ,'ADM_SIT'
                                    ,'ADM_SGV_EQP'
                                    ,'ADM_SGV_ACE'
                                    ,'ADM_SGV_PRC'
                                    ,'ADM_SGV_FTR'
                                    ,'ADM_SGV_CAD'
                                    ,'ADM_SGV_AUD'
                                    ,'ADM_SGV_ALO'
                                    ,'ADM_SGV_TMP'
                                    ,'ADM_SGR_AUD'
                                    ,'ADM_SGV_CEP'
                                    ,'ADM_SGR_CRED'
                                    ,'ADM_SIG_PRC'
                                    ,'ADM_SGV_TRN'
                                    ,'ADM_SGR_ACE'
                                    ,'ADM_SGV_JDE'
                                    ,'ADM_SGR_CAD'
                                    ,'ADM_SGV_AUX'
                                    ,'ADM_SGV_INT'
                                    ,'DATAMART'
                                    ,'ADM_SGV_DNE'
                                    ,'SIG_VIVO'
                                    ,'ADM_ACE_AUD'
                                    ,'ADM_ACE_CAD'
                                    --,'ADM_BPM'
                                    ,'ADM_SGL_CAD'
                                    ,'ADM_SGL_FTR'
                                    ,'ADM_SGR'
                                    ,'ADM_SGR_PROS'
                                    ,'ADM_SGV'
                                    ,'ADM_SGV_BI'
                                    ,'ADM_SGV_GRD'
                                    ,'ADM_SGV_RMV')
                    ORDER BY 1,2))
  LOOP
    utl_file.put_line(v_arquivo, 'var ' || y.owner || '__' || y.table_name ||' = {"fields":[');
    FOR z IN (SELECT table_name
                    ,column_name
                    ,data_type
                    ,nullable
                    ,data_default
                    ,comments
                    ,column_id
                    ,ispk
                    ,rownum AS linha
                FROM (SELECT t.owner
                            ,t.table_name
                            ,t.column_name
                            ,decode(t.data_type, 'VARCHAR2', 'VARCHAR2(' || t.data_length || ')', 'NUMBER', 'NUMBER(' || NVL(t.data_precision,'38') || ')','NVARCHAR','NVARCHAR('||t.data_length||')','NVARCHAR2','NVARCHAR2('||t.data_length||')','NCHAR','NCHAR('||t.data_length||')','CHAR','CHAR('||t.data_length||')', t.data_type) data_type
                            ,decode(t.nullable, 'Y', 'Sim', 'N', 'Nao', '**') nullable
                            ,data_default
                            ,TRIM(REPLACE(translate(c.comments, '"' || chr(10) || chr(13), '"  '), '"', '\"')) comments
                            ,column_id
                            ,NVL((select 'Y'
                                    from dba_constraints cc 
                                    join dba_cons_columns ccc on ccc.constraint_name = cc.constraint_name and ccc.table_name = cc.table_name and ccc.owner = cc.owner and ccc.position is not null
                                   where ccc.column_name = t.column_name
                                     and cc.owner = t.owner
                                     and cc.table_name = t.table_name
                                     and cc.constraint_type = 'P'),'N') ispk
                        FROM dba_tab_columns t
                        JOIN dba_col_comments c ON c.owner = t.owner AND c.table_name = t.table_name AND c.column_name = t.column_name
                       WHERE t.owner = y.owner
                         AND t.table_name = y.table_name
                       ORDER BY t.column_id))
    LOOP
      v_data_default := z.data_default;
      v_data_default := TRIM(REPLACE(REPLACE(translate(z.data_default, '"' || chr(10) || chr(13), '"  '), '"', '\"'), '+', ' + '));
      BEGIN
        SELECT orig.owner || '__' || orig.table_name
          INTO fktable
          FROM dba_cons_columns cc
          JOIN dba_constraints c
            ON c.constraint_name = cc.constraint_name
           AND c.owner = cc.owner
           AND c.table_name = cc.table_name
          JOIN dba_constraints orig
            ON orig.constraint_name = c.r_constraint_name
           AND orig.owner LIKE 'ADM_SG%'
         WHERE cc.table_name = z.table_name
           AND cc.owner = y.owner
           AND cc.column_name = z.column_name
           AND c.constraint_type = 'R';
      EXCEPTION
        WHEN no_data_found THEN
          fktable := '';
        WHEN OTHERS THEN
          fktable := '';
      END;
      IF z.ispk = 'Y' THEN ispkey := 1; ELSE ispkey := 0; END IF;
      IF z.linha != 1 THEN utl_file.put_line(v_arquivo, ','); END IF;
      utl_file.put_line(v_arquivo, '{"ispkey":"' || ispkey ||
                         '","foreignkeytable":"' || fktable ||
                         '","order":"' || z.column_id ||
                         '","name":"' || z.column_name ||
                         '","datatype":"' || z.data_type ||
                         '","nullable":"' || z.nullable ||
                         '","datadefault":"' || v_data_default ||
                         '","comments":"' || z.comments || '"}');
    END LOOP;
    utl_file.put_line(v_arquivo, '],'); -- Fechando CAMPOS
    utl_file.put_line(v_arquivo, '"indexes":[');
    FOR z IN (SELECT owner
                    ,index_name
                    ,table_name
                    --,index_columns
                    ,rownum AS linha
                FROM (SELECT i.index_name
                            --,(SELECT listagg(ic.column_name, ', ') within GROUP(ORDER BY ic.column_position)
                            --    FROM dba_ind_columns ic
                            --   WHERE ic.index_owner = i.owner
                            --     AND ic.index_name = i.index_name
                            --     AND ic.table_name = i.table_name) index_columns
                            ,i.owner
                            ,i.table_name
                        FROM dba_indexes i
                       WHERE i.owner = y.owner
                         AND i.table_name = y.table_name
                         AND i.index_type != 'LOB'
                       ORDER BY i.index_name))
    LOOP
            v_columns := '';
            IF z.linha != 1 THEN utl_file.put_line(v_arquivo, ','); END IF;
            FOR cols IN (SELECT ic.column_name
                               ,ic.column_position
                           FROM dba_ind_columns ic
                          WHERE ic.index_owner = z.owner
                            AND ic.index_name = z.index_name
                            AND ic.table_name = z.table_name
                          ORDER BY ic.column_position)
            LOOP

                v_columns := v_columns || cols.column_name;    
                
                --Buscando por Expressions.....
                IF instr(cols.column_name,'SYS') != 0 THEN
                    SELECT ex.column_expression
                      INTO v_col_aux
                      FROM dba_ind_expressions ex
                     WHERE ex.column_position = cols.column_position
                       AND ex.index_name = z.index_name
                       AND ex.index_owner = z.owner
                       AND ex.table_name = z.table_name;
                    v_col_aux := TRIM(REPLACE(REPLACE(translate(v_col_aux, '"' || chr(10) || chr(13), '"  '), '"', '\"'), '+', ' + '));
                    v_columns := v_columns || ' [' || v_col_aux || ']';
                END IF;
                
                v_columns := v_columns || ', ';

            END LOOP;
            v_columns := substr(v_columns,1,length(v_columns)-2); --Retirando o ', ' do ultimo registro
            v_columns := '{"name":"' || z.index_name || '","columns":"' || v_columns || '"}';
            utl_file.put_line(v_arquivo, v_columns );

    END LOOP;
    utl_file.put_line(v_arquivo, ']};'); -- Fechando INDICES
  END LOOP;
  utl_file.fclose(v_arquivo);
--  adm_sgv.sgv_sit_pk001.envia_ftp('172.16.20.21', '21', 'testerel1', 'testerel1', 'SGV_SIT', 'database.js', 'database.js');
--  utl_file.fremove('SGV_SIT', 'database.js');
END;
/
